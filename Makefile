.PHONY: all debug release documentation grammar test

all: debug release documentation

debug:
	dotnet build -c Debug src/NanoC.sln

release:
	dotnet build -c Release src/NanoC.sln

documentation:
	make -C doc

grammar:
	make -C src/NanoCLang/Grammar

test:
	-dotnet test src/NanoC.sln --collect:"XPlat Code Coverage"
	/c/Users/micha/.dotnet/tools/reportgenerator "-reports:src\NanoCLang.Tests\TestResults\*\coverage.cobertura.xml" "-targetdir:src\bin\public"
