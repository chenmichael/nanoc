﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for location bindings.
    /// </summary>
    public class LocationBinding : Base, IEquatable<LocationBinding?>, IHeapElement {
        /// <summary>
        /// Creates a new instance of a location binding that binds the <paramref name="blocks"/> to the <paramref name="location"/>.
        /// </summary>
        /// <param name="location">Location that is bound to.</param>
        /// <param name="blocks">Blocks that are bound in the location.</param>
        public LocationBinding(Location location, BlockType[] blocks) {
            Location = location;
            Blocks = blocks;
        }
        /// <summary>
        /// Location that is bound to.
        /// </summary>
        public Location Location { get; }
        /// <summary>
        /// Blocks that are bound in the location.
        /// </summary>
        public BlockType[] Blocks { get; }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Location.Tokens(args)) yield return tk;
            if (args.SpaceBetweenLocationAndBindOperator) yield return " ";
            yield return "~>";
            if (args.SpaceBetweenBindOperatorAndBlock) yield return " ";
            foreach (var tk in StringFormatterToken.Separated(Blocks, args, args.BlockSeparator)) yield return tk;
        }
        /// <inheritdoc cref="IHeapElement.GetBindings(GlobalEnvironment, out IEnumerable{KeyValuePair{string, string}})"/>
        public IEnumerable<LocationBinding> GetBindings(GlobalEnvironment phi, out IEnumerable<KeyValuePair<string, string>> links) {
            links = Enumerable.Empty<KeyValuePair<string, string>>();
            return new LocationBinding[] { this };
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as LocationBinding);
        /// <inheritdoc/>
        public bool Equals(LocationBinding? other) => !(other is null) && EqualityComparer<Location>.Default.Equals(Location, other.Location) && Blocks.SequenceEqual(other.Blocks);
        /// <inheritdoc/>
        public override int GetHashCode() {
            var hash = new HashCode();
            hash.Add(Location);
            foreach (var block in Blocks) hash.Add(block);
            return hash.ToHashCode();
        }
        /// <inheritdoc/>
        public static bool operator ==(LocationBinding? left, LocationBinding? right) => EqualityComparer<LocationBinding?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(LocationBinding? left, LocationBinding? right) => !(left == right);
        #endregion
    }
}