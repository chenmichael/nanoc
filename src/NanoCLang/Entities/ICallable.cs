﻿namespace NanoCLang.Entities {
    /// <summary>
    /// Provides an interface for callable entities that are called by NanoC calling conventions.
    /// </summary>
    public interface ICallable {
        /// <summary>
        /// Parameters and their types that the function expects.
        /// </summary>
        TypeParameter[] Parameters { get; }
        /// <summary>
        /// Locations that the function operates on.
        /// </summary>
        string[] Locations { get; }
    }
}