﻿using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// <see cref="Base"/> provides a superclass for all NanoC entities.
    /// All NanoC entities implement a conversion to a token stream to create a generic source code formatter.
    /// This can also be used to transpile the syntax.
    /// </summary>
    public abstract class Base : ITokenStream<StringFormatterToken, NanoCSourceFormat>, ITokenStream<StringFormatterToken, CSourceFormat> {
        /// <summary>
        /// Formats the code using the <see cref="StringFormatter"/> and returns it.
        /// </summary>
        /// <returns>String representation of the entity.</returns>
        public override string ToString() => new StringFormatter().ToString(Tokens(new NanoCSourceFormat()));
        /// <summary>
        /// Translates the entity to C code using the <see cref="StringFormatter"/> and returns it.
        /// </summary>
        /// <returns>String representation of the entity.</returns>
        public virtual string ToCString() => new StringFormatter().ToString(Tokens(new CSourceFormat()));
        /// <inheritdoc cref="ITokenStream{T, A}.Tokens(A)"/>
        public abstract IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args);
        /// <summary>
        /// Converts the entity into it's C language equivalent string token stream.
        /// </summary>
        /// <param name="args">Formatter arguments.</param>
        /// <returns>C token stream</returns>
        public virtual IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) => throw new System.InvalidProgramException($"Cannot convert entity of type {GetType().Name} into C-language!");
        #region Equality checks
        /// <summary>
        /// Checks if the entities are equal.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="right">Right hand operand.</param>
        /// <returns><see langword="true"/> iff <paramref name="left"/> is equal to <paramref name="right"/>.</returns>
        public static bool operator ==(Base left, Base right) => EqualityComparer<Base?>.Default.Equals(left, right);
        /// <summary>
        /// Checks if the entities are unequal.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="right">Right hand operand.</param>
        /// <returns><see langword="true"/> iff <paramref name="left"/> is not equal to <paramref name="right"/>.</returns>
        public static bool operator !=(Base left, Base right) => !(left == right);
        /// <inheritdoc/>
        public abstract override bool Equals(object? obj);
        /// <inheritdoc/>
        public abstract override int GetHashCode();
        #endregion
    }
}