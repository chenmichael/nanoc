﻿using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for structure fields inside structure definitions.
    /// </summary>
    public class StructField : Base, IEquatable<StructField?> {
        /// <summary>
        /// Creates a new instance of a structure field from a <paramref name="name"/> and the accompanying <paramref name="binding"/>.
        /// </summary>
        public StructField(string name, BlockType binding) {
            Name = name;
            Binding = binding;
        }
        /// <summary>
        /// Name of the structure field.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Binding that belongs to the field.
        /// </summary>
        public BlockType Binding { get; }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return Name;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Binding.Tokens(args)) yield return tk;
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as StructField);
        /// <inheritdoc/>
        public bool Equals(StructField? other) => !(other is null) && Name == other.Name && EqualityComparer<BlockType>.Default.Equals(Binding, other.Binding);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Name, Binding);
        /// <inheritdoc/>
        public static bool operator ==(StructField? left, StructField? right) => EqualityComparer<StructField?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(StructField? left, StructField? right) => !(left == right);
        #endregion
    }
}
