﻿using Microsoft.Z3;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for arbitrary indices.
    /// </summary>
    public class ArbitraryIndex : Index, IEquatable<ArbitraryIndex?> {
        /// <inheritdoc/>
        // Arbitrary indices always overlap with everything
        public override bool CollidesWith(int size, Index index, int otherSize) => true;
        /// <inheritdoc/>
        public override (BoolExpr guard, IntExpr value) SMTPossibleValues(NanoCSMT smt, int size, string prefix)
            => (smt.Context.MkTrue(), smt.Context.MkIntConst($"{prefix}arbindex"));
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "any";
        }
        /// <inheritdoc/>
        public override bool SubIndex(Index other) => other is ArbitraryIndex;
        /// <inheritdoc/>
        public override bool IncludesOffset(int offset) => true;
        /// <inheritdoc/>
        protected override Index Add(Index right) => new ArbitraryIndex();
        /// <inheritdoc/>
        protected override Index Sub(Index right) => new ArbitraryIndex();
        /// <inheritdoc/>
        public override object Clone() => new ArbitraryIndex();
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as ArbitraryIndex);
        /// <inheritdoc/>
        public bool Equals(ArbitraryIndex? other) => !(other is null);
        /// <inheritdoc/>
        public override int GetHashCode() => 0;
        /// <inheritdoc/>
        public static bool operator ==(ArbitraryIndex? left, ArbitraryIndex? right) => EqualityComparer<ArbitraryIndex?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(ArbitraryIndex? left, ArbitraryIndex? right) => !(left == right);
        #endregion
    }
}