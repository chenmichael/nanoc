﻿using Microsoft.Z3;
using System;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a superclass for all index entities.
    /// Also provides methods for analysing index overlaps.
    /// </summary>
    public abstract class Index : Base, ISubstitutable<Index>, ICloneable {
        /// <summary>
        /// Gets the zero-index that only contains the single offset zero.
        /// </summary>
        public static Index Zero { get; } = new SingletonIndex(0);
        /// <summary>
        /// Checks if the <paramref name="index"/> that contains a type of <paramref name="otherSize"/> in bytes collides with the current index.
        /// I.e. 0 and 2 collide with a 4 bit integer.
        /// Default behaviour if not overridden is the worst possible case that a collision always happens.
        /// </summary>
        /// <param name="index">Index to check against.</param>
        /// <param name="size">Size of the the type at the current index.</param>
        /// <param name="otherSize">Size of the type to check against.</param>
        /// <returns><see langword="true"/> iff the indices collide.</returns>
        public virtual bool CollidesWith(int size, Index index, int otherSize)
            => NanoCSMT.Default.IndicesCollide(this, size, index, otherSize);
        /// <summary>
        /// Embeds the index of a type with given size into the SMT expression that allows all possible index values for valid bytes.
        /// </summary>
        /// <param name="smt">Solver to use for solving the collision.</param>
        /// <param name="size">Size of the type at the current index.</param>
        /// <param name="prefix">Variable name suffix.</param>
        /// <returns>Returns a guard expression and the index expression.</returns>
        public abstract (BoolExpr guard, IntExpr value) SMTPossibleValues(NanoCSMT smt, int size, string prefix);
        /// <summary>
        /// Checks if the <paramref name="offset"/> is included in the index.
        /// </summary>
        /// <param name="offset">Offset to check for.</param>
        /// <returns><see langword="true"/> iff the <paramref name="offset"/> is included in the current index.</returns>
        public abstract bool IncludesOffset(int offset);
        /// <summary>
        /// Checks if the current index is a subindex of the <paramref name="other"/> index.
        /// </summary>
        /// <param name="other">Other index to check against.</param>
        /// <returns><see langword="true"/> iff the index values are included in the <paramref name="other"/> index.</returns>
        public abstract bool SubIndex(Index other);
        /// <inheritdoc/>
        public abstract object Clone();
        /// <inheritdoc/>
        public virtual Index Replace(Substitutor sub) => (Index)Clone();
        #region Operators and Helper functions
        /// <summary>
        /// Checks if the <paramref name="lhs"/> is a subindex of the <paramref name="rhs"/>.
        /// </summary>
        /// <param name="lhs">Left hand operand.</param>
        /// <param name="rhs">Right hand operand.</param>
        /// <returns><see langword="true"/> iff <paramref name="lhs"/> is a subindex of <paramref name="rhs"/>.</returns>
        public static bool operator <=(Index lhs, Index rhs) => lhs == rhs || lhs.SubIndex(rhs);
        /// <summary>
        /// Checks if the <paramref name="rhs"/> is a subindex of the <paramref name="lhs"/>.
        /// </summary>
        /// <param name="rhs">Left hand operand.</param>
        /// <param name="lhs">Right hand operand.</param>
        /// <returns><see langword="true"/> iff <paramref name="rhs"/> is a subindex of <paramref name="lhs"/>.</returns>
        public static bool operator >=(Index lhs, Index rhs) => lhs == rhs || rhs.SubIndex(lhs);
        /// <summary>
        /// Creates a new index that is a superset of all values the addition operation of <paramref name="left"/> and <paramref name="right"/> can assume.
        /// </summary>
        /// <param name="left">Index to add.</param>
        /// <param name="right">Index to add.</param>
        /// <returns>All posible values of the addition.</returns>
        public static Index operator +(Index left, Index right) => left.Add(right);
        /// <summary>
        /// Creates a new index that is a superset of all values the subtraction operation of <paramref name="left"/> and <paramref name="right"/> can assume.
        /// </summary>
        /// <param name="left">Index to add.</param>
        /// <param name="right">Index to add.</param>
        /// <returns>All posible values of the subtraction.</returns>
        public static Index operator -(Index left, Index right) => left.Sub(right);
        /// <summary>
        /// Creates a new index that is a superset of all values the addition operation of <see langword="this"/> and <paramref name="right"/> can assume.
        /// </summary>
        /// <param name="right">Index to add.</param>
        /// <returns>All posible values of the addition.</returns>
        protected abstract Index Add(Index right);
        /// <summary>
        /// Creates a new index that is a superset of all values the subtraction operation of <see langword="this"/> and <paramref name="right"/> can assume.
        /// </summary>
        /// <param name="right">Index to add.</param>
        /// <returns>All posible values of the subtraction.</returns>
        protected abstract Index Sub(Index right);
        #endregion
    }
}