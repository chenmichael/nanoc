﻿using Microsoft.Z3;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for sequence indices.
    /// </summary>
    public class SequenceIndex : Index, IEquatable<SequenceIndex?> {
        /// <summary>
        /// Creates a new instance of a sequence index that refers to a sequence of offsets starting from <paramref name="offset"/> with increments of <paramref name="step"/>.
        /// </summary>
        /// <param name="offset">Start offset of the sequence.</param>
        /// <param name="step">Increments between the sequence offsets.</param>
        public SequenceIndex(int offset, int step) {
            Offset = offset;
            Step = step <= 0 ? throw new ArgumentException("Step must be non-negative!") : step;
        }
        /// <summary>
        /// Start offset of the sequence.
        /// </summary>
        public int Offset { get; }
        /// <summary>
        /// Increments between the sequence offsets.
        /// </summary>
        public int Step { get; }
        /// <inheritdoc/>
        public override bool CollidesWith(int size, Index index, int otherSize) => index switch {
            // Handling Seq cap Sin, also redirected from Sin cap Seq
            SingletonIndex i => i.Offset + otherSize > Offset && base.CollidesWith(size, index, otherSize), // No overlap if the first index is earlier in the block than the sequence with enough offset
            _ => base.CollidesWith(size, index, otherSize)
        };
        /// <inheritdoc/>
        public override (BoolExpr guard, IntExpr value) SMTPossibleValues(NanoCSMT smt, int size, string prefix) {
            var x = smt.Context.MkIntConst($"{prefix}x");
            var i = smt.Context.MkIntConst($"{prefix}i");
            return (smt.Context.MkAnd(i >= 0, i < smt.Context.MkInt(size), x >= 0), (IntExpr)(smt.Context.MkInt(Offset) + i + (x * smt.Context.MkInt(Step))));
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return Offset.ToString();
            if (args.SpaceBetweenIndexOffsetAndPlus) yield return " ";
            yield return "+";
            if (args.SpaceBetweenIndexPlusAndStep) yield return " ";
            yield return Step.ToString();
        }
        /// <inheritdoc/>
        public override bool SubIndex(Index other) => other switch {
            ArbitraryIndex _ => true,
            SingletonIndex _ => false,
            SequenceIndex i => i.IncludesOffset(Offset) && (Step % i.Step == 0),
            _ => throw new IllFormedException(this, $"Cannot check for subindex with index of type {other.GetType().Name}!")
        };
        /// <inheritdoc/>
        public override bool IncludesOffset(int offset) => (offset >= Offset) && ((offset - Offset) % Step == 0);
        /// <inheritdoc/>
        protected override Index Add(Index right) => right switch {
            SingletonIndex i => new SequenceIndex(Offset + i.Offset, Step),
            SequenceIndex i => new SequenceIndex(Offset + i.Offset, GCD(Step, i.Step)),
            _ => new ArbitraryIndex()
        };
        /// <inheritdoc/>
        protected override Index Sub(Index right) => right switch {
            SingletonIndex i => new SequenceIndex(Offset - i.Offset, Step),
            _ => new ArbitraryIndex()
        };
        /// <summary>
        /// Greatest Common Denominator using Euclidian Algorithm.
        /// </summary>
        private static int GCD(int a, int b) {
            while (b != 0) b = a % (a = b);
            return a;
        }
        /// <inheritdoc/>
        public override object Clone() => new SequenceIndex(Offset, Step);
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as SequenceIndex);
        /// <inheritdoc/>
        public bool Equals(SequenceIndex? other) => !(other is null) && Offset == other.Offset && Step == other.Step;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Offset, Step);
        /// <inheritdoc/>
        public static bool operator ==(SequenceIndex? left, SequenceIndex? right) => EqualityComparer<SequenceIndex?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(SequenceIndex? left, SequenceIndex? right) => !(left == right);
        #endregion
    }
}