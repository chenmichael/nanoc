﻿using Microsoft.Z3;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for singleton indices.
    /// </summary>
    public class SingletonIndex : Index, IEquatable<SingletonIndex?> {
        /// <summary>
        /// Creates a new instance of a singleton index that refers to one single <paramref name="offset"/>.
        /// </summary>
        /// <param name="offset">Offset that is referred to.</param>
        public SingletonIndex(int offset) {
            Offset = offset;
        }
        /// <summary>
        /// Offset that is referred to.
        /// </summary>
        public int Offset { get; }
        /// <inheritdoc/>
        public override bool CollidesWith(int size, Index index, int otherSize) => index switch {
            // Overlapping of half opened intervals [a, b) cap [c, d)
            // ==> a < d && b < c
            SingletonIndex i => Offset < i.Offset + otherSize && i.Offset < Offset + size,
            // Sin cap Seq shall be handled by Seq cap Sin
            SequenceIndex i => i.CollidesWith(otherSize, this, size),
            _ => base.CollidesWith(size, index, otherSize)
        };
        /// <inheritdoc/>
        public override (BoolExpr guard, IntExpr value) SMTPossibleValues(NanoCSMT smt, int size, string prefix) {
            var i = smt.Context.MkIntConst($"{prefix}i");
            return (smt.Context.MkAnd(i >= 0, i < smt.Context.MkInt(size)), (IntExpr)(smt.Context.MkInt(Offset) + i));
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return Offset.ToString();
        }
        /// <inheritdoc/>
        public override bool IncludesOffset(int offset) => offset == Offset;
        /// <inheritdoc/>
        public override bool SubIndex(Index other) => other.IncludesOffset(Offset);
        /// <inheritdoc/>
        protected override Index Add(Index right) => right switch {
            SingletonIndex i => new SingletonIndex(Offset + i.Offset),
            SequenceIndex i => new SequenceIndex(Offset + i.Offset, i.Step),
            _ => new ArbitraryIndex()
        };
        /// <inheritdoc/>
        protected override Index Sub(Index right) => right switch {
            SingletonIndex i => new SingletonIndex(Offset - i.Offset),
            _ => new ArbitraryIndex()
        };
        /// <inheritdoc/>
        public override object Clone() => new SingletonIndex(Offset);
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as SingletonIndex);
        /// <inheritdoc/>
        public bool Equals(SingletonIndex? other) => !(other is null) && Offset == other.Offset;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Offset);
        /// <inheritdoc/>
        public static bool operator ==(SingletonIndex? left, SingletonIndex? right) => EqualityComparer<SingletonIndex?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(SingletonIndex? left, SingletonIndex? right) => !(left == right);
        #endregion
    }
}