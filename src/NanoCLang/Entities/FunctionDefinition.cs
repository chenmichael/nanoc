﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for function definitions.
    /// </summary>
    public class FunctionDefinition : Base, IEquatable<FunctionDefinition?> {
        /// <summary>
        /// Creates a new instance of a function definition with a specific <paramref name="schema"/> and a <paramref name="body"/>.
        /// </summary>
        /// <param name="body">Expression body of the function.</param>
        /// <param name="schema">Schema of the function.</param>
        public FunctionDefinition(Expression body, RawFunctionSchema schema) {
            Body = body;
            Schema = schema;
        }
        /// <summary>
        /// Expression that the function evaluates to.
        /// </summary>
        public Expression Body { get; }
        /// <summary>
        /// Schema of the function.
        /// </summary>
        public RawFunctionSchema Schema { get; }
        /// <summary>
        /// Checks if the function definition is well-formed and raises an exception if it is ill-formed.
        /// </summary>
        /// <param name="phi">Environemt to check the function definition with.</param>
        /// <param name="name">Name of the function to be defined.</param>
        /// <exception cref="IllFormedException">Function definition is ill-formed.</exception>
        public void WellFormed(GlobalEnvironment phi, string name) {
            if (!Schema.HasWorld && Body.RequiredFunctions().Contains(name))
                throw new IllFormedException(this, $"Cannot infer worlds of recursive function {name}. Please specify the world!");
            var schema = Schema.Build(phi, out var links);

            schema.WellFormed();
            var gamma = schema.GetEnvironment();
            gamma.AddStructLocs(links);
            World inferredWorld;
            //try {
            inferredWorld = Body.InferWorld(phi, gamma, schema.Heap);
            //} catch (IllFormedException e) {
            //    throw new IllFormedException(this, $"In function {name}: body is ill-formed!", e);
            //}
            var world = Schema.FixWorld(inferredWorld).Build(phi);

            var domain = inferredWorld.Heap.Domain().Concat(schema.Heap.Domain()).Concat(world.Heap.Domain()).ToHashSet();
            var h = inferredWorld.Heap.Filter(l => domain.Contains(l));
            if (h != world.Heap)
                throw new IllFormedException(world, "Expression heap does not match schema!");
            var h0 = inferredWorld.Heap.Filter(l => !domain.Contains(l));
            //world.WellFormed(gamma);
            h0.WellFormed(gamma);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "fun";
            if (args.SpaceAfterFunKeyword) yield return " ";
            yield return "(";
            if (args.FunctionParamNamesInDefinition)
                foreach (var tk in StringFormatterToken.Separated(Schema.Parameters.Select(i => i.Name), args.ParameterListSeparator)) yield return tk;
            if (args.SpaceInEmptyArgList && Schema.Parameters.Length == 0) yield return " ";
            yield return ")";
            if (args.NewlineBeforeFunctionBody) {
                yield return new NewLineToken();
                if (args.IndentFunctionBodyOpenBrace) yield return new string(' ', args.FunctionBodyIndent);
            } else if (args.SpaceBeforeFunctionBody) yield return " ";
            yield return "{";
            foreach (var tk in StringFormatterToken.Indented(args.FunctionBodyIndent, () => Body.Tokens(args).Prepend(new NewLineToken()))) yield return tk;
            if (args.NewlineBeforeFunctionBodyCloseBrace) {
                yield return new NewLineToken();
                if (args.IndentFunctionBodyCloseBrace) yield return new string(' ', args.FunctionBodyIndent);
            } else if (args.SpaceBeforeFunctionBodyCloseBrace) yield return " ";
            yield return "}";
            if (args.NewlineBeforeFunctionSchemaColon) {
                yield return new NewLineToken();
                if (args.IndentFunctionSchemaColon) yield return new string(' ', args.FunctionSchemaIndent);
            } else if (args.SpaceBeforeFunctionSchemaColon) yield return " ";
            yield return ":";
            if (args.NewlineAfterFunctionSchemaColon) {
                yield return new NewLineToken();
                if (args.IndentFunctionSchema) yield return new string(' ', args.FunctionSchemaIndent);
            } else if (args.SpaceAfterFunctionSchemaColon) yield return " ";
            foreach (var tk in StringFormatterToken.Indented(args.FunctionSchemaIndent, () => Schema.Tokens(args))) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            yield return "(";
            foreach (var tk in StringFormatterToken.Separated(Schema.Parameters, args, ", ")) yield return tk;
            yield return ")";
            yield return " ";
            yield return "{";
            args.PureRequiresReturn = true;
            foreach (var tk in StringFormatterToken.Indented(4, () => Body.Tokens(args).Prepend(new NewLineToken()))) yield return tk;
            yield return new NewLineToken();
            yield return "}";
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as FunctionDefinition);
        /// <inheritdoc/>
        public bool Equals(FunctionDefinition? other) => !(other is null) && EqualityComparer<Expression>.Default.Equals(Body, other.Body) && EqualityComparer<RawFunctionSchema>.Default.Equals(Schema, other.Schema);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Body, Schema);
        /// <inheritdoc/>
        public static bool operator ==(FunctionDefinition? left, FunctionDefinition? right) => EqualityComparer<FunctionDefinition?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(FunctionDefinition? left, FunctionDefinition? right) => !(left == right);
        #endregion
    }
}
