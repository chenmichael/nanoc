﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for structure definitions.
    /// </summary>
    public class StructDefinition : Base, IEquatable<StructDefinition?>, ICallable {
        /// <summary>
        /// Creates a new instance of a structure definition from a list of structure fields.
        /// </summary>
        /// <param name="locations">Additional locations that the structure operates on.</param>
        /// <param name="args">Parameters that the structure invariant depends on.</param>
        /// <param name="dependencies">Calls to other structs that the structure depends on.</param>
        /// <param name="fields">Fields of the structure.</param>
        public StructDefinition(string[] locations, TypeParameter[] args, FunctionCallExpression[] dependencies, StructField[] fields) {
            Locations = locations;
            Parameters = args;
            Dependencies = dependencies;
            Fields = fields;
        }
        /// <summary>
        /// Additional locations that the struct operates on.
        /// </summary>
        public string[] Locations { get; }
        /// <summary>
        /// Parameters that the structure invariant depends on.
        /// </summary>
        public TypeParameter[] Parameters { get; }
        /// <summary>
        /// List of structs that the struct depends on.
        /// </summary>
        public FunctionCallExpression[] Dependencies { get; }
        /// <summary>
        /// Member fields of the structure.
        /// </summary>
        public StructField[] Fields { get; }
        /// <summary>
        /// Checks if the structure definition is well-formed and raises an exception if it is ill-formed.
        /// </summary>
        /// <param name="phi">Environemt to check the structure definition with.</param>
        /// <exception cref="IllFormedException">structure definition is ill-formed.</exception>
        public void WellFormed(GlobalEnvironment phi) {
            var gamma = new LocalEnvironment();
            var emptyHeap = new Heap();
            foreach (var param in Parameters) {
                // check with empty heap because structures cannot depend on heaps that are external
                param.Type.WellFormed(gamma, emptyHeap);
                if (!gamma.TryAdd(param.Name, param.Type))
                    throw new IllFormedException(param, $"Structure parameters must have distinct names: {param.Name} is already used!");
            }
            var collision = Fields.Select(i => i.Name).Intersect(Parameters.Select(i => i.Name)).ToList();
            if (collision.Any())
                throw new IllFormedException(this, $"Cannot declare fields with name(s) {string.Join(", ", collision)}, because there are already parameters with the same name(s)!");
            var fieldcollision = Fields.GroupBy(i => i.Name).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
            if (fieldcollision.Any())
                throw new IllFormedException(this, $"Cannot declare fields with name(s) {string.Join(", ", fieldcollision)}, because there are already other fields with the same name(s) in the same structure!");

            foreach (var dep in Dependencies) dep.GetBindings(phi, out _);
        }
        /// <summary>
        /// Gets a substitutor object that replaces variables that refer to struct members in the heap with their block offset in the same location.
        /// </summary>
        /// <returns>Heap substitutor.</returns>
        public Substitutor GetSubstitutor() {
            var theta = new Substitutor();
            foreach (var field in Fields) {
                if (theta.Variables.ContainsKey(field.Name))
                    throw new IllFormedException(field, $"Cannot use field name {field.Name} twice in one structure!");
                else if (field.Binding.Index is SingletonIndex i)
                    theta.Variables[field.Name] = new VariableExpression(Heap.OffsetVar(i.Offset));
            }
            return theta;
        }

        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            // LBC (fields += structField)+ RBC COLON LBA locations = identifierList RBA
            yield return "struct";
            if (args.SpaceAfterFunKeyword) yield return " ";
            yield return "(";
            foreach (var tk in StringFormatterToken.Separated(Parameters, args, args.ParameterListSeparator)) yield return tk;
            if (args.SpaceInEmptyArgList && Parameters.Length == 0) yield return " ";
            yield return ")";
            if (Dependencies.Length > 0) {
                yield return " ";
                yield return ":";
                yield return " ";
                foreach (var tk in StringFormatterToken.Separated(Dependencies, args, args.ParameterListSeparator)) yield return tk;
            }
            if (args.NewlineBeforeFunctionBody) {
                yield return new NewLineToken();
                if (args.IndentFunctionBodyOpenBrace) yield return new string(' ', args.FunctionBodyIndent);
            } else if (args.SpaceBeforeFunctionBody) yield return " ";
            yield return "{";
            foreach (var tk in StringFormatterToken.Indented(args.FunctionBodyIndent, () => Fields.SelectMany(i => i.Tokens(args).Prepend(new NewLineToken())))) yield return tk;
            if (args.NewlineBeforeFunctionBodyCloseBrace) {
                yield return new NewLineToken();
                if (args.IndentFunctionBodyCloseBrace) yield return new string(' ', args.FunctionBodyIndent);
            } else if (args.SpaceBeforeFunctionBodyCloseBrace) yield return " ";
            yield return "}";
            if (args.NewlineBeforeFunctionSchemaColon) {
                yield return new NewLineToken();
                if (args.IndentFunctionSchemaColon) yield return new string(' ', args.FunctionSchemaIndent);
            } else if (args.SpaceBeforeFunctionSchemaColon) yield return " ";
            yield return ":";
            if (args.NewlineAfterFunctionSchemaColon) {
                yield return new NewLineToken();
                if (args.IndentFunctionSchema) yield return new string(' ', args.FunctionSchemaIndent);
            } else if (args.SpaceAfterFunctionSchemaColon) yield return " ";
            yield return "[";
            foreach (var tk in StringFormatterToken.Separated(Locations, args.ParameterListSeparator)) yield return tk;
            yield return "]";
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as StructDefinition);
        /// <inheritdoc/>
        public bool Equals(StructDefinition? other) => other != null && Locations.SequenceEqual(other.Locations) && Parameters.SequenceEqual(other.Parameters) && Dependencies.SequenceEqual(other.Dependencies) && Fields.SequenceEqual(other.Fields);
        /// <inheritdoc/>
        public override int GetHashCode() {
            var hash = new HashCode();
            foreach (var i in Locations) hash.Add(i);
            foreach (var i in Parameters) hash.Add(i);
            foreach (var i in Dependencies) hash.Add(i);
            foreach (var i in Fields) hash.Add(i);
            return hash.ToHashCode();
        }
        /// <inheritdoc/>
        public static bool operator ==(StructDefinition? left, StructDefinition? right) => EqualityComparer<StructDefinition?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(StructDefinition? left, StructDefinition? right) => !(left == right);
        #endregion
    }
}
