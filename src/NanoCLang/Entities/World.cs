﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for worlds.
    /// </summary>
    public class RawWorld : Base, IEquatable<RawWorld?> {
        /// <summary>
        /// Creates a new world instance that gives an expression a return <paramref name="type"/> and a modified <paramref name="heap"/>.
        /// </summary>
        /// <param name="type">Type of the world.</param>
        /// <param name="heap">Modified heap of the world.</param>
        public RawWorld(Type type, IHeapElement[] heap) {
            Type = type;
            Heap = heap;
        }
        /// <summary>
        /// Type of the world.
        /// </summary>
        public Type Type { get; }
        /// <summary>
        /// Modified heap of the world.
        /// </summary>
        public IHeapElement[] Heap { get; }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Type.Tokens(args)) yield return tk;
            foreach (var tk in WorldOperator(args)) yield return tk;
            foreach (var tk in Heap.Tokens(args)) yield return tk;
        }
        /// <summary>
        /// Print the world operator
        /// </summary>
        /// <param name="args">Formatter arguments</param>
        /// <returns>Token enumeration</returns>
        internal static IEnumerable<StringFormatterToken> WorldOperator(NanoCSourceFormat args) {
            for (int i = 0; i < args.NewlinesBeforeWorldOperator; i++) yield return new NewLineToken();
            if (args.NewlinesBeforeWorldOperator <= 0 && args.SpaceBeforeWorldOperator) yield return " ";
            yield return "/";
            for (int i = 0; i < args.NewlinesAfterWorldOperator; i++) yield return new NewLineToken();
            if (args.NewlinesAfterWorldOperator <= 0 && args.SpaceAfterWorldOperator) yield return " ";
        }
        /// <summary>
        /// Creates the world represented by the raw world by expanding structures with the environment <paramref name="phi"/>.
        /// </summary>
        /// <param name="phi">Global environment to expand structures with.</param>
        /// <returns>Built world.</returns>
        public World Build(GlobalEnvironment phi) => new World(Type, Heap.GenerateHeap(phi));
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as RawWorld);
        /// <inheritdoc/>
        public bool Equals(RawWorld? other) => !(other is null) && EqualityComparer<Type>.Default.Equals(Type, other.Type) && HeapElementComparer.Default.Equals(Heap, other.Heap);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Type, Heap);
        /// <inheritdoc/>
        public static bool operator ==(RawWorld? left, RawWorld? right) => EqualityComparer<RawWorld?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(RawWorld? left, RawWorld? right) => !(left == right);
        #endregion
    }
    /// <summary>
    /// Provides a class for worlds.
    /// </summary>
    public class World : Base, IEquatable<World?> {
        /// <summary>
        /// Creates a new world instance that gives an expression a return <paramref name="type"/> and a modified <paramref name="heap"/>.
        /// </summary>
        /// <param name="type">Type of the world.</param>
        /// <param name="heap">Modified heap of the world.</param>
        public World(Type type, Heap heap) {
            Type = type;
            Heap = heap;
        }
        /// <summary>
        /// Implicitly converts a built world into a raw world (trivial).
        /// </summary>
        /// <param name="world">World to convert.</param>
        public static implicit operator RawWorld(World world) => new RawWorld(world.Type, new IHeapElement[] { world.Heap });
        /// <summary>
        /// Type of the world.
        /// </summary>
        public Type Type { get; }
        /// <summary>
        /// Modified heap of the world.
        /// </summary>
        public Heap Heap { get; }
        /// <summary>
        /// Checks if the world is well-formed and raises an exception if it is ill-formed.
        /// </summary>
        /// <exception cref="IllFormedException">World is ill-formed.</exception>
        public void WellFormed(LocalEnvironment gamma) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "WF-World");
            Heap.WellFormed(gamma);
            Type.WellFormed(gamma, Heap);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Type.Tokens(args)) yield return tk;
            foreach (var tk in RawWorld.WorldOperator(args)) yield return tk;
            foreach (var tk in Heap.Tokens(args)) yield return tk;
        }
        /// <summary>
        /// Checks if the current world is a subworld of the <paramref name="other"/> world in the given environment.
        /// </summary>
        /// <param name="gamma">Environment to check the subworld in.</param>
        /// <param name="other">Other world to check against.</param>
        /// <returns><see langword="true"/> iff the current world is a subworld of the <paramref name="other"/> world.</returns>
        public bool SubWorld(LocalEnvironment gamma, World other) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "<:-World");
            return Type.SubType(gamma, other.Type) && Heap.SubHeap(gamma, other.Heap);
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as World);
        /// <inheritdoc/>
        public bool Equals(World? other) => !(other is null) && EqualityComparer<Type>.Default.Equals(Type, other.Type) && EqualityComparer<Heap>.Default.Equals(Heap, other.Heap);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Type, Heap);
        /// <inheritdoc/>
        public static bool operator ==(World? left, World? right) => EqualityComparer<World?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(World? left, World? right) => !(left == right);
        #endregion
    }
}