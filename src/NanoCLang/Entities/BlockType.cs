﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for blocks in a <see cref="LocationBinding"/>.
    /// </summary>
    public class BlockType : Base, IEquatable<BlockType?>, ISubstitutable<BlockType> {
        /// <summary>
        /// Creates a new instance of a block type that maps the indices to the given <paramref name="type"/>.
        /// </summary>
        /// <param name="index">Indices of the <paramref name="type"/> in the block.</param>
        /// <param name="type">Type of the allocation.</param>
        public BlockType(Index index, Type type) {
            Index = index;
            Type = type;
        }
        /// <summary>
        /// Valid offset(s) within the block.
        /// </summary>
        public Index Index { get; }
        /// <summary>
        /// Gets or sets the type of the allocation at the <see cref="Index"/>.
        /// </summary>
        public Type Type { get; internal set; }
        /// <summary>
        /// Gets a new block with the refinements set to true (being types replaced with their base types).
        /// </summary>
        public BlockType BaseType => new BlockType((Index)Index.Clone(), (Type)Type.BaseType.Clone());
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Index.Tokens(args)) yield return tk;
            if (args.SpaceBeforeTypeOperator) yield return " ";
            yield return ":";
            if (args.SpaceAfterTypeOperator) yield return " ";
            foreach (var tk in Type.Tokens(args)) yield return tk;
        }
        /// <summary>
        /// Checks if the current block is a subtype of the <paramref name="other"/> block in the given environment.
        /// </summary>
        /// <param name="gamma">Environment to check the subtype in.</param>
        /// <param name="other">Other block to check against.</param>
        /// <returns><see langword="true"/> iff the current block is a subtype of the <paramref name="other"/> type.</returns>
        public bool SubBlock(LocalEnvironment gamma, BlockType other)
            => Type.SubType(gamma, other.Type);
        /// <inheritdoc/>
        public BlockType Replace(Substitutor sub)
            => new BlockType((Index)Index.Clone(), Type.Replace(sub));
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as BlockType);
        /// <inheritdoc/>
        public bool Equals(BlockType? other) => !(other is null) && EqualityComparer<Index>.Default.Equals(Index, other.Index) && EqualityComparer<Type>.Default.Equals(Type, other.Type);
        /// <inheritdoc/>
        public override int GetHashCode() => System.HashCode.Combine(Index, Type);
        /// <inheritdoc/>
        public static bool operator ==(BlockType? left, BlockType? right) => EqualityComparer<BlockType?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(BlockType? left, BlockType? right) => !(left == right);
        #endregion
    }
}