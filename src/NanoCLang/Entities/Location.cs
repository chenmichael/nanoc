﻿using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for location entities.
    /// </summary>
    public class Location : Base, IEquatable<Location?>, ISubstitutable<Location>, ICloneable {
        /// <summary>
        /// Creates a new location instance with the given <paramref name="name"/>. <paramref name="isAbstract"/> specifies if the concrete or abstract location is referred to.
        /// </summary>
        /// <param name="name">Name of the location.</param>
        /// <param name="isAbstract"><c>true</c> iff the location is abstract, else concrete.</param>
        public Location(string name, bool isAbstract) {
            Name = name;
            Abstract = isAbstract;
        }
        /// <summary>
        /// Name of the location.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// <c>true</c> iff the location is abstract, else <see langword="false"/>.
        /// </summary>
        public bool Abstract { get; }
        /// <summary>
        /// <c>true</c> iff the location is concrete, else <see langword="false"/>.
        /// </summary>
        public bool Concrete => !Abstract;
        /// <inheritdoc/>
        public object Clone() => new Location(Name, Abstract);
        /// <inheritdoc/>
        public Location Replace(Substitutor rep)
            => rep.Locations.TryGetValue(Name, out var subs)
            ? new Location(subs, Abstract)
            : (Location)Clone();
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as Location);
        /// <inheritdoc/>
        public bool Equals(Location? other) => !(other is null) && Name == other.Name && Abstract == other.Abstract;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Name, Abstract);
        /// <inheritdoc/>
        public static bool operator ==(Location? left, Location? right) => EqualityComparer<Location?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(Location? left, Location? right) => !(left == right);
        #endregion
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            if (Abstract) yield return "~";
            yield return Name;
        }
    }
}