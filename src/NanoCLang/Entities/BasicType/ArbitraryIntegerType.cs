﻿namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class of integers of arbitrary size.
    /// </summary>
    public abstract class ArbitraryIntegerType : BasicType { }
}