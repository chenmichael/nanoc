﻿using NanoCLang.Environemnts;
using System;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a superclass for all basic type objects.
    /// </summary>
    public abstract class BasicType : Type, ICloneable {
        /// <inheritdoc/>
        public override BasicType BaseType => this;
        /// <inheritdoc/>
        public override bool ProperSubType(LocalEnvironment gamma, Type other) {
            return other switch {
                BasicType o => ProperBasicSubType(o),
                // Base type can only be a subtype of a refined type if the refined type has a trivially true refinement
                RefinedType o => gamma.With("v", BaseType, gamma => NanoCSMT.Default.TryEvalConstExpression(gamma, o.Refinement) is bool b && b),
                _ => throw new InvalidProgramException("Expected basic or refined type here!")
            };
        }
        private bool ProperBasicSubType(BasicType other) {
            switch (BaseType) {
            case IntegerType t when other.BaseType is IntegerType o && t.Size == o.Size && t.Values <= o.Values:
                VerbConsole.WriteLine(VerbosityLevel.Default, $"<:-Int: {this} <: {other}");
                return true;
            case ReferenceType t when other.BaseType is ReferenceType o && t.Location == o.Location && t.Offsets <= o.Offsets:
                VerbConsole.WriteLine(VerbosityLevel.Default, $"<:-Ref: {this} <: {other}");
                return true;
            case IntegerType t when t.Values == Index.Zero && other.BaseType is ReferenceType o && o.Abstract:
                VerbConsole.WriteLine(VerbosityLevel.Default, $"<:-NullPtr: {this} <: {other}");
                return true;
            case ReferenceType t when t.Concrete && other.BaseType is ReferenceType o && o.Abstract && (Base)t.Offsets == o.Offsets:
                VerbConsole.WriteLine(VerbosityLevel.Default, $"<:-Abstract: {this} <: {other}");
                return true;
            default: return false;
            }
        }
        /// <inheritdoc/>
        public abstract object Clone();
        /// <inheritdoc/>
        public override Type Replace(Substitutor rep) => (Type)Clone();
        /// <inheritdoc/>
        public override PureExpression Refinement => new BooleanConstant(true);
        /// <summary>
        /// Calculates the closest related ancestor of both basic type and returns it.
        /// Both basic types are a subtype of the return value.
        /// </summary>
        /// <param name="baseType1">First input type.</param>
        /// <param name="baseType2">Second input type.</param>
        /// <returns>Supertype of both if they are compatible.</returns>
        /// <exception cref="IllFormedException">Incompatible basic types.</exception>
        public static BasicType SuperType(BasicType baseType1, BasicType baseType2)
            => baseType1 == baseType2
            ? baseType1
            : (baseType1.ProperBasicSubType(baseType2)
                ? baseType2
                : (baseType2.ProperBasicSubType(baseType1)
                    ? baseType1
                    : throw new IllFormedException(baseType1, $"Incompatible base types {baseType1} and {baseType2}!")));
    }
}