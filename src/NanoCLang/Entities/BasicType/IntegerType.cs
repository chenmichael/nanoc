﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for NanoC integer types.
    /// </summary>
    public class IntegerType : ArbitraryIntegerType, IEquatable<IntegerType?> {
        /// <summary>
        /// Creates a new instance of a NanoC integer type.
        /// </summary>
        /// <param name="size">The size in bytes of the type.</param>
        /// <param name="values">The index of valid values.</param>
        public IntegerType(int size, Index values) {
            Size = size;
            Values = values;
        }
        /// <summary>
        /// Creates a new instance of a NanoC integer type with arbitrary index.
        /// </summary>
        /// <param name="size">The size in bytes of the type.</param>
        public IntegerType(int size) : this(size, new ArbitraryIndex()) { }
        /// <summary>
        /// Size of the type in bytes.
        /// </summary>
        public override int Size { get; }
        /// <summary>
        /// Index of allowed values for the integer.
        /// </summary>
        public Index Values { get; }
        /// <summary>
        /// Checks if the type is a void type.
        /// </summary>
        public bool IsVoid => Size == 0 && Values == Index.Zero;
        /// <summary>
        /// Returns a new void type.
        /// </summary>
        public static IntegerType Void => new IntegerType(0, new SingletonIndex(0));
        /// <summary>
        /// Returns a new boolean type.
        /// </summary>
        public static IntegerType Boolean => new IntegerType(1, new SequenceIndex(0, 1));
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Int: {this}");
            if (Size < 0)
                throw new IllFormedException(this, $"Integer cannot have negative size {Size}!");
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            switch (Size) {
            case 0 when Values == Index.Zero: yield return "void"; break;
            case 1 when Values is ArbitraryIndex: yield return "char"; break;
            case 4 when Values is ArbitraryIndex: yield return "int"; break;
            default:
                yield return Size.ToString();
                yield return "[";
                foreach (var tk in Values.Tokens(args)) yield return tk;
                yield return "]";
                break;
            }
        }
        /// <inheritdoc/>
        public override object Clone() => new IntegerType(Size, (Index)Values.Clone());
        internal static StringFormatterToken GetCType(int size) => size switch {
            0 => "void",
            1 => "int8_t",
            2 => "int16_t",
            4 => "int32_t",
            8 => "int64_t",
            _ => throw new InvalidProgramException($"Cannot declare a {size}-Byte integer in C!"),
        };
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) { yield return GetCType(Size); }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as IntegerType);
        /// <inheritdoc/>
        public bool Equals(IntegerType? other) => !(other is null) && Size == other.Size && EqualityComparer<Index>.Default.Equals(Values, other.Values);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Size, Values);
        /// <inheritdoc/>
        public static bool operator ==(IntegerType? left, IntegerType? right) => EqualityComparer<IntegerType?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(IntegerType? left, IntegerType? right) => !(left == right);
        #endregion
    }
}