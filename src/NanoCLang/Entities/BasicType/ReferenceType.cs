﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for NanoC reference types.
    /// </summary>
    public class ReferenceType : BasicType, IEquatable<ReferenceType?> {
        /// <summary>
        /// Creates a new instance of a reference type.
        /// </summary>
        /// <param name="location">Location that the reference refers to.</param>
        /// <param name="offsets">Offset at this <paramref name="location"/>.</param>
        public ReferenceType(Location location, Index offsets) {
            Location = location;
            Offsets = offsets;
        }
        /// <summary>
        /// Location that the type refers to.
        /// </summary>
        public Location Location { get; }
        /// <summary>
        /// Offset of the reference at the given <see cref="Location"/>.
        /// </summary>
        public Index Offsets { get; }
        /// <inheritdoc/>
        public override int Size => PointerSize;
        /// <inheritdoc cref="Size"/>
        public static int PointerSize { get; set; } = 8;
        /// <inheritdoc/>
        public override object Clone() => new ReferenceType((Location)Location.Clone(), (Index)Offsets.Clone());
        /// <inheritdoc/>
        public override Type Replace(Substitutor sub)
            => new ReferenceType(Location.Replace(sub), Offsets.Replace(sub));
        /// <summary>
        /// <c>true</c> iff the reference points to an abstract location, else <see langword="false"/>.
        /// </summary>
        public bool Abstract => Location.Abstract;
        /// <summary>
        /// <c>true</c> iff the reference points to an concrete location, else <see langword="false"/>.
        /// </summary>
        public bool Concrete => Location.Concrete;
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Ref: {this}");
            if (!heap.ContainsAbstract(Location.Name))
                throw new IllFormedException(Location, $"Cannot refer to invalid location {Location}!");
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "ref";
            if (args.SpaceAfterFunctionName) yield return " ";
            yield return "(";
            foreach (var tk in Location.Tokens(args)) yield return tk;
            yield return args.ParameterListSeparator;
            foreach (var tk in Offsets.Tokens(args)) yield return tk;
            yield return ")";
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            yield return "int8_t";
            yield return "*";
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as ReferenceType);
        /// <inheritdoc/>
        public bool Equals(ReferenceType? other) => !(other is null) && EqualityComparer<Location>.Default.Equals(Location, other.Location) && EqualityComparer<Index>.Default.Equals(Offsets, other.Offsets);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Location, Offsets);
        /// <inheritdoc/>
        public static bool operator ==(ReferenceType? left, ReferenceType? right) => EqualityComparer<ReferenceType?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(ReferenceType? left, ReferenceType? right) => !(left == right);
        #endregion
    }
}