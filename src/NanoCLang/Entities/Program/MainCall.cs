﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for the main call.
    /// </summary>
    public class MainCall : Program, IEquatable<MainCall?> {
        /// <summary>
        /// Creates a new main call that calls the main function named <paramref name="name"/> without parameters.
        /// </summary>
        /// <param name="name">Name of the main function.</param>
        public MainCall(string name) {
            Name = name;
        }
        /// <summary>
        /// Name of the main function.
        /// </summary>
        public string Name { get; }
        /// <inheritdoc/>
        public override World WellFormed(GlobalEnvironment phi) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-MainCall: {Name}");
            if (!phi.TryGetValue(Name, out var rawschema))
                throw new IllFormedException(this, $"Main call to unknown function {Name}!");
            var schema = rawschema.Build(phi);
            if (!schema.Heap.IsAbstract())
                throw new IllFormedException(this, $"Main call is invalid because {Name} operates on concrete locations!");
            return schema.World;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return Name;
            if (args.SpaceAfterFunctionName) yield return " ";
            yield return "(";
            if (args.SpaceInEmptyArgList) yield return " ";
            yield return ")";
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) => Name is null ? Enumerable.Empty<StringFormatterToken>()
            : Program.PrintHeader(args).Concat(new FunctionBinding(
                "main",
                new FunctionDefinition(
                    new ConcatenationExpression(new FunctionCallExpression(Name, new string[] { }),
                        new IntegerConstant(0, 4)),
                    new RawFunctionSchema(
                        new string[] { },
                        new TypeParameter[] { },
                        new IHeapElement[] { },
                        new RawWorld(new IntegerType(4), new IHeapElement[] { }))), new MainCall(null!)).Tokens(args));
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as MainCall);
        /// <inheritdoc/>
        public bool Equals(MainCall? other) => !(other is null) && Name == other.Name;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Name);
        /// <inheritdoc/>
        public static bool operator ==(MainCall? left, MainCall? right) => EqualityComparer<MainCall?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(MainCall? left, MainCall? right) => !(left == right);
        #endregion
    }
}