﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for function bindings.
    /// </summary>
    public class FunctionBinding : Program, IEquatable<FunctionBinding?> {
        /// <summary>
        /// Creates a new instance of a function binding that binds the <paramref name="definition"/> to the function <paramref name="name"/> in the <paramref name="body"/>.
        /// </summary>
        /// <param name="name">Name that the function is bound to.</param>
        /// <param name="definition">Definition of the function.</param>
        /// <param name="body">Program that the function name is bound in.</param>
        public FunctionBinding(string name, FunctionDefinition definition, Program body) {
            Name = name;
            Definition = definition;
            Body = body;
        }
        /// <summary>
        /// Definition of the function.
        /// </summary>
        public FunctionDefinition Definition { get; }
        /// <summary>
        /// Program that the function name is bound in.
        /// </summary>
        public Program Body { get; }
        /// <summary>
        /// Name that the function is bound to.
        /// </summary>
        public string Name { get; }
        /// <inheritdoc/>
        public override World WellFormed(GlobalEnvironment phi) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-FunctionBinding: {Name}");
            return phi.With(this, phi => {
                Definition.WellFormed(phi, Name);
                return Body.WellFormed(phi);
            });
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "letf";
            yield return " ";
            yield return Name;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Definition.Tokens(args)) yield return tk;
            yield return " ";
            yield return "in";
            if (Body is null) yield break;
            if (args.NewlinesAfterFunctionBinding <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesAfterFunctionBinding; i++) yield return new NewLineToken();
            foreach (var tk in Body.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            foreach(var tk in Program.PrintHeader(args)) yield return tk;
            foreach (var tk in Definition.Schema.World.Type.Tokens(args)) yield return tk;
            yield return " ";
            yield return Name;
            foreach (var tk in Definition.Tokens(args)) yield return tk;
            if (Body is null) yield break;
            for (int i = 0; i < args.NewlinesAfterFunctionBinding; i++)
                yield return new NewLineToken();
            if (!(Body is null)) foreach (var tk in Body.Tokens(args)) yield return tk;
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as FunctionBinding);
        /// <inheritdoc/>
        public bool Equals(FunctionBinding? other) => !(other is null) && EqualityComparer<FunctionDefinition>.Default.Equals(Definition, other.Definition) && EqualityComparer<Program>.Default.Equals(Body, other.Body) && Name == other.Name;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Definition, Body, Name);
        /// <inheritdoc/>
        public static bool operator ==(FunctionBinding? left, FunctionBinding? right) => EqualityComparer<FunctionBinding?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(FunctionBinding? left, FunctionBinding? right) => !(left == right);
        #endregion
    }
}
