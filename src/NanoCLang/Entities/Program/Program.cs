﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a superclass for all program entites.
    /// </summary>
    public abstract class Program : Base {
        /// <summary>
        /// Creates the default environment that programs are type checked with.
        /// This may contain NanoC support functions or standard libraries as well as includes from functions.
        /// </summary>
        public GlobalEnvironment DefaultEnvironemt => new GlobalEnvironment();
        /// <summary>
        /// Checks if the rogram is well-formed and raises an exception if it is ill-formed.
        /// If it is well-formed it returns the type of the program.
        /// Uses the <see cref="DefaultEnvironemt"/> to check the program.
        /// </summary>
        /// <exception cref="IllFormedException">Program is ill-formed.</exception>
        /// <returns>Type of the program entity.</returns>
        public virtual World WellFormed() => WellFormed(DefaultEnvironemt);
        /// <summary>
        /// Checks if the program is well-formed and raises an exception if it is ill-formed.
        /// </summary>
        /// <param name="phi">Environemt to check the program with.</param>
        /// <exception cref="IllFormedException">Program is ill-formed.</exception>
        /// <returns>Type of the program entity.</returns>
        public abstract World WellFormed(GlobalEnvironment phi);
        private static readonly string[] includes = new string[] {
            "stdlib.h" /* allocation expressions */,
            "stdint.h" /* fixed width integer types */,
        };
        /// <summary>
        /// Print the program header if it is not yet printed.
        /// </summary>
        /// <param name="args">Arguments that include whether the header was already printed.</param>
        /// <returns>Tokens that represent the header.</returns>
        protected static IEnumerable<StringFormatterToken> PrintHeader(CSourceFormat args) {
            if (args.HeaderPrinted) yield break;
            foreach (var include in includes) yield return $"#include <{include}>\n";
            yield return "\n";
            args.HeaderPrinted = true;
        }
    }
}
