﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for structure bindings.
    /// </summary>
    public class StructBinding : Program, IEquatable<StructBinding?> {
        /// <summary>
        /// Creates a new instance of a structure binding that binds the <paramref name="definition"/> to the structure <paramref name="name"/> in the <paramref name="body"/>.
        /// </summary>
        /// <param name="name">Name that the structure is bound to.</param>
        /// <param name="definition">Definition of the structure.</param>
        /// <param name="body">Program that the structure name is bound in.</param>
        public StructBinding(string name, StructDefinition definition, Program body) {
            Name = name;
            Definition = definition;
            Body = body;
        }
        /// <summary>
        /// Definition of the structure.
        /// </summary>
        public StructDefinition Definition { get; }
        /// <summary>
        /// Program that the structure name is bound in.
        /// </summary>
        public Program Body { get; }
        /// <summary>
        /// Name that the structure is bound to.
        /// </summary>
        public string Name { get; }
        /// <inheritdoc/>
        public override World WellFormed(GlobalEnvironment phi) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-StructBinding: {Name}");
            return phi.With(this, phi => {
                Definition.WellFormed(phi);
                return Body.WellFormed(phi);
            });
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "lets";
            yield return " ";
            yield return Name;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Definition.Tokens(args)) yield return tk;
            yield return " ";
            yield return "in";
            if (args.NewlinesAfterStructBinding <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesAfterStructBinding; i++) yield return new NewLineToken();
            foreach (var tk in Body.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            foreach (var tk in Program.PrintHeader(args)) yield return tk;
            foreach (var tk in Body.Tokens(args)) yield return tk;
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as StructBinding);
        /// <inheritdoc/>
        public bool Equals(StructBinding? other) => !(other is null) && EqualityComparer<StructDefinition>.Default.Equals(Definition, other.Definition) && EqualityComparer<Program>.Default.Equals(Body, other.Body) && Name == other.Name;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Definition, Body, Name);
        /// <inheritdoc/>
        public static bool operator ==(StructBinding? left, StructBinding? right) => EqualityComparer<StructBinding?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(StructBinding? left, StructBinding? right) => !(left == right);
        #endregion
    }
}
