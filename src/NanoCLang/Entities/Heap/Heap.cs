using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for heap entities.
    /// Also trivially implements the heap element interface.
    /// </summary>
    [DebuggerDisplay("{" + nameof(ToString) + "(),nq}")]
    public class Heap : Base, ISubstitutable<Heap>, IEquatable<Heap?>, IHeapElement, ICloneable {
        /// <summary>
        /// Creates a new heap instance from the list of location bindings.
        /// </summary>
        /// <param name="locations">Locations that are bound in the heap.</param>
        /// <exception cref="IllFormedException">Locations are not uniquely defined.</exception>
        public Heap(IEnumerable<LocationBinding> locations) : this(locations.ToList()) { }
        /// <inheritdoc cref="Heap(IEnumerable{LocationBinding})"/>
        public Heap(IList<LocationBinding> locations) {
            var duplicates = locations.GroupBy(i => i.Location)
              .Where(g => g.Count() > 1)
              .Select(y => y.Key).ToList();
            if (duplicates.Count > 0) {
                var firstDuplicate = duplicates.First();
                throw new IllFormedException(firstDuplicate, $"Cannot bind to location {firstDuplicate} twice in a heap!");
            }
            locationBindings = locations.ToDictionary(i => i.Location, i => i.Blocks);
        }
        /// <summary>
        /// Checks if the heap has a binding for the <paramref name="location"/>.
        /// </summary>
        /// <param name="location">Location to check for.</param>
        /// <returns><see langword="true"/> iff there exists a matching location binding.</returns>
        public bool Contains(Location location) => locationBindings.ContainsKey(location);
        /// <summary>
        /// Gets an iterator for the domain of the heap. I.e. the set of locations bound in the heap.
        /// </summary>
        /// <returns>Iterator over the locations.</returns>
        public IEnumerable<Location> Domain() => locationBindings.Keys;
        /// <summary>
        /// Performs a strong update to the location and offset appointed by the reference <paramref name="ptrRef"/>.
        /// Writes the <paramref name="value"/> to the location inside the new heap.
        /// Additionally performs a typecheck to verify that the written type matches current base type (refinement does not need to be checked for strong updating).
        /// </summary>
        /// <param name="ptrRef">Reference pointing to location and block offset for strong update.</param>
        /// <param name="value">Value expression to be written to the heap.</param>
        /// <param name="tau">Base type of the type that was bound at the written location. Can be typechecked against in the caller.</param>
        /// <returns>New heap that contains the strong update.</returns>
        public Heap StrongUpdate(ReferenceType ptrRef, PureExpression value, out BasicType tau) {
            var heap = (Heap)Clone();
            var bindings = heap.TryGetBinding(ptrRef.Location, out var tmp) ? tmp
                : throw new IllFormedException(ptrRef, $"Cannot access pointer to location {ptrRef.Location} if the location is not bound in the heap!");

            BlockType? bound = null;
            switch (ptrRef.Offsets) {
            case SingletonIndex i:
                VerbConsole.WriteLine(VerbosityLevel.Default, "T-Write-Field");
                foreach (var block in bindings) if (i <= block.Index) { bound = block; break; }
                if (bound is null)
                    throw new IllFormedException(this, $"Heap location does not have a writable field at index {i}!");
                bound.Type = new RefinedType(bound.Type.BaseType, v => PureExpression.EqualExpression(v, value));
                break;
            case SequenceIndex i:
                VerbConsole.WriteLine(VerbosityLevel.Default, "T-Write-Array");
                foreach (var block in bindings) if (i <= block.Index) { bound = block; break; }
                if (bound is null)
                    throw new IllFormedException(this, $"Heap location does not have a writable field at index {i}!");
                break;
            default: throw new InvalidProgramException($"Expected valid index but got {ptrRef.Offsets}");
            }
            tau = bound.Type.BaseType;
            return heap;
        }
        /// <summary>
        /// Clones the heap and returns the clone.
        /// </summary>
        /// <returns>Clone of the current heap.</returns>
        public object Clone() => Replace(new Substitutor());
        /// <summary>
        /// Gets a heap that is a super heap of both input heaps.
        /// </summary>
        /// <param name="gamma">Environment to compare heaps within.</param>
        /// <param name="heap1">First input heap.</param>
        /// <param name="heap2">Second input heap.</param>
        /// <returns>Superheap of both heaps.</returns>
        /// <exception cref="IllFormedException">Heaps are incomatible.</exception>
        public static Heap SuperHeap(LocalEnvironment gamma, Heap heap1, Heap heap2)
        => heap1.SubHeap(gamma, heap2)
                        ? heap2
                        : heap2.SubHeap(gamma, heap1)
                            ? heap1
                            : throw new IllFormedException(heap1, $"Incompatible branch heaps in if expression: {heap2} and {heap1}!");
        /// <summary>
        /// Checks if the heap has a binding for the <paramref name="location"/> and returns the binding at that location.
        /// </summary>
        /// <param name="location">Location to check for.</param>
        /// <param name="binding">Binding at that location.</param>
        /// <returns><see langword="true"/> iff there exists a matching location binding.</returns>
        public bool TryGetBinding(Location location, [NotNullWhen(true)] out BlockType[]? binding)
            => locationBindings.TryGetValue(location, out binding);
        /// <summary>
        /// Creates a new instance of an empty heap.
        /// </summary>
        public Heap() : this(new LocationBinding[] { }) { }
        /// <summary>
        /// Creates a new instance of a heap with a single location <paramref name="binding"/>.
        /// </summary>
        /// <param name="binding">Single binding on the heap (if not abstract the heap can not be well formed).</param>
        public Heap(LocationBinding binding) : this(new LocationBinding[] { binding }) { }
        /// <summary>
        /// Create a heap from a location -> blocktype[] enumeration.
        /// </summary>
        /// <param name="heap">Input to copy from.</param>
        public Heap(IEnumerable<KeyValuePair<Location, BlockType[]>> heap) {
            locationBindings = new Dictionary<Location, BlockType[]>(heap);
        }
        /// <summary>
        /// Create a heap from an existing heap.
        /// </summary>
        /// <param name="heap">Input to copy from.</param>
        public Heap(Heap heap) : this(heap.locationBindings) { }
        /// <summary>
        /// Locations that are bound in the heap.
        /// Same as <see cref="GetBindings(GlobalEnvironment, out IEnumerable{KeyValuePair{string, string}})"/> except that heap does not require a global environment to expand structures anymore.
        /// </summary>
        public IEnumerable<LocationBinding> BoundLocations => locationBindings.Select(i => new LocationBinding(i.Key, i.Value));
        /// <inheritdoc cref="IHeapElement.GetBindings(GlobalEnvironment, out IEnumerable{KeyValuePair{string, string}})"/>
        public IEnumerable<LocationBinding> GetBindings(GlobalEnvironment phi, out IEnumerable<KeyValuePair<string, string>> links) {
            links = Enumerable.Empty<KeyValuePair<string, string>>();
            return BoundLocations;
        }
        /// <summary>
        /// Checks if the heap is empty and returns <see langword="true"/> iff it is.
        /// </summary>
        public bool Empty => locationBindings.Count == 0;
        private readonly IDictionary<Location, BlockType[]> locationBindings;
        /// <summary>
        /// Checks if the heap contains purely abstract bindings.
        /// </summary>
        public bool IsAbstract() => locationBindings.All(i => i.Key.Abstract);
        /// <summary>
        /// Create a heap from the current heap with locations that the <paramref name="predicate"/> evaluates to <see langword="true"/> for.
        /// </summary>
        /// <param name="predicate">Predicate to apply to the locations.</param>
        public Heap Filter(Func<Location, bool> predicate) => new Heap(locationBindings.Where(i => predicate(i.Key)));
        /// <summary>
        /// Calculated the heap after subtracting the <paramref name="rhs"/> from the <paramref name="lhs"/>.
        /// </summary>
        /// <param name="lhs">Heap to subtract from.</param>
        /// <param name="rhs">Heap that is subtracted from the <paramref name="lhs"/>.</param>
        /// <returns><see langword="true"/> iff the rest was calculated.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="lhs"/> or <paramref name="rhs"/> is <see langword="null"/>.</exception>
        /// <exception cref="InvalidOperationException"><paramref name="rhs"/> is not a sub-heap of <paramref name="lhs"/>.</exception>
        public static Heap operator /(Heap lhs, Heap rhs) {
            // bad core: keys in subheap that are not in the "super"-heap
            var badCore = rhs.locationBindings.Keys.Except(lhs.locationBindings.Keys).ToList();
            if (badCore.Count > 0) {
                var l = badCore.First();
                throw new InvalidOperationException($"Heap is not a subheap: {l} is not bound in left heap!");
            }
            // union core: keys in both heaps, needed to check for equality of bindings
            var intersectionCore = rhs.locationBindings.Keys.ToHashSet();
            var intersection = lhs.Filter(i => intersectionCore.Contains(i));
            if (intersection != rhs)
                throw new InvalidOperationException($"Heap is not a subheap: Key Intersection is not consistend!");
            // good core: keys only in the super-heap
            var goodCore = lhs.locationBindings.Keys.Except(rhs.locationBindings.Keys).ToHashSet();
            return lhs.Filter(i => goodCore.Contains(i));
        }
        /// <summary>
        /// Try to get the rest of the heap after subtracting the <paramref name="heap"/> from this heap.
        /// If the input is a sub-heap of this heap the difference is calculated as <paramref name="rest"/> <see langword="true"/> returned,
        /// otherwise the function returns <see langword="false"/>.
        /// </summary>
        /// <param name="heap">Heap that is subtracted from this heap.</param>
        /// <param name="rest">Difference between the heaps, or <see langword="null"/></param>
        /// <exception cref="ArgumentNullException"><paramref name="heap"/> is <see langword="null"/>.</exception>
        /// <returns><see langword="true"/> iff the rest was calculated.</returns>
        public bool TryGetRest(Heap heap, [NotNullWhen(true)] out Heap? rest) {
            try {
                rest = this / heap;
                return true;
            } catch (InvalidOperationException) { rest = null; }
            return false;
        }
        /// <summary>
        /// Checks if the current heap is a subheap of the <paramref name="other"/> heap in the given environment.
        /// </summary>
        /// <param name="gamma">Environment to check the subheap in.</param>
        /// <param name="other">Other heap to check against.</param>
        /// <returns><see langword="true"/> iff the current heap is a subheap of the <paramref name="other"/> heap.</returns>
        public bool SubHeap(LocalEnvironment gamma, Heap other) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "<:-Heap");
            if (locationBindings.Count != other.locationBindings.Count) return false;
            var keys = locationBindings.Keys.ToHashSet();
            if (!keys.SetEquals(other.locationBindings.Keys.ToHashSet())) return false;
            foreach (var key in keys)
                if (!SubBlock(gamma, locationBindings[key], other.locationBindings[key])) return false;
            return true;
        }
        /// <summary>
        /// Checks if the block <paramref name="left"/> is a subtype of the <paramref name="right"/> block in the given environment.
        /// </summary>
        /// <param name="gamma">Environment to check the subtype in.</param>
        /// <param name="left">Current block to check with.</param>
        /// <param name="right">Other block to check against.</param>
        /// <returns><see langword="true"/> iff the current block is a subtype of the <paramref name="right"/> type.</returns>
        public static bool SubBlock(LocalEnvironment gamma, BlockType[] left, BlockType[] right) {
            var gammaOut = new LocalEnvironment(gamma);
            if (left.Length != right.Length) return false;
            for (int i = 0; i < left.Length; i++) {
                var b1 = left[i];
                var b2 = right[i];
                if (b1.Index != b2.Index) return false;
                if (!b1.Type.SubType(gammaOut, b2.Type)) return false;
                if (b1.Index is SingletonIndex idx) {
                    VerbConsole.WriteLine(VerbosityLevel.Default, "<:-Field");
                    gammaOut[OffsetVar(idx.Offset)] = b1.Type;
                } else VerbConsole.WriteLine(VerbosityLevel.Default, "<:-Array");
            }
            VerbConsole.WriteLine(VerbosityLevel.Default, "<:-Block-Empty");
            return true;
        }
        /// <summary>
        /// Checks if the heap is well-formed and raises an exception if it is ill-formed.
        /// </summary>
        /// <param name="gamma">Environemt to check the heap with.</param>
        /// <exception cref="IllFormedException">Heap is ill-formed.</exception>
        public void WellFormed(LocalEnvironment gamma) {
            if (Empty) VerbConsole.WriteLine(VerbosityLevel.Default, "WF-Empty");
            foreach (var locationBinding in locationBindings) {
                var l = locationBinding.Key;
                var b = locationBinding.Value;
                if (l.Abstract) {
                    VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Abstract: {l}");
                    // Check ~l not already on the heap (currenlty enforced by design)
                    WellFormedAbstract(gamma, b);
                } else {
                    VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Concrete: {l}");
                    // Check l_j not already on the heap (currenlty enforced by design)
                    if (!ContainsAbstract(l.Name))
                        throw new IllFormedException(l, $"Heap must contain binding to abstract location {l.Name}!");
                    WellFormedConcrete(gamma, b);
                }
            }
        }
        /// <summary>
        /// Checks if the heap contains a binding to the abstract location named <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Name of the location to check.</param>
        /// <returns><see langword="true"/> iff the abstract location exists.</returns>
        public bool ContainsAbstract(string name) => locationBindings.ContainsKey(new Location(name, true));
        /// <summary>
        /// Checks if the heap contains a binding to the concrete location named <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Name of the location to check.</param>
        /// <returns><see langword="true"/> iff the concrete location exists.</returns>
        public bool ContainsConcrete(string name) => locationBindings.ContainsKey(new Location(name, false));
        /// <summary>
        /// Check if the abstract block <paramref name="b"/> is well formed in the environment <paramref name="gammaIn"/>.
        /// </summary>
        /// <param name="gammaIn">Environment to check the block in.</param>
        /// <param name="b">Block to check.</param>
        private void WellFormedAbstract(LocalEnvironment gammaIn, BlockType[] b) {
            var gamma = new LocalEnvironment(gammaIn);
            for (int i = 0; i < b.Length; i++) {
                var binding = b[i];
                binding.Type.WellFormed(gamma, this);
                CheckIndexOverlap(b, i);
                switch (binding.Index) {
                case SingletonIndex idx:
                    VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Field: {binding.Type}");
                    gamma[OffsetVar(idx.Offset)] = binding.Type;
                    break;
                default:
                    VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Array: {binding.Type}");
                    break;
                }
            }
        }
        /// <summary>
        /// Gets a string that represents the variable containing the block offset in the environment.
        /// </summary>
        /// <param name="offset">Offset to represent.</param>
        /// <returns>Variable name</returns>
        public static string OffsetVar(int offset) => $"@{offset}";
        /// <summary>
        /// Check if the index of the current binding overlaps with previous bindings in the location.
        /// </summary>
        /// <param name="b">Array of bindings in the location.</param>
        /// <param name="i">Index of current binding in the location</param>
        private static void CheckIndexOverlap(BlockType[] b, int i) {
            var binding = b[i];
            var bindingSize = binding.Type.Size;
            for (int j = 0; j < i; j++) {
                var domB = b[j];
                try {
                    if (binding.Index.CollidesWith(bindingSize, domB.Index, domB.Type.Size))
                        throw new IllFormedException(binding.Index, $"Index collides with binding at {domB.Index}!");
                } catch (NotImplementedException e) {
                    throw new IllFormedException(binding.Index, $"Cannot safely reason about the index overlapping. Details in the inner exception!", e);
                }
            }
        }
        /// <summary>
        /// Check if the concrete block <paramref name="b"/> is well formed in the environment <paramref name="gamma"/>.
        /// </summary>
        /// <param name="gamma">Environment to check the block in.</param>
        /// <param name="b">Block to check.</param>
        private void WellFormedConcrete(LocalEnvironment gamma, BlockType[] b) {
            for (int i = 0; i < b.Length; i++) {
                var binding = b[i];
                VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-ConcBlock: {binding.Type}");
                binding.Type.WellFormed(gamma, this);
                CheckIndexOverlap(b, i);
            }
        }
        /// <inheritdoc/>
        public Heap Replace(Substitutor sub)
            => new Heap(locationBindings.Select(i
                => new KeyValuePair<Location, BlockType[]>(
                    i.Key.Replace(sub),
                    i.Value.Select(i => i.Replace(sub)).ToArray())));
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            var locations = BoundLocations.GetEnumerator();
            if (!locations.MoveNext()) { yield return "emp"; yield break; }
            foreach (var tk in locations.Current.Tokens(args)) yield return tk;
            while (locations.MoveNext())
                foreach (var tk in StringFormatterToken.Indented(args.IndentHeapListOperator,
                    () => PrintLocationBinding(args, locations.Current))) yield return tk;
        }

        private static IEnumerable<StringFormatterToken> PrintLocationBinding(NanoCSourceFormat args, LocationBinding location) {
            for (int i = 0; i < args.NewlinesBeforeBindingListOperator; i++) yield return new NewLineToken();
            yield return args.HeapBindingListSeparator;
            foreach (var tk in location.Tokens(args)) yield return tk;
        }
        /// <summary>
        /// Concatenates the <paramref name="left"/> and <paramref name="right"/> heap and returns
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns>Returns the concatenated heap.</returns>
        /// <exception cref="ArgumentException">Heap bindings overlap.</exception>
        public static Heap operator *(Heap left, Heap right) {
            var intersection = left.locationBindings.Keys.Intersect(right.locationBindings.Keys).ToList();
            if (intersection.Count > 0)
                throw new ArgumentException($"Heaps overlap in locations {string.Join(", ", intersection)}!");
            return new Heap(left.locationBindings.Concat(right.locationBindings));
        }
        /// <summary>
        /// Adds the location <paramref name="binding"/> to the <paramref name="heap"/>.
        /// </summary>
        /// <param name="heap">Heap to add to.</param>
        /// <param name="binding">Location binding to add.</param>
        /// <returns>Returns the concatenated heap.</returns>
        /// <exception cref="ArgumentException">Heap bindings overlap.</exception>
        public static Heap operator *(Heap heap, LocationBinding binding) {
            if (heap.locationBindings.Keys.Contains(binding.Location))
                throw new ArgumentException($"Cannot append binding to {string.Join(", ", binding.Location)}!");
            return new Heap(heap.locationBindings.Append(new KeyValuePair<Location, BlockType[]>(binding.Location, binding.Blocks)));
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as Heap);
        /// <inheritdoc/>
        public bool Equals(Heap? other) => !(other is null) && HeapComparer.Default.Equals(locationBindings, other.locationBindings);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(locationBindings);
        /// <inheritdoc/>
        public static bool operator ==(Heap? left, Heap? right) => EqualityComparer<Heap?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(Heap? left, Heap? right) => !(left == right);
        #endregion
    }
}