﻿using NanoCLang.Environemnts;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides an interface that makes heap elements available to the heap constructor.
    /// If new objects inherit from this interface the function <see cref="HeapElementComparer.Equals(IHeapElement[], IHeapElement[])"/> must be updated!
    /// </summary>
    public interface IHeapElement : ITokenStream<StringFormatterToken, NanoCSourceFormat> {
        /// <summary>
        /// Generates the available location bindings for the heap constructor from the current environment.
        /// </summary>
        /// <param name="phi">Global environment that supplies the structure definitions.</param>
        /// <param name="links">Structure location links.</param>
        /// <returns>Enumeration of generated heap bindings.</returns>
        IEnumerable<LocationBinding> GetBindings(GlobalEnvironment phi, out IEnumerable<KeyValuePair<string, string>> links);
    }
    /// <summary>
    /// Provides an extension class for heap element enumerations.
    /// </summary>
    public static class HeapElementExtensions {
        /// <summary>
        /// Generates a new heap from the list of heap generators.
        /// </summary>
        /// <param name="elems">Heap generators.</param>
        /// <param name="phi">Environment to generate with.</param>
        /// <param name="links">Structure location links.</param>
        /// <returns>New heap with all bindings.</returns>
        /// <exception cref="IllFormedException">Heap is not well formed.</exception>
        public static Heap GenerateHeap(this IEnumerable<IHeapElement> elems, GlobalEnvironment phi, out IEnumerable<KeyValuePair<string, string>> links) {
            var linkList = new List<KeyValuePair<string, string>>();
            var heapBindings = new List<LocationBinding>();
            foreach (var elem in elems) {
                heapBindings.AddRange(elem.GetBindings(phi, out var newLinks));
                linkList.AddRange(newLinks);
            }
            links = linkList;
            return new Heap(heapBindings);
        }
        /// <inheritdoc cref="GenerateHeap(IEnumerable{IHeapElement}, GlobalEnvironment, out IEnumerable{KeyValuePair{string, string}})"/>
        public static Heap GenerateHeap(this IEnumerable<IHeapElement> elems, GlobalEnvironment phi)
            => new Heap(elems.SelectMany(i => i.GetBindings(phi, out _)));
        /// <inheritdoc cref="ITokenStream{T, A}.Tokens(A)"/>
        public static IEnumerable<StringFormatterToken> Tokens(this IHeapElement[] elems, NanoCSourceFormat args)
        => elems.Length == 0
            ? Enumerable.Repeat<StringFormatterToken>("emp", 1)
            : StringFormatterToken.Separated(elems, args, args.HeapBindingListSeparator);
    }
}
