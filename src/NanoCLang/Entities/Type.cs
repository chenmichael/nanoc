﻿using NanoCLang.Environemnts;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a superclass for all NanoC types.
    /// May also include basic types that are used as refined types with trivial refinement.
    /// </summary>
    public abstract class Type : Base, ISubstitutable<Type> {
        /// <summary>
        /// Gets the size of the type in bytes.
        /// </summary>
        public abstract int Size { get; }
        /// <summary>
        /// Gets the underlying type of the current type.
        /// </summary>
        public abstract BasicType BaseType { get; }
        /// <summary>
        /// Gets a refinement that is valid for the type.
        /// </summary>
        public abstract PureExpression Refinement { get; }
        /// <summary>
        /// Checks if the type is well-formed and raises an exception if it is ill-formed.
        /// </summary>
        /// <exception cref="IllFormedException">Type is ill-formed.</exception>
        public abstract void WellFormed(LocalEnvironment gamma, Heap heap);
        /// <summary>
        /// Checks if the current type is a subtype of the <paramref name="other"/> type in the given environment.
        /// </summary>
        /// <param name="gamma">Environment to check the subtype in.</param>
        /// <param name="other">Other type to check against.</param>
        /// <returns><see langword="true"/> iff the current type is a subtype of the <paramref name="other"/> type.</returns>
        public virtual bool SubType(LocalEnvironment gamma, Type other) => this == other || ProperSubType(gamma, other);
        /// <summary>
        /// Checks if the current type is a proper subtype of the <paramref name="other"/> type in the given environment.
        /// </summary>
        /// <param name="gamma">Environment to check the subtype in.</param>
        /// <param name="other">Other type to check against.</param>
        /// <returns><see langword="true"/> iff the current type is a proper subtype of the <paramref name="other"/> type.</returns>
        public abstract bool ProperSubType(LocalEnvironment gamma, Type other);
        /// <inheritdoc/>
        public abstract Type Replace(Substitutor sub);
        /// <inheritdoc/>
        public abstract override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args);
    }
}