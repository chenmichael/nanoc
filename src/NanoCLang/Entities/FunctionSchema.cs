﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for a function schema that contains unbuilt heaps.
    /// </summary>
    public class RawFunctionSchema : Base, IEquatable<RawFunctionSchema?>, ICallable {
        /// <summary>
        /// Creates a new instance of a function schema.
        /// </summary>
        /// <param name="locations">Locations that the function operates on.</param>
        /// <param name="parameters">Parameters and their types that the function expects.</param>
        /// <param name="heap">Input heap that the function modifies.</param>
        /// <param name="world">World being the return type and heap of the function or null to infer the world.</param>
        public RawFunctionSchema(string[] locations, TypeParameter[] parameters, IHeapElement[] heap, RawWorld? world) {
            Locations = locations;
            Parameters = parameters;
            Heap = heap;
            this.world = world;
        }
        /// <summary>
        /// Locations that the function operates on.
        /// </summary>
        public string[] Locations { get; }
        /// <summary>
        /// Parameters and their types that the function expects.
        /// </summary>
        public TypeParameter[] Parameters { get; }
        /// <summary>
        /// Input heap that the function modifies.
        /// </summary>
        public IHeapElement[] Heap { get; }
        /// <summary>
        /// World being the return type and heap of the function.
        /// </summary>
        public RawWorld World => world ?? throw new InvalidOperationException("World of the function is unknown!");
        private RawWorld? world;
        /// <summary>
        /// Fix the world to the schema after inferring.
        /// </summary>
        /// <param name="world">World to set to the schema.</param>
        public RawWorld FixWorld(RawWorld world) {
            if (this.world is null)
                VerbConsole.WriteLine(VerbosityLevel.Default, $"Schema world was inferred as {world}!");
            return this.world ??= world;
        }
        /// <summary>
        /// Gets a boolean that shows if the schema was already assigned a world by the parser or the inferrer.
        /// </summary>
        public bool HasWorld => !(world is null);
        /// <summary>
        /// Gets the arity of the function, being the number of expected parameters.
        /// </summary>
        public int Arity => Parameters.Length;
        /// <summary>
        /// Creates the function schema represented by the raw schema by expanding structures with the environment <paramref name="phi"/>.
        /// </summary>
        /// <param name="phi">Global environment to expand structures with.</param>
        /// <param name="links">Structure location links.</param>
        /// <returns>Built function schema.</returns>
        public FunctionSchema Build(GlobalEnvironment phi, out IEnumerable<KeyValuePair<string, string>> links) => new FunctionSchema(Locations, Parameters, Heap.GenerateHeap(phi, out links), world?.Build(phi));
        /// <inheritdoc cref="Build(GlobalEnvironment, out IEnumerable{KeyValuePair{string, string}})"/>
        public FunctionSchema Build(GlobalEnvironment phi) => new FunctionSchema(Locations, Parameters, Heap.GenerateHeap(phi, out _), world?.Build(phi));
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "[";
            foreach (var tk in StringFormatterToken.Separated(Locations, args.ParameterListSeparator)) yield return tk;
            yield return "]";
            if (args.NewlinesBeforeFunctionParams <= 0 && args.SpaceBetweenLocationsAndFunctionParams) yield return " ";
            for (int i = 0; i < args.NewlinesBeforeFunctionParams; i++) yield return new NewLineToken();
            yield return "(";
            foreach (var tk in StringFormatterToken.Separated(Parameters, args, args.ParameterListSeparator)) yield return tk;
            if (args.SpaceInEmptyArgList && Parameters.Length == 0) yield return " ";
            yield return ")";
            foreach (var tk in RawWorld.WorldOperator(args)) yield return tk;
            foreach (var tk in Heap.Tokens(args)) yield return tk;
            if (world is null) yield break;
            for (int i = 0; i < args.NewlinesBeforeFunctionWorld; i++) yield return new NewLineToken();
            if (args.NewlinesBeforeFunctionWorld <= 0 && args.SpaceBeforeFunctionWorld) yield return " ";
            yield return "->";
            for (int i = 0; i < args.NewlinesAfterFunctionWorld; i++) yield return new NewLineToken();
            if (args.NewlinesAfterFunctionWorld <= 0 && args.SpaceAfterFunctionWorld) yield return " ";
            foreach (var tk in world.Tokens(args)) yield return tk;
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as RawFunctionSchema);
        /// <inheritdoc/>
        public bool Equals(RawFunctionSchema? other) => !(other is null) && Locations.SequenceEqual(other.Locations) && Parameters.SequenceEqual(other.Parameters) && HeapElementComparer.Default.Equals(Heap, other.Heap) && EqualityComparer<RawWorld?>.Default.Equals(World, other.World);
        /// <inheritdoc/>
        public override int GetHashCode() {
            var hash = new HashCode();
            foreach (var loc in Locations) hash.Add(loc);
            foreach (var param in Parameters) hash.Add(param);
            foreach (var heapelem in Heap) hash.Add(heapelem);
            hash.Add(World);
            return hash.ToHashCode();
        }
        /// <inheritdoc/>
        public static bool operator ==(RawFunctionSchema? left, RawFunctionSchema? right) => EqualityComparer<RawFunctionSchema?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(RawFunctionSchema? left, RawFunctionSchema? right) => !(left == right);
        #endregion
    }
    /// <summary>
    /// Provides a class for a function schema.
    /// </summary>
    public class FunctionSchema : Base, IEquatable<FunctionSchema?>, ICallable {
        /// <summary>
        /// Creates a new instance of a function schema.
        /// </summary>
        /// <param name="locations">Locations that the function operates on.</param>
        /// <param name="parameters">Parameters and their types that the function expects.</param>
        /// <param name="heap">Input heap that the function modifies.</param>
        /// <param name="world">World being the return type and heap of the function or null to infer the world.</param>   public FunctionSchema(string[] locations, TypeParameter[] parameters, Heap heap, World? world) {
        public FunctionSchema(string[] locations, TypeParameter[] parameters, Heap heap, World? world) {
            Locations = locations;
            Parameters = parameters;
            Heap = heap;
            this.world = world;
        }
        /// <summary>
        /// Locations that the function operates on.
        /// </summary>
        public string[] Locations { get; }
        /// <summary>
        /// Parameters and their types that the function expects.
        /// </summary>
        public TypeParameter[] Parameters { get; }
        /// <summary>
        /// Input heap that the function modifies.
        /// </summary>
        public Heap Heap { get; }
        /// <summary>
        /// World being the return type and heap of the function.
        /// </summary>
        public World World => world ?? throw new InvalidOperationException("World of the function is unknown!");
        private readonly World? world;
        /// <summary>
        /// Gets a boolean that shows if the schema was already assigned a world by the parser or the inferrer.
        /// </summary>
        public bool HasWorld => !(world is null);
        /// <summary>
        /// Gets the arity of the function, being the number of expected parameters.
        /// </summary>
        public int Arity => Parameters.Length;
        /// <summary>
        /// Checks if the function schema is well-formed and raises an exception if it is ill-formed.
        /// </summary>
        /// <exception cref="IllFormedException">Function schema is ill-formed.</exception>
        public void WellFormed() {
            VerbConsole.WriteLine(VerbosityLevel.Default, "WF-Schema");
            var gamma = new LocalEnvironment();
            foreach (var param in Parameters) {
                param.Type.WellFormed(gamma, Heap);
                if (!gamma.TryAdd(param.Name, param.Type))
                    throw new IllFormedException(param, $"Parameters must have distinct names: {param.Name} is already used!");
            }
            Heap.WellFormed(gamma);
            world?.WellFormed(gamma);
        }
        /// <inheritdoc cref="GetEnvironment(TypeParameter[])"/>
        public LocalEnvironment GetEnvironment()
            => new LocalEnvironment(Parameters.ToDictionary(i => i.Name, i => i.Type));
        /// <summary>
        /// Creates a new local environment from the type parameters that represent the bindings in the function body.
        /// </summary>
        /// <returns>Local environment of the function parameters.</returns>
        internal static LocalEnvironment GetEnvironment(TypeParameter[] parameters)
            => new LocalEnvironment(parameters.ToDictionary(i => i.Name, i => i.Type));
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "[";
            foreach (var tk in StringFormatterToken.Separated(Locations, args.ParameterListSeparator)) yield return tk;
            yield return "]";
            if (args.NewlinesBeforeFunctionParams <= 0 && args.SpaceBetweenLocationsAndFunctionParams) yield return " ";
            for (int i = 0; i < args.NewlinesBeforeFunctionParams; i++) yield return new NewLineToken();
            yield return "(";
            foreach (var tk in StringFormatterToken.Separated(Parameters, args, args.ParameterListSeparator)) yield return tk;
            if (args.SpaceInEmptyArgList && Parameters.Length == 0) yield return " ";
            yield return ")";
            foreach (var tk in RawWorld.WorldOperator(args)) yield return tk;
            foreach (var tk in Heap.Tokens(args)) yield return tk;
            if (world is null) yield break;
            for (int i = 0; i < args.NewlinesBeforeFunctionWorld; i++) yield return new NewLineToken();
            if (args.NewlinesBeforeFunctionWorld <= 0 && args.SpaceBeforeFunctionWorld) yield return " ";
            yield return "->";
            for (int i = 0; i < args.NewlinesAfterFunctionWorld; i++) yield return new NewLineToken();
            if (args.NewlinesAfterFunctionWorld <= 0 && args.SpaceAfterFunctionWorld) yield return " ";
            foreach (var tk in world.Tokens(args)) yield return tk;
        }
        #region Equality Checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as FunctionSchema);
        /// <inheritdoc/>
        public bool Equals(FunctionSchema? other) => other != null && Locations.SequenceEqual(other.Locations) && Parameters.SequenceEqual(other.Parameters) && EqualityComparer<Heap>.Default.Equals(Heap, other.Heap) && EqualityComparer<World?>.Default.Equals(world, other.world);
        /// <inheritdoc/>
        public override int GetHashCode() {
            var hash = new HashCode();
            foreach (var i in Locations) hash.Add(i);
            foreach (var i in Parameters) hash.Add(i);
            hash.Add(Heap);
            hash.Add(world);
            return hash.ToHashCode();
        }
        /// <inheritdoc/>
        public static bool operator ==(FunctionSchema? left, FunctionSchema? right) => EqualityComparer<FunctionSchema?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(FunctionSchema? left, FunctionSchema? right) => !(left == right);
        #endregion
    }
}
