﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for refined types.
    /// </summary>
    public class RefinedType : Type, IEquatable<RefinedType?> {
        /// <summary>
        /// Gets the default value that the refinement parameter uses.
        /// </summary>
        private const string DEFAULT_NAME = "v";
        /// <summary>
        /// Creates a new instance of a refined type that refines the <paramref name="baseType"/> with the <paramref name="refinement"/> replacing the <paramref name="name"/> in the refinement with the values.
        /// </summary>
        /// <param name="name">Name of the free variable that is used in the refinement.</param>
        /// <param name="baseType">Base type that is refined.</param>
        /// <param name="refinement">Refinement expression.</param>
        public RefinedType(string name, BasicType baseType, PureExpression refinement) {
            RefinementParameter = name;
            BaseType = baseType;
            Refinement = refinement;
        }
        /// <summary>
        /// Creates a new refined type from the <paramref name="refinement"/> function that gives the refinement with respect to the type variable.
        /// </summary>
        /// <param name="baseType">Base type that is refined.</param>
        /// <param name="refinement">Refinement function.</param>
        /// <param name="refineVar">Name of the free variable that is used in the refinement.</param>
        public RefinedType(BasicType baseType, Func<VariableExpression, PureExpression> refinement, string refineVar = DEFAULT_NAME)
            : this(refineVar, baseType, refinement(new VariableExpression(refineVar))) { }
        /// <summary>
        /// Creates a new refined type, that further refines the initial <paramref name="type"/>, from the <paramref name="refinement"/> function that gives the refinement with respect to the type variable.
        /// </summary>
        /// <param name="type">Refined type that is further refined.</param>
        /// <param name="refinement">Refinement function.</param>
        /// <param name="refineVar">Name of the free variable that is used in the refinement.</param>
        public RefinedType(Type type, Func<VariableExpression, PureExpression> refinement, string refineVar = DEFAULT_NAME)
            : this(refineVar, type.BaseType, type is RefinedType
                  ? refinement(new VariableExpression(refineVar)) & type.Refinement
                  : refinement(new VariableExpression(refineVar))) { }
        /// <summary>
        /// Name of the free variable that is used in the refinement.
        /// </summary>
        public string RefinementParameter { get; } // Constant v
        /// <summary>
        /// Base type that is refined.
        /// </summary>
        public override BasicType BaseType { get; }
        /// <summary>
        /// Refinement expression of the <see cref="RefinementParameter"/>.
        /// </summary>
        public override PureExpression Refinement { get; }
        /// <inheritdoc/>
        public override int Size => BaseType.Size;
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Type: {this}");
            BaseType.WellFormed(gamma, heap);
            gamma.With(RefinementParameter, BaseType, gamma => Refinement.WellFormed(gamma));
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "{";
            yield return RefinementParameter;
            if (args.SpaceBeforeTypeOperator) yield return " ";
            yield return ":";
            if (args.SpaceAfterTypeOperator) yield return " ";
            foreach (var tk in BaseType.Tokens(args)) yield return tk;
            if (args.SpaceBeforeRefineOperator) yield return " ";
            yield return "|";
            if (args.SpaceAfterRefineOperator) yield return " ";
            foreach (var tk in Refinement.Tokens(args)) yield return tk;
            yield return "}";
        }
        /// <inheritdoc/>
        public override bool ProperSubType(LocalEnvironment gamma, Type other) => BaseType.SubType(gamma, other.BaseType) && CheckRefinement(gamma, other);
        private bool CheckRefinement(LocalEnvironment gamma, Type other) {
            return other switch {
                BasicType _ => true,
                RefinedType refined => CheckRefinement(gamma, refined),
                _ => throw new InvalidProgramException("Expected basic type or refined type here!")
            };
        }
        private bool CheckRefinement(LocalEnvironment gamma, RefinedType refined) => Refinement == refined.Refinement || gamma.With("v", BaseType,
            gamma => NanoCSMT.Default.ValidImplication(gamma, Refinement, refined.Refinement));
        /// <inheritdoc/>
        public override Type Replace(Substitutor sub)
            => new RefinedType(RefinementParameter, (BasicType)BaseType.Replace(sub), Refinement.Replace(sub));
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) => BaseType.Tokens(args);
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as RefinedType);
        /// <inheritdoc/>
        public bool Equals(RefinedType? other) => !(other is null) && RefinementParameter == other.RefinementParameter && EqualityComparer<BasicType>.Default.Equals(BaseType, other.BaseType) && EqualityComparer<PureExpression>.Default.Equals(Refinement, other.Refinement);
        /// <inheritdoc/>
        public override int GetHashCode() => System.HashCode.Combine(RefinementParameter, BaseType, Refinement);
        /// <inheritdoc/>
        public static bool operator ==(RefinedType? left, RefinedType? right) => EqualityComparer<RefinedType?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(RefinedType? left, RefinedType? right) => !(left == right);
        #endregion
    }
}