using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides an interface that allows for variable expressions to be aliased.
    /// </summary>
    /// <typeparam name="T">Type of object to substitute in.</typeparam>
    public interface ISubstitutable<T> {
        /// <summary>
        /// Substitutes all entities in the object with the substitutor rules from <paramref name="sub"/> and returns a cloned object with substitutes..
        /// </summary>
        /// <param name="sub">Replacement partial function.</param>
        /// <returns>New object with replaced entities.</returns>
        T Replace(Substitutor sub);
    }
    /// <summary>
    /// Provides a class for substituting entities in abstract syntax trees.
    /// </summary>
    public class Substitutor {
        /// <summary>
        /// The location replacement dictionary. Replaces all location names, does not change abstract/concrete!
        /// </summary>
        public IDictionary<string, string> Locations { get; set; } = new Dictionary<string, string>();
        /// <summary>
        /// The variable replacement dictionary. Replaces all variables that are keys with the expression in the corresponding key-value.
        /// </summary>
        public IDictionary<string, PureExpression> Variables { get; set; } = new Dictionary<string, PureExpression>();
        /// <summary>
        /// The block offset replacement dictionary. Replaces all block offsets with expressions.
        /// </summary>
        public IDictionary<int, PureExpression> BlockOffsets { get; set; } = new Dictionary<int, PureExpression>();
    }
}