﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provide an abstract base class for all writing expressions.
    /// </summary>
    public abstract class WriteExpression : Expression {
        /// <summary>
        /// Equivalent read expression that is fixed during type inference.
        /// </summary>
        public virtual PointerWriteExpression? BaseWrite { get; protected set; }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args)
            => (BaseWrite ?? throw new InvalidOperationException("Write Expression must be fixed when translating!"))
            .Tokens(args);
    }
    /// <summary>
    /// Provides a class for pointer write expressions.
    /// </summary>
    public class PointerWriteExpression : WriteExpression, IEquatable<PointerWriteExpression?> {
        /// <summary>
        /// Creates a new instance of a pointer write expression that writes the <paramref name="value"/> to the <paramref name="pointer"/>.
        /// </summary>
        /// <param name="pointer">Pointer to write to.</param>
        /// <param name="value">Value that shall be written.</param>
        public PointerWriteExpression(PureExpression pointer, PureExpression value) {
            Pointer = pointer;
            Value = value;
        }
        /// <summary>
        /// Pointer that is written to.
        /// </summary>
        public PureExpression Pointer { get; }
        /// <summary>
        /// Value that is written to the <see cref="Pointer"/>.
        /// </summary>
        public PureExpression Value { get; }
        /// <inheritdoc/>
        public override PointerWriteExpression? BaseWrite { get => this; protected set => throw new InvalidOperationException("Cannot set base write for base!"); }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            var ptrType = Pointer.InferType(gamma);
            _ = PointerReadExpression.GetPointerValueType(ptrType, gamma, heap);
            if (!(ptrType.BaseType is ReferenceType ptrRef))
                throw new IllFormedException(Pointer, "Pointer is not a reference!");
            var newHeap = heap.StrongUpdate(ptrRef, Value, out var tau);
            Value.CheckType(gamma, tau);
            return new World(IntegerType.Void, newHeap);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "*";
            if (args.SpaceAfterDerefOperator) yield return " ";
            foreach (var tk in Pointer.Tokens(args)) yield return tk;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Value.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            var writtenType = Value.FixedType;
            if (writtenType is null) throw new InvalidOperationException("Expression must have fixed world when translating!");
            yield return "*";
            if (args.SpaceAfterDerefOperator) yield return " ";
            yield return "((";
            foreach (var tk in writtenType.Tokens(args)) yield return tk;
            yield return "*)";
            foreach (var tk in Pointer.Tokens(args)) yield return tk;
            yield return ")";
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Value.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as PointerWriteExpression);
        /// <inheritdoc/>
        public bool Equals(PointerWriteExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Pointer, other.Pointer) && EqualityComparer<PureExpression>.Default.Equals(Value, other.Value);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Pointer, Value);
        /// <inheritdoc/>
        public static bool operator ==(PointerWriteExpression? left, PointerWriteExpression? right) => EqualityComparer<PointerWriteExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(PointerWriteExpression? left, PointerWriteExpression? right) => !(left == right);
        #endregion
    }
}
