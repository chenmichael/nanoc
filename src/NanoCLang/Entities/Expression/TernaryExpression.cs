﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for ternary expressions.
    /// </summary>
    public class TernaryExpression : Expression, IEquatable<TernaryExpression?> {
        /// <summary>
        /// Creates a new ternary expression instance.
        /// </summary>
        /// <param name="condition">Ternary condition expression.</param>
        /// <param name="then">Expression to evaluate if the <paramref name="condition"/> is truthy.</param>
        /// <param name="else">Expression to evaluate if the <paramref name="condition"/> is falsy.</param>
        public TernaryExpression(PureExpression condition, Expression then, Expression @else) {
            Condition = condition;
            Then = then;
            Else = @else;
        }
        /// <summary>
        /// Condition that decides the evaluation of the bodies.
        /// </summary>
        public PureExpression Condition { get; }
        /// <summary>
        /// Expression to evaluate if the <see cref="Condition"/> is truthy.
        /// </summary>
        public Expression Then { get; }
        /// <summary>
        /// Expression to evaluate if the <see cref="Condition"/> is falsy.
        /// </summary>
        public Expression Else { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            var condType = Condition.InferType(gamma);
            if (!(condType.BaseType is IntegerType condBase))
                throw new IllFormedException(Condition, $"Condition is not boolean nor integral in ternary!");
            var thenGuard = PureExpression.UnqualExpression(Condition, new IntegerConstant(0, condBase.Size));
            var thenWorld = gamma.Guarded(thenGuard,
                gamma => Then.InferWorld(phi, gamma, heap));
            var elseGuard = PureExpression.EqualExpression(Condition, new IntegerConstant(0, condBase.Size));
            var elseWorld = gamma.Guarded(elseGuard,
                gamma => Else.InferWorld(phi, gamma, heap));

            try {
                var returnWorld = new World(
                    new RefinedType(BasicType.SuperType(thenWorld.Type.BaseType, elseWorld.Type.BaseType),
                        v => (thenGuard & thenWorld.Type.Refinement) | (elseGuard & elseWorld.Type.Refinement)),
                    Heap.SuperHeap(gamma, thenWorld.Heap, elseWorld.Heap));
                returnWorld.WellFormed(gamma);

                if (gamma.Guarded(thenGuard, gamma => thenWorld.SubWorld(gamma, returnWorld))
                    && gamma.Guarded(elseGuard, gamma => elseWorld.SubWorld(gamma, returnWorld)))
                    return returnWorld;
            } catch (IllFormedException) { }

            // Fallback to default, less sophisticated world checking
            return thenWorld.SubWorld(gamma, elseWorld)
                ? elseWorld
                : elseWorld.SubWorld(gamma, thenWorld)
                ? thenWorld
                : throw new IllFormedException(Else, "Two branches of the ternary must return the same type!");
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "if";
            if (args.NewlinesAfterIf <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesAfterIf; i++) yield return new NewLineToken();
            foreach (var tk in Condition.Tokens(args)) yield return tk;
            if (args.NewlinesBeforeThen <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesBeforeThen; i++) yield return new NewLineToken();
            yield return "then";
            foreach (var tk in StringFormatterToken.Indented(args.IfBodyIndent, () => PrintBody(Then, args.NewlinesAfterThen, args))) yield return tk;
            if (args.NewlinesBeforeElse <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesBeforeElse; i++) yield return new NewLineToken();
            yield return "else";
            foreach (var tk in StringFormatterToken.Indented(args.IfBodyIndent, () => PrintBody(Else, args.NewlinesAfterElse, args))) yield return tk;
        }
        private IEnumerable<StringFormatterToken> PrintBody(Expression expression, int newlines, NanoCSourceFormat args) {
            if (newlines <= 0) yield return " ";
            else for (int i = 0; i < newlines; i++) yield return new NewLineToken();
            foreach (var tk in expression.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            yield return "if (";
            args.PureRequiresReturn = false;
            foreach (var tk in Condition.Tokens(args)) yield return tk;
            yield return ") {";
            yield return new NewLineToken();
            args.PureRequiresReturn = true;
            foreach (var tk in Then.Tokens(args)) yield return tk;
            yield return ";";
            yield return new NewLineToken();
            yield return "} else {";
            yield return new NewLineToken();
            args.PureRequiresReturn = true;
            foreach (var tk in Else.Tokens(args)) yield return tk;
            yield return ";";
            yield return new NewLineToken();
            yield return "}";
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() => Then.RequiredFunctions().Concat(Else.RequiredFunctions());
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as TernaryExpression);
        /// <inheritdoc/>
        public bool Equals(TernaryExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Condition, other.Condition) && EqualityComparer<Expression>.Default.Equals(Then, other.Then) && EqualityComparer<Expression>.Default.Equals(Else, other.Else);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Condition, Then, Else);
        /// <inheritdoc/>
        public static bool operator ==(TernaryExpression? left, TernaryExpression? right) => EqualityComparer<TernaryExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(TernaryExpression? left, TernaryExpression? right) => !(left == right);
        #endregion
    }
}
