﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for allocation expressions.
    /// </summary>
    public class AllocationExpression : Expression, IEquatable<AllocationExpression?> {
        /// <summary>
        /// Creates a new instance of an allocation expression that allocates the <paramref name="size"/> in bytes at the <paramref name="location"/> and returns a pointer to the beginning of the allocation.
        /// </summary>
        /// <param name="location">Location that should be allocated in.</param>
        /// <param name="size">Size in bytes of the allocation.</param>
        public AllocationExpression(string location, PureExpression size) {
            Location = location;
            Size = size;
        }
        /// <summary>
        /// Location that is to be allocated.
        /// </summary>
        public string Location { get; }
        /// <summary>
        /// Size in bytes of the allocated block.
        /// </summary>
        public PureExpression Size { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            var labs = new Location(Location, true);
            var lj = new Location(Location, false);
            if (!heap.TryGetBinding(labs, out var b))
                throw new IllFormedException(this, $"Allocation invalid: heap does not contain abstract location binding {labs}!");
            if (heap.TryGetBinding(lj, out _))
                throw new IllFormedException(this, $"Allocation invalid: abstract location {labs} was already unfolded to {lj}!");
            var st = Size.InferType(gamma);
            if (!(st.BaseType is IntegerType s))
                throw new IllFormedException(Size, $"Allocation invalid: expected integral type size but got {st.BaseType}");
            if (!st.SubType(gamma, new RefinedType(new IntegerType(s.Size), v => v > new IntegerConstant(0, s.Size))))
                throw new IllFormedException(Size, $"Allocation invalid: size must be a positive integer!");
            Heap hout;
            try {
                hout = heap * new LocationBinding(lj, b.Select(i => i.BaseType).ToArray());
            } catch (ArgumentException e) {
                throw new IllFormedException(this, $"Cannot concatenate heaps: {e.Message}!");
            }
            return new World(
                new RefinedType(new ReferenceType(lj, new SingletonIndex(0)),
                    v => new UninterpretedApplicationExpression("Safe", v)
                    & new UninterpretedApplicationExpression("BLen", v, Size)),
                hout);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "malloc";
            if (args.SpaceAfterFunctionName) yield return " ";
            yield return "(";
            yield return Location;
            yield return args.ParameterListSeparator;
            foreach (var tk in Size.Tokens(args)) yield return tk;
            yield return ")";
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            if (FixedWorld is null) throw new InvalidOperationException("Expression must have fixed world when translating!");
            if (args.PureRequiresReturn) yield return "return ";
            yield return "((";
            foreach (var tk in FixedWorld.Type.Tokens(args)) yield return tk;
            yield return ")";
            yield return "malloc";
            if (args.SpaceAfterFunctionName) yield return " ";
            yield return "(";
            foreach (var tk in Size.Tokens(args)) yield return tk;
            yield return "))";
            if (args.PureRequiresReturn) yield return ";";
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as AllocationExpression);
        /// <inheritdoc/>
        public bool Equals(AllocationExpression? other) => !(other is null) && Location == other.Location && EqualityComparer<PureExpression>.Default.Equals(Size, other.Size);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Location, Size);
        /// <inheritdoc/>
        public static bool operator ==(AllocationExpression? left, AllocationExpression? right) => EqualityComparer<AllocationExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(AllocationExpression? left, AllocationExpression? right) => !(left == right);
        #endregion
    }
}
