﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for structure write expressions.
    /// </summary>
    public class StructWriteExpression : WriteExpression, IEquatable<StructWriteExpression?> {
        /// <summary>
        /// Creates a new instance of a structure write expression that writes the value to the <paramref name="structure"/>.
        /// </summary>
        /// <param name="structure">Structure to write to.</param>
        /// <param name="member">Member in the structure to write to.</param>
        /// <param name="value">Value to insert into the <paramref name="member"/>.</param>
        public StructWriteExpression(PureExpression structure, string member, PureExpression value) {
            Structure = structure;
            Member = member;
            Value = value;
        }
        /// <summary>
        /// Structure that is written to.
        /// </summary>
        public PureExpression Structure { get; }
        /// <summary>
        /// Member in the structure to write to.
        /// </summary>
        public string Member { get; }
        /// <summary>
        /// Value to insert into the <see cref="Member"/>.
        /// </summary>
        public PureExpression Value { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-StrWrite");
            return (BaseWrite = new PointerWriteExpression(GetProjectionPointer(phi, gamma, Structure, Member), Value))
                .InferWorld(phi, gamma, heap);
        }
        /// <summary>
        /// Resolves the structure projection and outputs a correctly offset pointer to the requested structure member.
        /// </summary>
        /// <param name="phi">Global environment that contains the structures definition.</param>
        /// <param name="gamma">Local environment that contains information about the location of the pointer.</param>
        /// <param name="origin">Pointer to the origin of the structure.</param>
        /// <param name="member">Name of requested structure member.</param>
        /// <returns>Offset pointer to member of structure.</returns>
        internal static PureExpression GetProjectionPointer(GlobalEnvironment phi, LocalEnvironment gamma, PureExpression origin, string member) {
            var type = origin.InferType(gamma);
            var baseType = type.BaseType;
            if (!(baseType is ReferenceType t))
                throw new IllFormedException(origin, $"Cannot perform structure projection on non-reference type!");
            if (baseType != new ReferenceType(new Location(t.Location.Name, false), Index.Zero))
                throw new IllFormedException(origin, $"Pointer must point to beginning of a structure!");
            if (!gamma.TryGetStructLoc(t.Location.Name, out var structName))
                throw new IllFormedException(origin, $"Location {t.Location} is not linked to a structure type!");
            if (!phi.TryGetStruct(structName, out var def))
                throw new IllFormedException(origin, $"Link to undefined structure named {structName}!");
            var offset = FindOffset(def, member);
            return origin + new IntegerConstant(offset, 4);
        }

        /// <summary>
        /// Finds the offset of the requested member in the struct given its definition <paramref name="def"/>.
        /// </summary>
        /// <param name="def">Structure definition.</param>
        /// <param name="member">Requested member to access.</param>
        /// <returns>Offset of the member in the struct.</returns>
        private static int FindOffset(StructDefinition def, string member) {
            var memberField = def.Fields.FirstOrDefault(i => i.Name == member);
            if (memberField is null)
                throw new IllFormedException(def, $"Structure has no member named {member}!");
            if (!(memberField.Binding.Index is SingletonIndex i))
                throw new IllFormedException(def, $"Structure member must point to a signle offset, instead it points to offsets {memberField.Binding.Index}!");
            return i.Offset;
        }

        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Structure.Tokens(args)) yield return tk;
            yield return "->";
            yield return Member;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Value.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as StructWriteExpression);
        /// <inheritdoc/>
        public bool Equals(StructWriteExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Structure, other.Structure);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Structure);
        /// <inheritdoc/>
        public static bool operator ==(StructWriteExpression? left, StructWriteExpression? right) => EqualityComparer<StructWriteExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(StructWriteExpression? left, StructWriteExpression? right) => !(left == right);
        #endregion
    }
}
