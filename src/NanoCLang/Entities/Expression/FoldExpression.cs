﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for folding expressions.
    /// </summary>
    public class FoldExpression : Expression, IEquatable<FoldExpression?> {
        /// <summary>
        /// Creates a new instance of a folding expression that folds the <paramref name="location"/>.
        /// </summary>
        /// <param name="location">Location that is folded.</param>
        public FoldExpression(string location) {
            Location = location;
        }
        /// <summary>
        /// Concrete location that is folded to its abstract location.
        /// </summary>
        public string Location { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            var labs = new Location(Location, true);
            if (!heap.TryGetBinding(labs, out var b1))
                throw new IllFormedException(this, $"Cannot fold ~{Location} without heap binding!");
            var lj = new Location(Location, false);
            if (!heap.TryGetBinding(lj, out var b2))
                throw new IllFormedException(this, $"Cannot fold {Location} because it was never unfolded!");
            if (!Heap.SubBlock(gamma, b2, b1))
                throw new IllFormedException(this, $"Cannot fold {Location} because it doesnt match abstract constraints!");
            return new World(IntegerType.Void, heap.Filter(i => i != lj));
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "[";
            if (args.SpaceBeforeFoldingKeywords) yield return " ";
            yield return "fold";
            yield return " ";
            yield return Location;
            if (args.SpaceAfterFoldingKeywords) yield return " ";
            yield return "]";
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            yield return "/* ";
            foreach (var tk in Tokens((NanoCSourceFormat)args)) yield return tk;
            yield return " */";
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as FoldExpression);
        /// <inheritdoc/>
        public bool Equals(FoldExpression? other) => !(other is null) && Location == other.Location;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Location);
        /// <inheritdoc/>
        public static bool operator ==(FoldExpression? left, FoldExpression? right) => EqualityComparer<FoldExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(FoldExpression? left, FoldExpression? right) => !(left == right);
        #endregion
    }
}
