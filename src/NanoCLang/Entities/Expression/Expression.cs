﻿using NanoCLang.Environemnts;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides an abstract superclass for all valid NanoC expressions.
    /// This class also implements the well formedness and type checking/inference mechanisms for expressions.
    /// </summary>
    public abstract class Expression : Base {
        /// <summary>
        /// Infer the world of the expression and throw an exception if it is ill-formed.
        /// Additionally fixes the resulting world to this entity.
        /// </summary>
        /// <param name="phi">Global Environemt to check the expression with.</param>
        /// <param name="gamma">Local Environemt to check the expression with.</param>
        /// <param name="heap">Input heap of the expression.</param>
        /// <exception cref="IllFormedException">Expression is ill-formed.</exception>
        public World InferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) => FixedWorld = DoInferWorld(phi, gamma, heap);
        /// <summary>
        /// Infer the world of the expression and throw an exception if it is ill-formed.
        /// </summary>
        /// <param name="phi">Global Environemt to check the expression with.</param>
        /// <param name="gamma">Local Environemt to check the expression with.</param>
        /// <param name="heap">Input heap of the expression.</param>
        /// <exception cref="IllFormedException">Expression is ill-formed.</exception>
        protected abstract World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap);
        /// <summary>
        /// Gets the world that was fixed to this expression during type-checking.
        /// </summary>
        public World? FixedWorld { get; protected set; }
        /// <summary>
        /// Typecheck the expression with the <paramref name="world"/> and throw an exception if it is ill-formed.
        /// </summary>
        /// <param name="phi">Global Environemt to check the expression with.</param>
        /// <param name="gamma">Local Environemt to check the expression with.</param>
        /// <param name="heap">Input heap of the expression.</param>
        /// <param name="world">World to typecheck with.</param>
        /// <exception cref="IllFormedException">Expression is ill-formed.</exception>
        public virtual void CheckType(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap, World world) {
            var inferred = InferWorld(phi, gamma, heap);
            if (!inferred.SubWorld(gamma, world)) throw new IllFormedException(this, $"This is not of world {world}!");
        }
        /// <summary>
        /// Gets an enumeration of required function calls. Can be used to check if a function is recursive.
        /// </summary>
        /// <returns>Enumeration of called functions.</returns>
        public abstract IEnumerable<string> RequiredFunctions();
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) => Tokens((NanoCSourceFormat)args);
    }
}