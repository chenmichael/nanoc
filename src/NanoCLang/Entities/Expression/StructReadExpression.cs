﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for structure read expressions.
    /// </summary>
    public class StructReadExpression : ReadExpression, IEquatable<StructReadExpression?> {
        /// <summary>
        /// Creates a new instance of a structure read expression that reads the value of the <paramref name="structure"/>.
        /// </summary>
        /// <param name="structure">Structure to write to.</param>
        /// <param name="member">Member in the structure to access.</param>
        public StructReadExpression(PureExpression structure, string member) {
            Structure = structure;
            Member = member;
        }
        /// <summary>
        /// Structure that is read from.
        /// </summary>
        public PureExpression Structure { get; }
        /// <summary>
        /// Member in the structure to access.
        /// </summary>
        public string Member { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-StrRead");
            return (BaseRead = new PointerReadExpression(StructWriteExpression.GetProjectionPointer(phi, gamma, Structure, Member)))
                .InferWorld(phi, gamma, heap);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Structure.Tokens(args)) yield return tk;
            yield return "->";
            yield return Member;
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as StructReadExpression);
        /// <inheritdoc/>
        public bool Equals(StructReadExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Structure, other.Structure);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Structure);
        /// <inheritdoc/>
        public static bool operator ==(StructReadExpression? left, StructReadExpression? right) => EqualityComparer<StructReadExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(StructReadExpression? left, StructReadExpression? right) => !(left == right);
        #endregion
    }
}
