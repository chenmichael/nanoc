﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provide an abstract base class for all reading expressions.
    /// </summary>
    public abstract class ReadExpression : Expression {
        /// <summary>
        /// Equivalent read expression that is fixed during type inference.
        /// </summary>
        public virtual PointerReadExpression? BaseRead { get; protected set; }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args)
            => (BaseRead ?? throw new InvalidOperationException("Read Expression must be fixed when translating!"))
            .Tokens(args);
    }
    /// <summary>
    /// Provides a class for pointer read expressions.
    /// </summary>
    public class PointerReadExpression : ReadExpression, IEquatable<PointerReadExpression?> {
        /// <summary>
        /// Creates a new instance of a pointer read expression that reads the value of the <paramref name="pointer"/>.
        /// </summary>
        /// <param name="pointer">Pointer to write to.</param>
        public PointerReadExpression(PureExpression pointer) {
            Pointer = pointer;
        }
        /// <summary>
        /// Pointer that is read from.
        /// </summary>
        public PureExpression Pointer { get; }
        /// <inheritdoc/>
        public override PointerReadExpression? BaseRead { get => this; protected set => throw new InvalidOperationException("Cannot set base read for base!"); }
        /// <summary>
        /// Gets the type that the pointer points to if it is well-formed.
        /// Additionally checks if the type that is bound at the index in the heap is safe within bounds of the allocation.
        /// </summary>
        /// <param name="ptrType">(Refined) type of the pointer expression.</param>
        /// <param name="gamma">Local environment to check the pointer with.</param>
        /// <param name="heap">Heap that contains (if well-formed) the appointed location.</param>
        /// <returns>Type of the value the pointer points to.</returns>
        public static Type GetPointerValueType(Type ptrType, LocalEnvironment gamma, Heap heap) {
            if (!(ptrType.BaseType is ReferenceType ptrRef))
                throw new IllFormedException(ptrType, $"Cannot read from non-reference of type {ptrType.BaseType}");
            if (ptrRef.Abstract)
                throw new IllFormedException(ptrType, "Cannot read from abstract reference!");
            var loc = ptrRef.Location;
            var bindings = heap.TryGetBinding(loc, out var tmp) ? tmp
                : throw new IllFormedException(ptrType, $"Cannot access pointer to location {loc} if the location is not bound in the heap!");

            Type? bound = null;
            foreach (var block in bindings) if (ptrRef.Offsets <= block.Index) {
                    bound = block.Type;
                    break;
                }

            if (bound is null)
                throw new IllFormedException(ptrRef.Offsets, $"Heap location {loc} does not contain a binding at offset {ptrRef.Offsets}!");

            if (!ptrType.SubType(gamma, new RefinedType(ptrRef,
                v => new UninterpretedApplicationExpression("SafeN", v, new IntegerConstant(bound.Size, 4)))))
                throw new IllFormedException(ptrType, $"Pointer is not safe and can not be read from!");
            return bound;
        }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-Read");
            var ptrType = Pointer.InferType(gamma);
            return new World(GetPointerValueType(ptrType, gamma, heap), heap);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "*";
            if (args.SpaceAfterDerefOperator) yield return " ";
            foreach (var tk in Pointer.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            if (FixedWorld is null) throw new InvalidOperationException("Expression must have fixed world when translating!");
            yield return "*";
            if (args.SpaceAfterDerefOperator) yield return " ";
            yield return "((";
            foreach (var tk in FixedWorld.Type.Tokens(args)) yield return tk;
            yield return "*)";
            foreach (var tk in Pointer.Tokens(args)) yield return tk;
            yield return ")";
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as PointerReadExpression);
        /// <inheritdoc/>
        public bool Equals(PointerReadExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Pointer, other.Pointer);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Pointer);
        /// <inheritdoc/>
        public static bool operator ==(PointerReadExpression? left, PointerReadExpression? right) => EqualityComparer<PointerReadExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(PointerReadExpression? left, PointerReadExpression? right) => !(left == right);
        #endregion
    }
}
