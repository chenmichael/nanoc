﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for function call expressions.
    /// </summary>
    public class FunctionCallExpression : Expression, IEquatable<FunctionCallExpression?>, IHeapElement, ISubstitutable<FunctionCallExpression> {
        /// <summary>
        /// Creates a new instance of a function call expression that calls the function with the given <paramref name="name"/> with the <paramref name="parameters"/> and refers to the <paramref name="locations"/>.
        /// </summary>
        /// <param name="name">Name of the function to call.</param>
        /// <param name="locations">Locations that the function operates on.</param>
        /// <param name="parameters">Parameters that are passed to the function.</param>
        public FunctionCallExpression(string name, string[] locations, params PureExpression[] parameters) {
            Name = name;
            Locations = locations;
            Parameters = parameters;
        }
        /// <summary>
        /// Name of the function to call.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Locations that the function operates on.
        /// </summary>
        public string[] Locations { get; }
        /// <summary>
        /// Parameters that are passed to the function.
        /// </summary>
        public PureExpression[] Parameters { get; }
        /// <summary>
        /// Gets the arity of the function call, being the number of supplied parameters.
        /// </summary>
        public int Arity => Parameters.Length;
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-Call");
            if (!phi.TryGetValue(Name, out var rawschema))
                throw new IllFormedException(this, $"No function named {Name} was declared in this environment!");
            var schema = rawschema.Build(phi);
            var theta = GetSubstitutions(schema);
            CheckParameterSubtyping(gamma, schema, theta);
            var usedLocs = new HashSet<string>(Locations);
            var hm = heap.Filter(i => usedLocs.Contains(i.Name));
            var hu = heap.Filter(i => !usedLocs.Contains(i.Name));
            // hu.WellFormed(gamma);
            hm.WellFormed(gamma);
            if (!hm.SubHeap(gamma, schema.Heap.Replace(theta)))
                throw new IllFormedException(this, "Heap is not compatible with function schema heap!");
            var hout = hu * schema.World.Heap.Replace(theta);
            hout.WellFormed(gamma);
            return new World(schema.World.Type.Replace(theta), hout);
        }
        /// <inheritdoc cref="IHeapElement.GetBindings(GlobalEnvironment, out IEnumerable{KeyValuePair{string, string}})"/>
        public IEnumerable<LocationBinding> GetBindings(GlobalEnvironment phi, out IEnumerable<KeyValuePair<string, string>> links) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "WF-StructCall");
            if (Locations.Length < 1)
                throw new IllFormedException(this, "At least one location must be given. The first location sets the main struct location!");
            var l = new Location(Locations[0], true);
            if (!phi.TryGetStruct(Name, out var def))
                throw new IllFormedException(this, $"No structure named {Name} was declared in this environment!");
            var linklist = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>(l.Name, Name) };
            var bindings = new List<LocationBinding>();
            var theta = GetSubstitutions(def);
            foreach (var dep in def.Dependencies.Select(i => i.Replace(theta))) {
                bindings.AddRange(dep.GetBindings(phi, out var newLinks));
                linklist.AddRange(newLinks);
            }
            bindings.Add(new LocationBinding(l, def.Fields.Select(i => i.Binding.Replace(theta).Replace(def.GetSubstitutor())).ToArray()));
            links = linklist;
            return bindings;
        }

        private void CheckParameterSubtyping(LocalEnvironment gamma, FunctionSchema schema, Substitutor theta) {
            for (var i = 0; i < Parameters.Length; i++) {
                var param = Parameters[i];
                var expParam = schema.Parameters[i];
                param.CheckType(gamma, expParam.Type.Replace(theta));
            }
        }

        private Substitutor GetSubstitutions(ICallable callable) => new Substitutor() { Locations = LocationSubstitution(this, callable.Locations, Locations), Variables = VariableSubstitution(this, callable.Parameters, Parameters) };

        private IDictionary<string, PureExpression> VariableSubstitution(Base entity, TypeParameter[] schemaparams, PureExpression[] @params) {
            return schemaparams.Length != @params.Length
                   ? throw new IllFormedException(entity, $"Parameter count mismatch! Expected {schemaparams.Length}, but got {@params.Length}!")
                         : new Dictionary<string, PureExpression>(schemaparams.Zip(@params, (a, b) => new KeyValuePair<string, PureExpression>(a.Name, b)));
        }

        private static IDictionary<string, string> LocationSubstitution(Base entity, string[] schemalocations, string[] locations) {
            return schemalocations.Length != locations.Length
                   ? throw new IllFormedException(entity, $"Location count mismatch! Expected {schemalocations.Length}, but got {locations.Length}!")
                         : new Dictionary<string, string>(schemalocations.Zip(locations, (a, b) => new KeyValuePair<string, string>(a, b)).Where(i => i.Key != i.Value));
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "[";
            foreach (var tk in StringFormatterToken.Separated(Locations, args.ParameterListSeparator)) yield return tk;
            yield return "]";
            if (args.SpaceBetweenLocationsAndFunctionName) yield return " ";
            yield return Name;
            if (args.SpaceAfterFunctionName) yield return " ";
            yield return "(";
            foreach (var tk in StringFormatterToken.Separated(Parameters, args, args.ParameterListSeparator)) yield return tk;
            if (args.SpaceInEmptyArgList && Parameters.Length == 0) yield return " ";
            yield return ")";
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            var requireReturn = args.PureRequiresReturn;
            if (requireReturn) yield return "return ";
            yield return Name;
            if (args.SpaceAfterFunctionName) yield return " ";
            yield return "(";
            args.PureRequiresReturn = false;
            foreach (var tk in StringFormatterToken.Separated(Parameters, args, args.ParameterListSeparator)) yield return tk;
            if (args.SpaceInEmptyArgList && Parameters.Length == 0) yield return " ";
            yield return ")";
            if (requireReturn) yield return ";";
            args.PureRequiresReturn = requireReturn;
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield return Name; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as FunctionCallExpression);
        /// <inheritdoc/>
        public bool Equals(FunctionCallExpression? other) => !(other is null) && Name == other.Name && Locations.SequenceEqual(other.Locations) && Parameters.SequenceEqual(other.Parameters);
        /// <inheritdoc/>
        public override int GetHashCode() {
            var hash = new HashCode();
            hash.Add(Name);
            foreach (var loc in Locations) hash.Add(loc);
            foreach (var param in Parameters) hash.Add(param);
            return hash.ToHashCode();
        }
        /// <inheritdoc/>
        public FunctionCallExpression Replace(Substitutor sub) => new FunctionCallExpression(Name,
            Locations.Select(i => sub.Locations.TryGetValue(i, out var subs) ? subs : i).ToArray(),
            Parameters.Select(i => i.Replace(sub)).ToArray());
        /// <inheritdoc/>
        public static bool operator ==(FunctionCallExpression? left, FunctionCallExpression? right) => EqualityComparer<FunctionCallExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(FunctionCallExpression? left, FunctionCallExpression? right) => !(left == right);
        #endregion
    }
}