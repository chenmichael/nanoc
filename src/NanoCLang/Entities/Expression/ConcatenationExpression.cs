﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for binding expressions.
    /// </summary>
    public class ConcatenationExpression : Expression, IEquatable<ConcatenationExpression?> {
        /// <summary>
        /// Creates a new instance of a concatenation expression that evaluates the <paramref name="expression"/> and then the <paramref name="next"/> expression.
        /// </summary>
        /// <param name="expression">Expression that is evaluated first.</param>
        /// <param name="next">Expression that is evaluated second.</param>
        public ConcatenationExpression(Expression expression, Expression next) {
            Expression = expression;
            Next = next;
        }
        /// <summary>
        /// Expression that is evaluated first.
        /// </summary>
        public Expression Expression { get; }
        /// <summary>
        /// Expression that is evaluated with the binding.
        /// </summary>
        public Expression Next { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-Concat");
            var expressionWorld = Expression.InferWorld(phi, gamma, heap);
            return Next.InferWorld(phi, gamma, expressionWorld.Heap);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "let";
            yield return " ";
            foreach (var tk in Expression.Tokens(args)) yield return tk;
            yield return " ";
            yield return "in";
            if (args.NewlinesAfterBindingExpression <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesAfterBindingExpression; i++) yield return new NewLineToken();
            foreach (var tk in Next.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() => Expression.RequiredFunctions().Concat(Next.RequiredFunctions());
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            args.PureRequiresReturn = false;
            foreach (var tk in Expression.Tokens(args)) yield return tk;
            yield return ";";
            yield return new NewLineToken();
            args.PureRequiresReturn = true;
            foreach (var tk in Next.Tokens(args)) yield return tk;
            yield return ";";
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as ConcatenationExpression);
        /// <inheritdoc/>
        public bool Equals(ConcatenationExpression? other) => !(other is null) && EqualityComparer<Expression>.Default.Equals(Expression, other.Expression) && EqualityComparer<Expression>.Default.Equals(Next, other.Next);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Expression, Next);
        /// <inheritdoc/>
        public static bool operator ==(ConcatenationExpression? left, ConcatenationExpression? right) => EqualityComparer<ConcatenationExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(ConcatenationExpression? left, ConcatenationExpression? right) => !(left == right);
        #endregion
    }
}
