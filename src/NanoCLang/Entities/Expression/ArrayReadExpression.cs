﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for array read expressions.
    /// </summary>
    public class ArrayReadExpression : ReadExpression, IEquatable<ArrayReadExpression?> {
        /// <summary>
        /// Creates a new instance of a array read expression that reads the value of the <paramref name="array"/>.
        /// </summary>
        /// <param name="array">Array to write to.</param>
        /// <param name="offset">Offset in the array to access.</param>
        public ArrayReadExpression(PureExpression array, PureExpression offset) {
            Array = array;
            Offset = offset;
        }
        /// <summary>
        /// Array that is read from.
        /// </summary>
        public PureExpression Array { get; }
        /// <summary>
        /// Offset in the array to access.
        /// </summary>
        public PureExpression Offset { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-ArrRead");
            return (BaseRead = new PointerReadExpression(ArrayWriteExpression.GetArrayPointer(gamma, Array, Offset)))
                .InferWorld(phi, gamma, heap);
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Array.Tokens(args)) yield return tk;
            yield return "[";
            foreach (var tk in Offset.Tokens(args)) yield return tk;
            yield return "]";
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as ArrayReadExpression);
        /// <inheritdoc/>
        public bool Equals(ArrayReadExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Array, other.Array);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Array);
        /// <inheritdoc/>
        public static bool operator ==(ArrayReadExpression? left, ArrayReadExpression? right) => EqualityComparer<ArrayReadExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(ArrayReadExpression? left, ArrayReadExpression? right) => !(left == right);
        #endregion
    }
}
