﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for unfold expressions.
    /// </summary>
    public class UnfoldExpression : Expression, IEquatable<UnfoldExpression?> {
        /// <summary>
        /// Creates an instance of an unfold expression that unfolds the pointer in the <paramref name="expression"/> at the <paramref name="location"/> and binds it the <paramref name="name"/> in the <paramref name="next"/> expression.
        /// </summary>
        /// <param name="name">Name of the binding.</param>
        /// <param name="location">Location that the <paramref name="expression"/> refers to.</param>
        /// <param name="expression">Expression that is bound to the <paramref name="name"/>.</param>
        /// <param name="next">Expression that is evaluated with the binding.</param>
        public UnfoldExpression(string name, string location, PureExpression expression, Expression next) {
            Name = name;
            Location = location;
            Expression = expression;
            Next = next;
        }
        /// <summary>
        /// Name that the <see cref="Expression"/> is bound to.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Location that the pointer in the <see cref="Expression"/> refers to.
        /// </summary>
        public string Location { get; }
        /// <summary>
        /// Pointer that shall be unfolded and bound to the <see cref="Name"/>.
        /// </summary>
        public PureExpression Expression { get; }
        /// <summary>
        /// Expression that is evaluated with the binding.
        /// </summary>
        public Expression Next { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            var ptrType = Expression.InferType(gamma);
            if (!(ptrType.BaseType is ReferenceType ptrBase))
                throw new IllFormedException(Expression, $"Cannot unfold expression of non-reference type {ptrType.BaseType}!");
            var labs = ptrBase.Location;
            var lj = new Location(labs.Name, false);
            if (ptrBase.Concrete)
                throw new IllFormedException(Expression, $"Cannot unfold reference to concrete location {labs}!");
            if (!heap.TryGetBinding(labs, out var b))
                throw new IllFormedException(Expression, $"Cannot unfold reference to location {labs} without a heap binding!");
            if (heap.Contains(lj))
                throw new IllFormedException(Expression, $"Cannot unfold, because {lj} was already unfolded in heap!");
            if (!ptrType.SubType(gamma, new RefinedType(ptrBase, v => PureExpression.UnqualExpression(v, new IntegerConstant(0, 4)))))
                throw new IllFormedException(Expression, $"Cannot unfold possibly nullptr reference {ptrBase}!");
            var theta = new Substitutor();
            for (var i = 0; i < b.Length; i++) {
                var blockType = b[i];
                switch (blockType.Index) {
                case SingletonIndex idx:
                    var fresh = new VariableExpression(Heap.OffsetVar(idx.Offset));
                    theta.BlockOffsets.Add(idx.Offset, fresh);
                    break;
                case SequenceIndex idx: break;
                default: throw new InvalidProgramException("Expected singleton or sequence index!");
                }
            }
            BlockType[] bj = new BlockType[b.Length];
            for (var i = 0; i < b.Length; i++) {
                var blockType = b[i];
                bj[i] = (blockType.Index) switch {
                    SingletonIndex idx => new BlockType((Index)idx.Clone(), new RefinedType(blockType.Type.BaseType, v => PureExpression.EqualExpression(v, new VariableExpression(Heap.OffsetVar(idx.Offset))))),
                    SequenceIndex idx => new BlockType((Index)idx.Clone(), blockType.Type.Replace(theta)),
                    _ => throw new InvalidProgramException("Expected singleton or sequence index!")
                };
            }
            var h1 = heap * new Heap(new LocationBinding(lj, bj));
            var world = gamma.With(b.Where(i => i.Index is SingletonIndex)
                .Select(i => new KeyValuePair<string, Type>(Heap.OffsetVar((i.Index as SingletonIndex ?? throw new InvalidProgramException("Bad cast!")).Offset), i.Type.Replace(theta))),
                gamma => {
                    h1.WellFormed(gamma);
                    return gamma.With(Name, new RefinedType(new ReferenceType(lj, ptrBase.Offsets), v => PureExpression.EqualExpression(v, Expression)),
                        gamma => Next.InferWorld(phi, gamma, h1));
                });
            world.WellFormed(gamma);
            return world;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "letu";
            yield return " ";
            yield return Name;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            yield return "[";
            if (args.SpaceBeforeFoldingKeywords) yield return " ";
            yield return "unfold";
            yield return " ";
            yield return Location;
            if (args.SpaceAfterFoldingKeywords) yield return " ";
            yield return "]";
            if (args.SpaceBetweenUnfoldAndExpression) yield return " ";
            foreach (var tk in Expression.Tokens(args)) yield return tk;
            yield return " ";
            yield return "in";
            if (args.NewlinesAfterUnfoldExpression <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesAfterUnfoldExpression; i++) yield return new NewLineToken();
            foreach (var tk in Next.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            foreach (var tk in new ReferenceType(new Location(Location, true), Index.Zero).Tokens(args)) yield return tk;
            yield return " ";
            yield return Name;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            yield return "/* [";
            if (args.SpaceBeforeFoldingKeywords) yield return " ";
            yield return "unfold";
            yield return " ";
            yield return Location;
            if (args.SpaceAfterFoldingKeywords) yield return " ";
            yield return "] */ ";
            args.PureRequiresReturn = false;
            if (args.SpaceBetweenUnfoldAndExpression) yield return " ";
            foreach (var tk in Expression.Tokens(args)) yield return tk;
            yield return ";";
            if (args.NewlinesAfterUnfoldExpression <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesAfterUnfoldExpression; i++) yield return new NewLineToken();
            args.PureRequiresReturn = false;
            foreach (var tk in Next.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() => Next.RequiredFunctions();
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as UnfoldExpression);
        /// <inheritdoc/>
        public bool Equals(UnfoldExpression? other) => !(other is null) && Name == other.Name && Location == other.Location && EqualityComparer<PureExpression>.Default.Equals(Expression, other.Expression) && EqualityComparer<Expression>.Default.Equals(Next, other.Next);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Name, Location, Expression, Next);
        /// <inheritdoc/>
        public static bool operator ==(UnfoldExpression? left, UnfoldExpression? right) => EqualityComparer<UnfoldExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(UnfoldExpression? left, UnfoldExpression? right) => !(left == right);
        #endregion
    }
}
