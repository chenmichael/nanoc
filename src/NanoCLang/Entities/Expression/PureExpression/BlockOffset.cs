﻿using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for block offset constants.
    /// </summary>
    public class BlockOffset : VariableExpression, IEquatable<BlockOffset?>, ICloneable {
        /// <summary>
        /// Creates a new block <paramref name="offset"/>.
        /// </summary>
        /// <param name="offset">Offset of the block offset literal.</param>
        public BlockOffset(int offset) : base(Heap.OffsetVar(offset)) {
            Offset = offset;
        }
        /// <summary>
        /// The offset in the heap that this entity refers to.
        /// </summary>
        public int Offset { get; }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "@";
            yield return Offset.ToString();
        }
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor sub)
            => sub.BlockOffsets.TryGetValue(Offset, out var exp)
            ? exp
            : (BlockOffset)Clone();
        /// <inheritdoc/>
        public override object Clone() => new BlockOffset(Offset);
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as BlockOffset);
        /// <inheritdoc/>
        public bool Equals(BlockOffset? other) => !(other is null) && Offset == other.Offset;
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Offset);
        /// <inheritdoc/>
        public static bool operator ==(BlockOffset? left, BlockOffset? right) => EqualityComparer<BlockOffset?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(BlockOffset? left, BlockOffset? right) => !(left == right);
        #endregion
    }
}
