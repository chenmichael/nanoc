using Microsoft.Z3;
using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using static NanoCLang.Entities.RelationalExpression;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides an abstract class for pure expressions.
    /// Pure expressions are a subset of all NanoC expressions that do not modify the heap.
    /// </summary>
    public abstract class PureExpression : Expression, ISubstitutable<PureExpression> {
        /// <summary>
        /// Gets the type that was fixed to the pure expression during type-checking.
        /// </summary>
        public Type? FixedType { get; private set; }
        /// <summary>
        /// Infer the type of the pure expression based on the environment <paramref name="gamma"/>.
        /// Additionally fixes the result of the inference to the expression entity.
        /// </summary>
        /// <param name="gamma">Environment for type inference.</param>
        /// <returns>Inferred type of the expression</returns>
        public Type InferType(LocalEnvironment gamma) => FixedType = DoInferType(gamma);
        /// <summary>
        /// Infer the type of the pure expression based on the environment <paramref name="gamma"/>.
        /// </summary>
        /// <param name="gamma">Environment for type inference.</param>
        /// <returns>Inferred type of the expression</returns>
        protected abstract Type DoInferType(LocalEnvironment gamma);
        /// <summary>
        /// Converts the expression to a boolean expression that the SMT solver can handle.
        /// </summary>
        /// <param name="gamma">The local environment to solve with.</param>
        /// <param name="smt">SMT solver context.</param>
        /// <returns>Boolean expression that makes up the environment.</returns>
        public virtual BoolExpr ToBoolean(LocalEnvironment gamma, NanoCSMT smt) => throw new NotImplementedException($"No boolean EUFA for type {GetType().Name}!");
        /// <summary>
        /// Converts the expression to an arithmetic expression that the SMT solver can handle.
        /// </summary>
        /// <param name="gamma">The local environment to solve with.</param>
        /// <param name="smt">SMT solver context.</param>
        /// <returns>Arithmetic expression that makes up the environment.</returns>
        public virtual ArithExpr ToArithmetic(LocalEnvironment gamma, NanoCSMT smt) => throw new NotImplementedException($"No integral EUFA for type {GetType().Name}!");
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-Pure");
            return new World(InferType(gamma), heap);
        }
        /// <inheritdoc/>
        public override void CheckType(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap, World world) => CheckType(gamma, world.Type);
        /// <summary>
        /// Typecheck the pure expression with the <paramref name="type"/> and throw an exception if it is ill-formed.
        /// </summary>
        /// <param name="gamma">Local Environemt to check the expression with.</param>
        /// <param name="type">Type to typecheck with.</param>
        /// <exception cref="IllFormedException">Pure expression is ill-formed.</exception>
        public virtual void CheckType(LocalEnvironment gamma, Type type) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-PureSub");
            var inferred = InferType(gamma);
            if (!inferred.SubType(gamma, type)) throw new IllFormedException(this, $"{this} : {{{inferred}}} is not of type {type}!");
        }
        /// <summary>
        /// Infer the type of the pure expression based on the environment <paramref name="gamma"/>.
        /// </summary>
        /// <param name="gamma">Environment for well-formedness checking.</param>
        public abstract void WellFormed(LocalEnvironment gamma);
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        /// <inheritdoc/>
        public abstract PureExpression Replace(Substitutor sub);
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            if (args.PureRequiresReturn) yield return "return ";
            var requiresReturn = args.PureRequiresReturn;
            args.PureRequiresReturn = false;
            foreach (var tk in NoReturnTokens(args)) yield return tk;
            args.PureRequiresReturn = requiresReturn;
            if (args.PureRequiresReturn) yield return ";";
        }
        /// <inheritdoc cref="Tokens(CSourceFormat)"/>
        protected virtual IEnumerable<StringFormatterToken> NoReturnTokens(CSourceFormat args) => Tokens((NanoCSourceFormat)args);
        #region Helper functions / Operators
        /// <summary>
        /// Converts the boolean <paramref name="value"/> to a pure expression in form of a boolean literal.
        /// </summary>
        /// <param name="value">Value to convert to an expression.</param>
        /// <returns><see cref="BooleanConstant"/> with the given <paramref name="value"/>.</returns>
        public static explicit operator PureExpression(bool value) => new BooleanConstant(value);
        /// <summary>
        /// Converts the integer literal <paramref name="value"/> to a pure expression in form of an integer literal.
        /// </summary>
        /// <param name="value">Value to convert to an expression.</param>
        /// <returns><see cref="IntegerConstant"/> with the given <paramref name="value"/>.</returns>
        public static explicit operator PureExpression(int value) => new IntegerConstant(value, sizeof(int));
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression EqualExpression(PureExpression lhs, PureExpression rhs)
            => new RelationalExpression(lhs, RelOperator.Equal, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression UnqualExpression(PureExpression lhs, PureExpression rhs)
            => new RelationalExpression(lhs, RelOperator.Unequal, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator <=(PureExpression lhs, PureExpression rhs)
            => new RelationalExpression(lhs, RelOperator.LessEqual, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator >=(PureExpression lhs, PureExpression rhs)
            => new RelationalExpression(lhs, RelOperator.GreaterEqual, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator <(PureExpression lhs, PureExpression rhs)
            => new RelationalExpression(lhs, RelOperator.Less, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator >(PureExpression lhs, PureExpression rhs)
            => new RelationalExpression(lhs, RelOperator.Greater, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator +(PureExpression lhs, PureExpression rhs)
            => new AdditionExpression(lhs, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator -(PureExpression lhs, PureExpression rhs)
            => new SubtractionExpression(lhs, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator &(PureExpression lhs, PureExpression rhs)
            => new BooleanExpression(lhs, BooleanExpression.BoolOperator.And, rhs);
        /// <summary>
        /// Generates a new binary expression from the input.
        /// </summary>
        /// <param name="lhs">Left hand side of the operation.</param>
        /// <param name="rhs">Right hand side of the operation.</param>
        /// <returns><see cref="BinaryExpression"/> that models the operation.</returns>
        public static BinaryExpression operator |(PureExpression lhs, PureExpression rhs)
            => new BooleanExpression(lhs, BooleanExpression.BoolOperator.Or, rhs);
        #endregion
    }
}
