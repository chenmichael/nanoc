﻿using Microsoft.Z3;
using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for variable literal expression.
    /// </summary>
    public class VariableExpression : PureExpression, IEquatable<VariableExpression?>, ICloneable {
        /// <summary>
        /// Creates a new instance of a variable literal with the given <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Name that the literal refers to.</param>
        public VariableExpression(string name) {
            Name = name;
        }
        /// <summary>
        /// Name that the literal refers to.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Converts a string to its literal variable expression.
        /// </summary>
        /// <param name="name">Name of the variable.</param>
        public static implicit operator VariableExpression(string name) => new VariableExpression(name);
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-Var: {Name}");
            if (!gamma.ContainsKey(Name))
                throw new IllFormedException(this, $"Reference to undeclared variable {Name}!");
        }
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor rep)
            => rep.Variables.TryGetValue(Name, out var substitute) ? substitute : (PureExpression)Clone();
        /// <inheritdoc/>
        public virtual object Clone() => new VariableExpression(Name);
        /// <inheritdoc/>
        public override ArithExpr ToArithmetic(LocalEnvironment gamma, NanoCSMT smt) => smt.Context.MkIntConst(Name);
        /// <inheritdoc/>
        public override BoolExpr ToBoolean(LocalEnvironment gamma, NanoCSMT smt) => smt.Context.MkBoolConst(Name);
        /// <inheritdoc/>     
        protected override Type DoInferType(LocalEnvironment gamma) {
            return !gamma.TryGetValue(Name, out var type)
                ? throw new IllFormedException(this, $"Variable {Name} is not bound in the local environment!")
                : new RefinedType(type, v => EqualExpression(v, this));
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return Name;
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as VariableExpression);
        /// <inheritdoc/>
        public bool Equals(VariableExpression? other) => !(other is null) && Name == other.Name;
        /// <inheritdoc/>
        public override int GetHashCode() => System.HashCode.Combine(Name);
        /// <inheritdoc/>
        public static bool operator ==(VariableExpression? left, VariableExpression? right) => EqualityComparer<VariableExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(VariableExpression? left, VariableExpression? right) => !(left == right);
        #endregion
    }
}
