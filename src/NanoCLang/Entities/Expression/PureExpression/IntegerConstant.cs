﻿using Microsoft.Z3;
using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for integer literal constants.
    /// </summary>
    public class IntegerConstant : PureExpression, IEquatable<IntegerConstant?>, ICloneable {
        /// <summary>
        /// Creates a new integer constant with <paramref name="value"/> and a <paramref name="size"/> in bytes.
        /// </summary>
        /// <param name="value">Value of the literal.</param>
        /// <param name="size">Size of the integer literal in bytes.</param>
        public IntegerConstant(int value, int size) {
            Size = size;
            Value = value;
        }
        /// <summary>
        /// Size of the integer literal in bytes.
        /// </summary>
        public int Size { get; }
        /// <summary>
        /// Value of the literal.
        /// </summary>
        public int Value { get; }
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma) {
            if (Size < 0)
                throw new IllFormedException(this, $"Integer literal cannot have negative size {Size}!");
        }
        /// <inheritdoc/>
        protected override Type DoInferType(LocalEnvironment gamma) => new RefinedType(new IntegerType(Size, new SingletonIndex(Value)),
                           v => EqualExpression(v, this));
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            if (Value == 0 && Size == 0) {
                yield return "return";
                yield break;
            }
            yield return Value.ToString();
            yield return "_";
            yield return Size.ToString();
        }
        /// <inheritdoc/>
        public override ArithExpr ToArithmetic(LocalEnvironment gamma, NanoCSMT smt) => smt.Context.MkInt(Value);
        /// <inheritdoc/>
        public override BoolExpr ToBoolean(LocalEnvironment gamma, NanoCSMT smt) => smt.Context.MkBool(Value != 0);
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor sub) => (PureExpression)Clone();
        /// <inheritdoc/>
        public virtual object Clone() => new IntegerConstant(Value, Size);
        /// <inheritdoc/>
        protected override IEnumerable<StringFormatterToken> NoReturnTokens(CSourceFormat args) {
            if (args.PureRequiresReturn && Size == 0) yield break;
            yield return "(";
            yield return IntegerType.GetCType(Size);
            yield return ")";
            yield return Value.ToString();
            yield return "LL";
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as IntegerConstant);
        /// <inheritdoc/>
        public bool Equals(IntegerConstant? other) => !(other is null) && Size == other.Size && Value == other.Value;
        /// <inheritdoc/>
        public override int GetHashCode() => System.HashCode.Combine(Size, Value);
        /// <inheritdoc/>
        public static bool operator ==(IntegerConstant? left, IntegerConstant? right) => EqualityComparer<IntegerConstant?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(IntegerConstant? left, IntegerConstant? right) => !(left == right);
        #endregion
    }
}
