﻿using Microsoft.Z3;
using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for uninterpreted function calls.
    /// </summary>
    public class UninterpretedApplicationExpression : PureExpression, IEquatable<UninterpretedApplicationExpression?> {
        /// <summary>
        /// Creates a new uninterpreted function call instance that calls the function named <paramref name="name"/> with the given <paramref name="parameters"/>.
        /// </summary>
        /// <param name="name">Name of the uninterpreted function to call.</param>
        /// <param name="parameters">Parameters to pass to the function.</param>
        public UninterpretedApplicationExpression(string name, params PureExpression[] parameters) {
            Name = name;
            Parameters = parameters;
        }
        /// <summary>
        /// Name of the uninterpreted function to call.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Parameters to pass to the function.
        /// </summary>
        public PureExpression[] Parameters { get; }
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-UnintF: {this}");
            foreach (var param in Parameters)
                param.WellFormed(gamma);
        }
        /// <inheritdoc/>
        protected override Type DoInferType(LocalEnvironment gamma) => throw new InvalidOperationException("Cannot infer type of uninterpreted functions!");
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return Name;
            yield return "(";
            foreach (var tk in StringFormatterToken.Separated(Parameters, args, args.ParameterListSeparator)) yield return tk;
            if (args.SpaceInEmptyArgList && Parameters.Length == 0) yield return " ";
            yield return ")";
        }
        /// <inheritdoc/>
        public override BoolExpr ToBoolean(LocalEnvironment gamma, NanoCSMT smt) {
            switch (Name) {
            case "PAdd":
                if (Parameters.Length != 3) throw new IllFormedException(this, $"Inconsistend parameter count for uninterpreted function {Name}, expected 3, got {Parameters.Length}!");
                return smt.PAdd((IntExpr)Parameters[0].ToArithmetic(gamma, smt), (IntExpr)Parameters[1].ToArithmetic(gamma, smt), Parameters[2].ToArithmetic(gamma, smt));
            case "BLen":
                if (Parameters.Length != 2) throw new IllFormedException(this, $"Inconsistend parameter count for uninterpreted function {Name}, expected 2, got {Parameters.Length}!");
                return smt.BLen((IntExpr)Parameters[0].ToArithmetic(gamma, smt), Parameters[1].ToArithmetic(gamma, smt));
            case "Safe":
                if (Parameters.Length != 1) throw new IllFormedException(this, $"Inconsistend parameter count for uninterpreted function {Name}, expected 1, got {Parameters.Length}!");
                return smt.Safe((IntExpr)Parameters[0].ToArithmetic(gamma, smt));
            case "SafeN":
                if (Parameters.Length != 2) throw new IllFormedException(this, $"Inconsistend parameter count for uninterpreted function {Name}, expected 2, got {Parameters.Length}!");
                return smt.SafeN((IntExpr)Parameters[0].ToArithmetic(gamma, smt), (IntExpr)Parameters[1].ToArithmetic(gamma, smt));
            default: throw new IllFormedException(this, $"Function {Name} is not a boolean function!");
            }
        }
        /// <inheritdoc/>
        public override ArithExpr ToArithmetic(LocalEnvironment gamma, NanoCSMT smt) {
            switch (Name) {
            case "BS":
                if (Parameters.Length != 1) throw new IllFormedException(this, $"Inconsistend parameter count for uninterpreted function {Name}, expected 1, got {Parameters.Length}!");
                return smt.BS((IntExpr)Parameters[0].ToArithmetic(gamma, smt));
            case "BE":
                if (Parameters.Length != 1) throw new IllFormedException(this, $"Inconsistend parameter count for uninterpreted function {Name}, expected 1, got {Parameters.Length}!");
                return smt.BS((IntExpr)Parameters[0].ToArithmetic(gamma, smt));
            default: throw new IllFormedException(this, $"Function {Name} is not an integral function!");
            }
        }
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor sub)
            => new UninterpretedApplicationExpression(Name, Parameters.Select(i => i.Replace(sub)).ToArray());
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as UninterpretedApplicationExpression);
        /// <inheritdoc/>
        public bool Equals(UninterpretedApplicationExpression? other) => !(other is null) && Name == other.Name && Parameters.SequenceEqual(other.Parameters);
        /// <inheritdoc/>
        public override int GetHashCode() {
            var hash = new HashCode();
            hash.Add(Name);
            foreach (var param in Parameters) hash.Add(param);
            return hash.ToHashCode();
        }
        /// <inheritdoc/>
        public static bool operator ==(UninterpretedApplicationExpression? left, UninterpretedApplicationExpression? right) => EqualityComparer<UninterpretedApplicationExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(UninterpretedApplicationExpression? left, UninterpretedApplicationExpression? right) => !(left == right);
        #endregion
    }
}
