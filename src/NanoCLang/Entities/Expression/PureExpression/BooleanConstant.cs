﻿namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for boolean literal constants.
    /// </summary>
    public class BooleanConstant : IntegerConstant {
        /// <summary>
        /// Creates a new boolean constant with the given <paramref name="value"/>.
        /// </summary>
        /// <param name="value">Value of the literal.</param>
        public BooleanConstant(bool value) : base(value ? 1 : 0, 1) { }
        /// <inheritdoc/>
        public override object Clone() => new BooleanConstant(Value != 0);
    }
}
