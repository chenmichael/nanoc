﻿using Microsoft.Z3;
using NanoCLang.Environemnts;
using System;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for relation expressions.
    /// </summary>
    public class BooleanExpression : BinaryExpression {
        private readonly BoolOperator @operator;
        /// <summary>
        /// Creates a new instance of a relation expression of <paramref name="left"/> and <paramref name="right"/>.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="op">Operator to apply</param>
        /// <param name="right">Right hand operand.</param>
        public BooleanExpression(PureExpression left, BoolOperator op, PureExpression right) : base(left, GetOperator(op), right) {
            @operator = op;
        }

        /// <summary>
        /// Creates a new instance of a relation expression of <paramref name="left"/> and <paramref name="right"/>.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="op">Operator to apply</param>
        /// <param name="right">Right hand operand.</param>
        public BooleanExpression(PureExpression left, string op, PureExpression right) : base(left, op, right) {
            @operator = op switch {
                "&&" => BoolOperator.And,
                "||" => BoolOperator.Or,
                _ => throw new ArgumentOutOfRangeException("Invalid boolean operator!")
            };
        }

        /// <summary>
        /// Enumeration of valid boolean operators.
        /// </summary>
        public enum BoolOperator {
            /// <summary>
            /// Logical and conjunction operator.
            /// </summary>
            And,
            /// <summary>
            /// Logical or disjunction operator.
            /// </summary>
            Or
        }
        private static string GetOperator(BoolOperator op) => op switch {
            BoolOperator.And => "&&",
            BoolOperator.Or => "||",
            _ => throw new ArgumentOutOfRangeException("Invalid boolean operator!")
        };
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor sub)
            => new BooleanExpression(Left.Replace(sub), @operator, Right.Replace(sub));
        /// <inheritdoc/>
        protected override Type DoInferType(LocalEnvironment gamma) {
            var left = Left.InferType(gamma);
            var right = Right.InferType(gamma);
            return left.BaseType != right.BaseType
                ? throw new IllFormedException(this, $"Cannot compare {left.BaseType} and {right.BaseType}!")
                : new RefinedType(IntegerType.Boolean,
                v => EqualExpression(v, this));
        }
        /// <inheritdoc/>
        public override BoolExpr ToBoolean(LocalEnvironment gamma, NanoCSMT smt) => @operator switch {
            BoolOperator.And => smt.Context.MkAnd(Left.ToBoolean(gamma, smt), Right.ToBoolean(gamma, smt)),
            BoolOperator.Or => smt.Context.MkOr(Left.ToBoolean(gamma, smt), Right.ToBoolean(gamma, smt)),
            _ => throw new InvalidProgramException("Unexpected operator! Constructor forbids this!")
        };
    }
}
