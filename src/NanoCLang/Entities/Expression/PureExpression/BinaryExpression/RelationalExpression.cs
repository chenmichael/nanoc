﻿using Microsoft.Z3;
using NanoCLang.Environemnts;
using System;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for relation expressions.
    /// </summary>
    public class RelationalExpression : BinaryExpression {
        private readonly RelOperator @operator;
        /// <summary>
        /// Creates a new instance of a relation expression of <paramref name="left"/> and <paramref name="right"/>.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="op">Operator to apply</param>
        /// <param name="right">Right hand operand.</param>
        public RelationalExpression(PureExpression left, RelOperator op, PureExpression right) : base(left, GetOperator(op), right) {
            @operator = op;
        }

        /// <summary>
        /// Creates a new instance of a relation expression of <paramref name="left"/> and <paramref name="right"/>.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="op">Operator to apply</param>
        /// <param name="right">Right hand operand.</param>
        public RelationalExpression(PureExpression left, string op, PureExpression right) : base(left, op, right) {
            @operator = op switch {
                "<" => RelOperator.Less,
                ">" => RelOperator.Greater,
                "<=" => RelOperator.LessEqual,
                ">=" => RelOperator.GreaterEqual,
                "==" => RelOperator.Equal,
                "!=" => RelOperator.Unequal,
                _ => throw new ArgumentOutOfRangeException("Invalid relational operator!")
            };
        }

        /// <summary>
        /// Enumeration of valid relational operators.
        /// </summary>
        public enum RelOperator {
            /// <summary>
            /// Strictly less than operator.
            /// </summary>
            Less,
            /// <summary>
            /// Strictly greater than operator.
            /// </summary>
            Greater,
            /// <summary>
            /// Less or equal to operator.
            /// </summary>
            LessEqual,
            /// <summary>
            /// Greater or equal to operator.
            /// </summary>
            GreaterEqual,
            /// <summary>
            /// Equality operator.
            /// </summary>
            Equal,
            /// <summary>
            /// Inequality operator
            /// </summary>
            Unequal
        }
        private static string GetOperator(RelOperator op) => op switch {
            RelOperator.Less => "<",
            RelOperator.Greater => ">",
            RelOperator.LessEqual => "<=",
            RelOperator.GreaterEqual => ">=",
            RelOperator.Equal => "==",
            RelOperator.Unequal => "!=",
            _ => throw new ArgumentOutOfRangeException("Invalid relational operator!")
        };
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor sub)
            => new RelationalExpression(Left.Replace(sub), @operator, Right.Replace(sub));
        /// <inheritdoc/>
        protected override Type DoInferType(LocalEnvironment gamma) {
            var left = Left.InferType(gamma);
            var right = Right.InferType(gamma);

            switch (left.BaseType) {
            case IntegerType t when right.BaseType is IntegerType o && t.Size == o.Size: break;
            case IntegerType _ when right.BaseType is ReferenceType: break;
            case ReferenceType _ when right.BaseType is IntegerType: break;
            case ReferenceType t when right.BaseType is ReferenceType o && t.Location == o.Location: break;
            default: throw new IllFormedException(this, $"Cannot compare {left.BaseType} and {right.BaseType}!");
            }
            return new RefinedType(IntegerType.Boolean,
                v => EqualExpression(v, this));
        }
        /// <inheritdoc/>
        public override BoolExpr ToBoolean(LocalEnvironment gamma, NanoCSMT smt) => @operator switch {
            RelOperator.Less => smt.Context.MkLt(Left.ToArithmetic(gamma, smt), Right.ToArithmetic(gamma, smt)),
            RelOperator.Greater => smt.Context.MkGt(Left.ToArithmetic(gamma, smt), Right.ToArithmetic(gamma, smt)),
            RelOperator.LessEqual => smt.Context.MkLe(Left.ToArithmetic(gamma, smt), Right.ToArithmetic(gamma, smt)),
            RelOperator.GreaterEqual => smt.Context.MkGe(Left.ToArithmetic(gamma, smt), Right.ToArithmetic(gamma, smt)),
            RelOperator.Equal => Equality(gamma, smt),
            RelOperator.Unequal => !Equality(gamma, smt),
            _ => throw new InvalidProgramException("Unexpected operator! Constructor forbids this!")
        };
        private BoolExpr Equality(LocalEnvironment gamma, NanoCSMT smt) {
            return Left.InferType(gamma).BaseType is IntegerType i && i.Size == 1
                ? smt.Context.MkEq(Left.ToBoolean(gamma, smt), Right.ToBoolean(gamma, smt))
                : smt.Context.MkEq(Left.ToArithmetic(gamma, smt), Right.ToArithmetic(gamma, smt));
        }
    }
}
