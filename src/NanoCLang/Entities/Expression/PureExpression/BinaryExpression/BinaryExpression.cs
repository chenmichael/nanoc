﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a superclass for binary expressions.
    /// </summary>
    public abstract class BinaryExpression : PureExpression, IEquatable<BinaryExpression?> {
        /// <summary>
        /// Creates a new instance of a binary operation applied to <paramref name="left"/> and <paramref name="right"/>.
        /// </summary>
        /// <param name="left">Left hand expression of the operator.</param>
        /// <param name="operation">Operation to evaluate for the expression.</param>
        /// <param name="right">Right hand expression of the operator.</param>
        public BinaryExpression(PureExpression left, string operation, PureExpression right) {
            Left = left;
            Operation = operation;
            Right = right;
        }
        /// <summary>
        /// Left hand expression of the operator.
        /// </summary>
        public PureExpression Left { get; }
        /// <summary>
        /// Operation to evaluate for the expression.
        /// </summary>
        public string Operation { get; }
        /// <summary>
        /// Right hand expression of the operator.
        /// </summary>
        public PureExpression Right { get; }
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"WF-BinOp: {this}");
            Left.WellFormed(gamma);
            Right.WellFormed(gamma);
        }
        /// <inheritdoc/>
        protected abstract override Type DoInferType(LocalEnvironment gamma);
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "(";
            foreach (var tk in Left.Tokens(args)) yield return tk;
            if (args.SpaceBeforeBinOp) yield return " ";
            yield return Operation;
            if (args.SpaceAfterBinOp) yield return " ";
            foreach (var tk in Right.Tokens(args)) yield return tk;
            yield return ")";
        }
        /// <inheritdoc/>
        protected override IEnumerable<StringFormatterToken> NoReturnTokens(CSourceFormat args) {
            yield return "(";
            foreach (var tk in Left.Tokens(args)) yield return tk;
            if (args.SpaceBeforeBinOp) yield return " ";
            yield return Operation;
            if (args.SpaceAfterBinOp) yield return " ";
            foreach (var tk in Right.Tokens(args)) yield return tk;
            yield return ")";
        }
        /// <summary>
        /// Applies the operation on indices and gives a superset of all possible values as output index.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="right">Right hand operand.</param>
        /// <returns>Resulting index that covers all possible values.</returns>
        public virtual Index IndexOperation(Index left, Index right) => new ArbitraryIndex();
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as BinaryExpression);
        /// <inheritdoc/>
        public bool Equals(BinaryExpression? other) => !(other is null) && Operation == other.Operation && EqualityComparer<PureExpression>.Default.Equals(Left, other.Left) && EqualityComparer<PureExpression>.Default.Equals(Right, other.Right);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Left, Operation, Right);
        /// <inheritdoc/>
        public static bool operator ==(BinaryExpression? left, BinaryExpression? right) => EqualityComparer<BinaryExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(BinaryExpression? left, BinaryExpression? right) => !(left == right);
        #endregion
    }
}
