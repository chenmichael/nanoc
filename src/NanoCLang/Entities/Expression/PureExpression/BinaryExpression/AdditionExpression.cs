﻿using Microsoft.Z3;
using NanoCLang.Environemnts;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for additive expressions.
    /// </summary>
    public class AdditionExpression : BinaryExpression {
        /// <summary>
        /// Operator used for this expression.
        /// </summary>
        public const string OPERATOR = "+";
        /// <summary>
        /// Creates a new instance of an additive expression of <paramref name="left"/> + <paramref name="right"/>.
        /// </summary>
        /// <param name="left">Left hand operand.</param>
        /// <param name="right">Right hand operand.</param>
        public AdditionExpression(PureExpression left, PureExpression right) : base(left, OPERATOR, right) { }
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor sub)
            => new AdditionExpression(Left.Replace(sub), Right.Replace(sub));
        /// <inheritdoc/>
        protected override Type DoInferType(LocalEnvironment gamma) {
            var left = Left.InferType(gamma);
            var right = Right.InferType(gamma);
            switch (left.BaseType) {
            case IntegerType t:
                switch (right.BaseType) {
                case IntegerType o when t.Size == o.Size:
                    return new RefinedType(
                        new IntegerType(t.Size, IndexOperation(t.Values, o.Values)),
                        v => EqualExpression(v, this));
                case ReferenceType o:
                    return new RefinedType(
                        new ReferenceType(o.Location, IndexOperation(t.Values, o.Offsets)),
                        v => new UninterpretedApplicationExpression("PAdd", v, Right, Left));
                default: throw new IllFormedException(this, $"Cannot add {left.BaseType} and {right.BaseType}!");
                }
            case ReferenceType t:
                switch (right.BaseType) {
                case IntegerType o:
                    return new RefinedType(
                    new ReferenceType(t.Location, IndexOperation(t.Offsets, o.Values)),
                    v => new UninterpretedApplicationExpression("PAdd", v, Left, Right));
                default: throw new IllFormedException(this, $"Cannot add {left.BaseType} and {right.BaseType}!");
                }
            default: throw new IllFormedException(this, $"No binary operation {left.BaseType}!");
            }
        }
        /// <inheritdoc/>
        public override ArithExpr ToArithmetic(LocalEnvironment gamma, NanoCSMT smt)
            => smt.Context.MkAdd(Left.ToArithmetic(gamma, smt), Right.ToArithmetic(gamma, smt));
        /// <inheritdoc/>
        public override Index IndexOperation(Index left, Index right) => left + right;
    }
}
