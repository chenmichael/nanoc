﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for assertions.
    /// </summary>
    public class AssertionExpression : PureExpression, IEquatable<AssertionExpression?> {
        /// <summary>
        /// Creates a new assertion expression instance that asserts that the <paramref name="expression"/> is truty.
        /// </summary>
        /// <param name="expression">Expression to evaluate.</param>
        public AssertionExpression(PureExpression expression) {
            Expression = expression;
        }
        /// <summary>
        /// Expression to evaluate.
        /// </summary>
        public PureExpression Expression { get; }
        /// <inheritdoc/>
        public override void WellFormed(LocalEnvironment gamma) => Expression.WellFormed(gamma);
        /// <inheritdoc/>
        protected override Type DoInferType(LocalEnvironment gamma) {
            var type = Expression.InferType(gamma);
            if (!type.SubType(gamma, new RefinedType(new IntegerType(type.Size), v => UnqualExpression(v, (PureExpression)0))))
                throw new IllFormedException(Expression, $"Assertion failed!");
            return IntegerType.Void;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "assert";
            if (args.SpaceAfterFunctionName) yield return " ";
            yield return "(";
            foreach (var tk in Expression.Tokens(args)) yield return tk;
            yield return ")";
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            yield return "/* ";
            foreach (var tk in Tokens((NanoCSourceFormat)args)) yield return tk;
            yield return " */";
        }
        /// <inheritdoc/>
        public override void CheckType(LocalEnvironment gamma, Type type) {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"T-Assert: {Expression}");
            if (!(type is IntegerType i && i.IsVoid)) throw new IllFormedException(this, $"Assertion expression is of type {type.BaseType.GetType().Name}!");
            var inferredType = Expression.InferType(gamma);
            Expression.CheckType(gamma, new RefinedType(new IntegerType(inferredType.Size), v => UnqualExpression(v, (PureExpression)0)));
        }
        /// <inheritdoc/>
        public override PureExpression Replace(Substitutor sub)
            => new AssertionExpression(Expression.Replace(sub));
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as AssertionExpression);
        /// <inheritdoc/>
        public bool Equals(AssertionExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Expression, other.Expression);
        /// <inheritdoc/>
        public override int GetHashCode() => System.HashCode.Combine(Expression);
        /// <inheritdoc/>
        public static bool operator ==(AssertionExpression? left, AssertionExpression? right) => EqualityComparer<AssertionExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(AssertionExpression? left, AssertionExpression? right) => !(left == right);
        #endregion
    }
}
