﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for binding expressions.
    /// </summary>
    public class BindingExpression : Expression, IEquatable<BindingExpression?> {
        /// <summary>
        /// Creates a new instance of a binding expression that binds the <paramref name="expression"/> to the <paramref name="name"/> in the <paramref name="next"/> expression.
        /// </summary>
        /// <param name="name">Name of the binding.</param>
        /// <param name="expression">Expression that is bound to the <paramref name="name"/>.</param>
        /// <param name="next">Expression that is evaluated with the binding.</param>
        public BindingExpression(string name, Expression expression, Expression next) {
            Name = name;
            Expression = expression;
            Next = next;
        }
        /// <summary>
        /// Name that the <see cref="Expression"/> is bound to.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Expression that is bound to the <see cref="Name"/>.
        /// </summary>
        public Expression Expression { get; }
        /// <summary>
        /// Expression that is evaluated with the binding.
        /// </summary>
        public Expression Next { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-Let");
            var expressionWorld = Expression.InferWorld(phi, gamma, heap);
            return gamma.With(Name, expressionWorld.Type,
                gamma => Next.InferWorld(phi, gamma, expressionWorld.Heap));
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return "let";
            yield return " ";
            yield return Name;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Expression.Tokens(args)) yield return tk;
            yield return " ";
            yield return "in";
            if (args.NewlinesAfterBindingExpression <= 0) yield return " ";
            else for (int i = 0; i < args.NewlinesAfterBindingExpression; i++) yield return new NewLineToken();
            foreach (var tk in Next.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            var boundType = Expression.FixedWorld?.Type;
            if (boundType is null) throw new InvalidOperationException("Expression must have fixed world when translating!");
            foreach (var tk in boundType.Tokens(args)) yield return tk;
            yield return " ";
            yield return Name;
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            args.PureRequiresReturn = false;
            foreach (var tk in Expression.Tokens(args)) yield return tk;
            yield return ";";
            yield return new NewLineToken();
            args.PureRequiresReturn = true;
            foreach (var tk in Next.Tokens(args)) yield return tk;
            yield return ";";
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() => Expression.RequiredFunctions().Concat(Next.RequiredFunctions());
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as BindingExpression);
        /// <inheritdoc/>
        public bool Equals(BindingExpression? other) => !(other is null) && Name == other.Name && EqualityComparer<Expression>.Default.Equals(Expression, other.Expression) && EqualityComparer<Expression>.Default.Equals(Next, other.Next);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Name, Expression, Next);
        /// <inheritdoc/>
        public static bool operator ==(BindingExpression? left, BindingExpression? right) => EqualityComparer<BindingExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(BindingExpression? left, BindingExpression? right) => !(left == right);
        #endregion
    }
}
