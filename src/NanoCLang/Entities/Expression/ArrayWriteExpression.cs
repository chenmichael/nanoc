﻿using NanoCLang.Environemnts;
using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for array write expressions.
    /// </summary>
    public class ArrayWriteExpression : WriteExpression, IEquatable<ArrayWriteExpression?> {
        /// <summary>
        /// Creates a new instance of a array write expression that reads the value of the <paramref name="array"/>.
        /// </summary>
        /// <param name="array">Array to write to.</param>
        /// <param name="offset">Offset in the array to access.</param>
        /// <param name="value">Value to write to the array.</param>
        public ArrayWriteExpression(PureExpression array, PureExpression offset, PureExpression value) {
            Array = array;
            Offset = offset;
            Value = value;
        }
        /// <summary>
        /// Array that is written to.
        /// </summary>
        public PureExpression Array { get; }
        /// <summary>
        /// Offset in the array to access.
        /// </summary>
        public PureExpression Offset { get; }
        /// <summary>
        /// Value that is written to the array.
        /// </summary>
        public PureExpression Value { get; }
        /// <inheritdoc/>
        protected override World DoInferWorld(GlobalEnvironment phi, LocalEnvironment gamma, Heap heap) {
            VerbConsole.WriteLine(VerbosityLevel.Default, "T-ArrWrite");
            return (BaseWrite = new PointerWriteExpression(GetArrayPointer(gamma, Array, Offset), Value))
                .InferWorld(phi, gamma, heap);
        }
        /// <summary>
        /// Gets a properly offset pointer to the array, can also be used to offset structure pointers manually, so no checking on the bound block index is necessary.
        /// </summary>
        /// <param name="gamma">Local environment that contains type of the array value.</param>
        /// <param name="array">Pointer to the memory block.</param>
        /// <param name="offset">Offset from that <paramref name="array"/> pointer.</param>
        /// <returns>Offset Pointer</returns>
        internal static PureExpression GetArrayPointer(LocalEnvironment gamma, PureExpression array, PureExpression offset) {
            var type = array.InferType(gamma);
            if (!(type.BaseType is ReferenceType))
                throw new IllFormedException(array, $"Cannot perform pointer access on non-reference expression!");
            return array + offset;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            foreach (var tk in Array.Tokens(args)) yield return tk;
            yield return "[";
            foreach (var tk in Offset.Tokens(args)) yield return tk;
            yield return "]";
            if (args.SpaceBeforeBindingAssignment) yield return " ";
            yield return "=";
            if (args.SpaceAfterBindingAssignment) yield return " ";
            foreach (var tk in Value.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<string> RequiredFunctions() { yield break; }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as ArrayWriteExpression);
        /// <inheritdoc/>
        public bool Equals(ArrayWriteExpression? other) => !(other is null) && EqualityComparer<PureExpression>.Default.Equals(Array, other.Array);
        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Array);
        /// <inheritdoc/>
        public static bool operator ==(ArrayWriteExpression? left, ArrayWriteExpression? right) => EqualityComparer<ArrayWriteExpression?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(ArrayWriteExpression? left, ArrayWriteExpression? right) => !(left == right);
        #endregion
    }
}
