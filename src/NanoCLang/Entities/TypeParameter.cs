﻿using System;
using System.Collections.Generic;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for type parameters binding a type to a name.
    /// </summary>
    public class TypeParameter : Base, IEquatable<TypeParameter?> {
        /// <summary>
        /// Creates a new type parameter instance binding the <paramref name="type"/> to the <paramref name="name"/>.
        /// </summary>
        /// <param name="name">Name that the <paramref name="type"/> is bound to.</param>
        /// <param name="type">Type that is bound to the <paramref name="name"/>.</param>
        public TypeParameter(string name, Type type) {
            Name = name;
            Type = type;
        }
        /// <summary>
        /// Name that the <see cref="Type"/> is bound to.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Type that is bound to the <see cref="Name"/>.
        /// </summary>
        public Type Type { get; }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(NanoCSourceFormat args) {
            yield return Name;
            if (args.SpaceBeforeTypeOperator) yield return " ";
            yield return ":";
            if (args.SpaceAfterTypeOperator) yield return " ";
            foreach (var tk in Type.Tokens(args)) yield return tk;
        }
        /// <inheritdoc/>
        public override IEnumerable<StringFormatterToken> Tokens(CSourceFormat args) {
            foreach (var tk in Type.Tokens(args)) yield return tk;
            yield return " ";
            yield return Name;
        }
        #region Equality checks
        /// <inheritdoc/>
        public override bool Equals(object? obj) => Equals(obj as TypeParameter);
        /// <inheritdoc/>
        public bool Equals(TypeParameter? other) => !(other is null) && Name == other.Name && EqualityComparer<Type>.Default.Equals(Type, other.Type);
        /// <inheritdoc/>
        public override int GetHashCode() => System.HashCode.Combine(Name, Type);
        /// <inheritdoc/>
        public static bool operator ==(TypeParameter? left, TypeParameter? right) => EqualityComparer<TypeParameter?>.Default.Equals(left, right);
        /// <inheritdoc/>
        public static bool operator !=(TypeParameter? left, TypeParameter? right) => !(left == right);
        #endregion
    }
}