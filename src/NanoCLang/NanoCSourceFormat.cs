﻿namespace NanoCLang {
    /// <summary>
    /// Provides a class for passing options to the source formatter for NanoC.
    /// </summary>
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class NanoCSourceFormat {
        public bool SpaceAfterFunctionName { get; internal set; } = false;
        public bool SpaceInEmptyArgList { get; internal set; } = false;
        public bool SpaceBeforeBindingAssignment { get; internal set; } = true;
        public bool SpaceAfterBindingAssignment { get; internal set; } = true;
        public bool SpaceBeforeInInBinding { get; internal set; } = true;
        public int NewlinesAfterFunctionBinding { get; internal set; } = 2;
        public bool SpaceBetweenIndexOffsetAndPlus { get; internal set; } = false;
        public bool SpaceBetweenIndexPlusAndStep { get; internal set; } = false;
        public bool SpaceBeforeFoldingKeywords { get; internal set; } = false;
        public bool SpaceAfterFoldingKeywords { get; internal set; } = false;
        public bool SpaceBetweenUnfoldAndExpression { get; internal set; } = false;
        public int NewlinesAfterUnfoldExpression { get; internal set; } = 1;
        public int NewlinesAfterIf { get; internal set; } = 0;
        public int NewlinesBeforeThen { get; internal set; } = 1;
        public int NewlinesAfterThen { get; internal set; } = 0;
        public int NewlinesBeforeElse { get; internal set; } = 1;
        public int NewlinesAfterElse { get; internal set; } = 0;
        public bool SpaceAfterFunKeyword { get; internal set; } = false;
        public bool FunctionParamNamesInDefinition { get; internal set; } = true;
        public bool NewlineBeforeFunctionBody { get; internal set; } = false;
        /// <summary>
        /// Overridden if <see cref="NewlineBeforeFunctionBody"/> is set.
        /// </summary>
        public bool SpaceBeforeFunctionBody { get; internal set; } = true;
        public bool IndentFunctionBodyOpenBrace { get; internal set; } = false;
        public bool NewlineBeforeFunctionBodyCloseBrace { get; internal set; } = true;
        /// <summary>
        /// Overridden if <see cref="NewlineBeforeFunctionBodyCloseBrace"/> is set.
        /// </summary>
        public bool SpaceBeforeFunctionBodyCloseBrace { get; internal set; } = true;
        public bool IndentFunctionBodyCloseBrace { get; internal set; } = false;
        public bool NewlineBeforeFunctionSchemaColon { get; internal set; } = false;
        public bool IndentFunctionSchemaColon { get; internal set; } = false;
        public bool SpaceBeforeFunctionSchemaColon { get; internal set; } = true;
        public bool NewlineAfterFunctionSchemaColon { get; internal set; } = false;
        public bool IndentFunctionSchema { get; internal set; } = true;
        public bool SpaceAfterFunctionSchemaColon { get; internal set; } = true;
        public int FunctionBodyIndent { get; internal set; } = 4;
        public int FunctionSchemaIndent { get; internal set; } = 0;
        /// <summary>
        /// Amount of indent on body of then and else blocks.
        /// </summary>
        public int IfBodyIndent { get; internal set; } = 5;
        public int NewlinesAfterBindingExpression { get; internal set; } = 1;
        public bool SpaceAfterDerefOperator { get; internal set; } = false;
        public string ParameterListSeparator { get; internal set; } = ", ";
        public string BlockSeparator { get; internal set; } = ", ";
        public bool SpaceBetweenBindOperatorAndBlock { get; internal set; } = true;
        public bool SpaceBetweenLocationAndBindOperator { get; internal set; } = true;
        public bool SpaceBeforeBinOp { get; internal set; } = true;
        public bool SpaceAfterBinOp { get; internal set; } = true;
        public bool SpaceBeforeTypeOperator { get; internal set; } = true;
        public bool SpaceAfterTypeOperator { get; internal set; } = true;
        public bool SpaceBeforeRefineOperator { get; internal set; } = true;
        public bool SpaceAfterRefineOperator { get; internal set; } = true;
        public int NewlinesBeforeBindingListOperator { get; internal set; } = 1;
        public int IndentHeapListOperator { get; internal set; } = 0;
        public string HeapBindingListSeparator { get; internal set; } = "* ";
        public int NewlinesBeforeWorldOperator { get; internal set; } = 1;
        public int NewlinesAfterWorldOperator { get; internal set; } = 0;
        public bool SpaceBeforeWorldOperator { get; internal set; } = false;
        public bool SpaceAfterWorldOperator { get; internal set; } = true;
        public bool SpaceBeforeFunctionWorld { get; internal set; } = false;
        public int NewlinesBeforeFunctionWorld { get; internal set; } = 1;
        public bool SpaceAfterFunctionWorld { get; internal set; } = true;
        public int NewlinesAfterFunctionWorld { get; internal set; } = 0;
        public bool SpaceBetweenLocationsAndFunctionName { get; internal set; } = false;
        public bool SpaceBetweenLocationsAndFunctionParams { get; internal set; } = false;
        public int NewlinesBeforeFunctionParams { get; internal set; } = 1;
        public int NewlinesAfterStructBinding { get; internal set; } = 2;
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}