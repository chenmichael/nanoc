parser grammar NanoCBaseParser ;

options {
  tokenVocab = NanoCBaseLexer ;
}

// Locations

location
    : name = IDENTIFIER # concreteLocation
    | TILDE name = IDENTIFIER # abstractLocation
    ;

singleLocation
    : entrypoint = location EOF
    ;

// Indices

index
    : value = INTEGER # singleton
    | value = INTEGER PLUS step = INTEGER # sequence
    | ANY # arbitraryIndex
    ;

singleIndex
    : entrypoint = index EOF
    ;

// Pure Expressions

pureExpression
    : left = pureExpression op = (PLUS | MINUS) right = pureExpression # integerAdditive
    | left = pureExpression op = (LESS | LESSEQUAL | GREATER | GREATEREQUAL) right = pureExpression # relationalComparison
    | left = pureExpression op = (EQUALS | UNEQUAL) right = pureExpression # equalityComparison
    | left = pureExpression op = AND right = pureExpression # conjunctionExpression
    | left = pureExpression op = OR right = pureExpression # disjunctionExpression
    | LB body = pureExpression RB # bracedExpression
    | ASSERT LB body = pureExpression RB # assertion
    | AT offset = INTEGER # blockOffset
    | name = IDENTIFIER LB (a += pureExpression (COMMA a += pureExpression)*)? RB # unintApplication
    | value = INTEGER UNDERSCORE size = INTEGER # integerConstant
    | name = IDENTIFIER # variable
    ;

singlePureExpression
    : entrypoint = pureExpression EOF
    ;

// Basic Types

basicType
    : size = INTEGER LBA typeIndex = index RBA # integerType
    | REF LB locationName = location COMMA typeIndex = index RB # referenceType
    ;

singleBasicType
    : entrypoint = basicType EOF
    ;

// Refined Types

refinedType
    : LBC variable = IDENTIFIER COLON baseType = basicType PIPE refinement = pureExpression RBC # refinedBasicType
    | trivial = basicType # triviallyRefinedType
    ;

singleRefinedType
    : entrypoint = refinedType EOF
    ;

// Expressions

functionCall
    : LBA locations = identifierList RBA name = IDENTIFIER LB (a += pureExpression (COMMA a += pureExpression)*)? RB
    ;

expression
    : IF condition = pureExpression THEN bodyThen = expression ELSE bodyElse = expression # ifThenElse
    | LET name = IDENTIFIER ASSIGN definition = expression IN body = expression # binding
    | LET value = expression IN body = expression # concatenation
    | LETU name = IDENTIFIER ASSIGN LBA UNFOLD locationName = IDENTIFIER RBA definition = pureExpression IN body = expression # locationUnfold
    | LBA FOLD locationName = IDENTIFIER RBA # locationFold
    | DEREF ptr = pureExpression ASSIGN value = pureExpression # pointerWrite
    | DEREF ptr = pureExpression # pointerRead
    | ptr = pureExpression ARROW member = IDENTIFIER ASSIGN value = pureExpression # structWrite
    | ptr = pureExpression ARROW member = IDENTIFIER # structRead
    | ptr = pureExpression LBA offset = pureExpression RBA ASSIGN value = pureExpression # arrayWrite
    | ptr = pureExpression LBA offset = pureExpression RBA # arrayRead
    | MALLOC LB locationName = IDENTIFIER COMMA size = pureExpression RB # allocation
    | call = functionCall # functionCallExpression
    | body = pureExpression # pureExpAsExp
    ;

singleExpression
    : entrypoint = expression EOF
    ;

// Blocks

blockBinding
    : offset = index COLON type = refinedType
    ; // indexed block type binding

block
    : (bindings += blockBinding (COMMA bindings += blockBinding)*)?
    ; // block

singleBlock
    : entrypoint = block EOF
    ;

// Heaps

locationBinding
    : atLocation = location TARROW bound = block
    ;

heapElement
    : binding = locationBinding
    | structheap = functionCall
    ;

heap
    : EMP
    | elems += heapElement (DEREF elems += heapElement)*
    ;

singleHeap
    : entrypoint = heap EOF
    ;

// Function Schemas

typeArg
    : name = IDENTIFIER COLON type = refinedType
    ; // x : T

typeArgList
    : (typeArgs += typeArg (COMMA typeArgs += typeArg)*)?
    ;

world
    : ARROW returnType = refinedType SLASH outHeap = heap
    |
    ; // T / h

identifierList
    : (idents += IDENTIFIER (COMMA idents += IDENTIFIER)*)?
    ;

functionSchema
    : LBA locations = identifierList RBA LB typeArgs = typeArgList RB SLASH inHeap = heap type = world
    ;

singleFunctionSchema
    : entrypoint = functionSchema EOF
    ;

// Functions

functionDefinition
    : FUN LB(x += IDENTIFIER(COMMA x += IDENTIFIER) *)? RB LBC body = expression RBC COLON schema = functionSchema
    ;

singleFunctionDefinition
    : entrypoint = functionDefinition EOF
    ;

// Structs

structField
    : name = IDENTIFIER ASSIGN fieldtype = blockBinding
    ;

structDefinition
    : STRUCT LB typeArgs = typeArgList RB (COLON depends += functionCall (COMMA depends += functionCall)*)? LBC (fields += structField)+ RBC COLON LBA locations = identifierList RBA
    ;

// Programs

program
    : LETF name = IDENTIFIER ASSIGN definition = functionDefinition IN body = program # functionBinding
    | LETS name = IDENTIFIER ASSIGN definition = structDefinition IN body = program # structBinding
    | name = IDENTIFIER LB RB # mainCall
    ;

// Unit

singleProgram
    : entrypoint = program EOF
    ; // compilation unit
