lexer grammar NanoCLexer ;

@header {
#pragma warning disable CS3021
}

// Keywords

LET : 'let' ;
LETF : 'letf' ;
LETS : 'lets' ;
LETU : 'letu' ;
FOLD : 'fold' ;
UNFOLD : 'unfold' ;
FUN : 'fun' ;
STRUCT : 'struct' ;
IF : 'if' ;
THEN : 'then' ;
ELSE : 'else' ;
ANY : 'any' ;
ASSERT : 'assert' ;
MALLOC : 'malloc' ;
INT : 'int' ;
CHAR : 'char' ;
VOID : 'void' ;
RETURN : 'return' ;
EMP : 'emp' ;
IN : 'in' ;
REF : 'ref' ;

// Operators

AT : '@' ;
PLUS : '+' ;
MINUS : '-' ;
EQUALS : '==' ;
UNEQUAL : '!=' ;
GREATEREQUAL : '>=' ;
LESSEQUAL : '<=' ;
AND : '&&' ;
OR : '||' ;
GREATER : '>' ;
LESS : '<' ;
ASSIGN : '=' ;
LB : '(' ;
RB : ')' ;
LBC : '{' ;
RBC : '}' ;
LBA : '[' ;
RBA : ']' ;
DEREF : '*' ;
COMMA : ',' ;
TARROW : '~>' ;
TILDE : '~' ;
UNDERSCORE : '_' ;
COLON : ':' ;
SLASH : '/' ;
PIPE : '|' ;
ARROW : '->' ;
// DOT : '.' ;

// Identifiers

fragment IDENTIFIERCHAR : [a-zA-Z0-9_] ;
fragment ALPHA : [a-zA-Z] ;
fragment DIGIT : [0-9] ;

IDENTIFIER : ALPHA IDENTIFIERCHAR* ; // f
INTEGER : MINUS? DIGIT+ ; // integer constant

BLOCKCOMMENT: '/*' .*? '*/' -> skip ;
LINECOMMENT: '//' ~ [\r\n]* -> skip ;

WHITESPACE : [ \t\r\n]+ -> skip ;

ERROR : . ;