parser grammar NanoCParser ;

options {
  tokenVocab = NanoCLexer ;
}

@header {
using System.Linq;
using NanoCLang.Entities;
#nullable disable
#pragma warning disable CS3021
}

// Locations

location returns [Location res]
    : name = IDENTIFIER
        { $res = new Location($name.text, false); } # concreteLocation
    | TILDE name = IDENTIFIER
        { $res = new Location($name.text, true); } # abstractLocation
    ;

singleLocation returns [Location res]
    : entrypoint = location EOF
        { $res = $entrypoint.res; }
    ;

// Indices

index returns [Entities.Index res] // Index collides with System.Index
    : value = INTEGER
        { $res = new SingletonIndex(int.Parse($value.text)); } # singleton
    | value = INTEGER PLUS step = INTEGER
        { $res = new SequenceIndex(int.Parse($value.text), int.Parse($step.text)); } # sequence
    | ANY
        { $res = new ArbitraryIndex(); } # arbitraryIndex
    ;

singleIndex returns [Entities.Index res]
    : entrypoint = index EOF
        { $res = $entrypoint.res; }
    ;

// Pure Expressions

pureExpression returns [PureExpression res]
    : left = pureExpression op = (PLUS | MINUS) right = pureExpression
        { $res = $op.text switch {
            "+" => new AdditionExpression($left.res, $right.res),
            "-" => new SubtractionExpression($left.res, $right.res),
            _ => throw new ArgumentOutOfRangeException("Invalid additive operator!")
        }; } # integerAdditive
    | RETURN
        { $res = new IntegerConstant(0, 0); } # voidReturn
    | left = pureExpression op = (LESS | LESSEQUAL | GREATER | GREATEREQUAL) right = pureExpression
        { $res = new RelationalExpression($left.res, $op.text, $right.res); } # relationalComparison
    | left = pureExpression op = (EQUALS | UNEQUAL) right = pureExpression
        { $res = new RelationalExpression($left.res, $op.text, $right.res); } # equalityComparison
    | left = pureExpression op = AND right = pureExpression
        { $res = new BooleanExpression($left.res, $op.text, $right.res); } # conjunctionExpression
    | left = pureExpression op = OR right = pureExpression
        { $res = new BooleanExpression($left.res, $op.text, $right.res); } # disjunctionExpression
    | LB body = pureExpression RB
        { $res = $body.res; } # bracedExpression
    | ASSERT LB body = pureExpression RB
        { $res = new AssertionExpression($body.res); } # assertion
    | AT offset = INTEGER
        { $res = new BlockOffset(int.Parse($offset.text)); } # blockOffset
    | name = IDENTIFIER LB (a += pureExpression (COMMA a += pureExpression)*)? RB
        { $res = new UninterpretedApplicationExpression($name.text, $a.Select(i => i.res).ToArray()); } # unintApplication
    | value = INTEGER UNDERSCORE size = INTEGER
        { $res = new IntegerConstant(int.Parse($value.text), int.Parse($size.text)); } # integerConstant
    | name = IDENTIFIER
        { $res = new VariableExpression($name.text); } # variable
    ;

singlePureExpression returns [PureExpression res]
    : entrypoint = pureExpression EOF
        { $res = $entrypoint.res; }
    ;

// Basic Types

basicType returns [BasicType res]
    : size = INTEGER LBA typeIndex = index RBA
        { $res = new IntegerType(int.Parse($size.text), $typeIndex.res); } # integerType
    | REF LB locationName = location COMMA typeIndex = index RB
        { $res = new ReferenceType($locationName.res, $typeIndex.res); } # referenceType
    | INT
        { $res = new IntegerType(4); } # fourbyteInt
    | CHAR
        { $res = new IntegerType(1); } # singlebyteInt
    | VOID
        { $res = IntegerType.Void; } # singlebyteInt
    ;

singleBasicType returns [BasicType res]
    : entrypoint = basicType EOF
        { $res = $entrypoint.res; }
    ;

// Refined Types

refinedType returns [Entities.Type res]
    : LBC variable = IDENTIFIER COLON baseType = basicType PIPE refinement = pureExpression RBC
        { $res = new RefinedType($variable.text, $baseType.res, $refinement.res); } # refinedBasicType
    | trivial = basicType
        { $res = $trivial.res; } # triviallyRefinedType
    ;

singleRefinedType returns [RefinedType res]
    : entrypoint = refinedType EOF
        { $res = $entrypoint.res as RefinedType; }
    ;

// Expressions

functionCall returns [FunctionCallExpression res]
    : LBA locations = identifierList RBA name = IDENTIFIER LB (a += pureExpression (COMMA a += pureExpression)*)? RB
        { $res = new FunctionCallExpression($name.text, $locations.res, $a.Select(i => i.res).ToArray()); }
    ;

expression returns [Expression res] 
    : IF condition = pureExpression THEN bodyThen = expression ELSE bodyElse = expression
        { $res = new TernaryExpression($condition.res, $bodyThen.res, $bodyElse.res); } # ifThenElse
    | LET name = IDENTIFIER ASSIGN definition = expression IN body = expression
        { $res = new BindingExpression($name.text, $definition.res, $body.res); } # binding
    | LET value = expression IN body = expression
        { $res = new ConcatenationExpression($value.res, $body.res); } # concatenation
    | LETU name = IDENTIFIER ASSIGN LBA UNFOLD locationName = IDENTIFIER RBA definition = pureExpression IN body = expression
        { $res = new UnfoldExpression($name.text, $locationName.text, $definition.res, $body.res); } # locationUnfold
    | LBA FOLD locationName = IDENTIFIER RBA
        { $res = new FoldExpression($locationName.text); } # locationFold
    | DEREF ptr = pureExpression ASSIGN value = pureExpression
        { $res = new PointerWriteExpression($ptr.res, $value.res); } # pointerWrite
    | DEREF ptr = pureExpression
        { $res = new PointerReadExpression($ptr.res); } # pointerRead
    | ptr = pureExpression ARROW member = IDENTIFIER ASSIGN value = pureExpression
        { $res = new StructWriteExpression($ptr.res, $member.text, $value.res); } # structWrite
    | ptr = pureExpression ARROW member = IDENTIFIER
        { $res = new StructReadExpression($ptr.res, $member.text); } # structRead
    | ptr = pureExpression LBA offset = pureExpression RBA ASSIGN value = pureExpression
        { $res = new ArrayWriteExpression($ptr.res, $offset.res, $value.res); } # arrayWrite
    | ptr = pureExpression LBA offset = pureExpression RBA
        { $res = new ArrayReadExpression($ptr.res, $offset.res); } # arrayRead
    | MALLOC LB locationName = IDENTIFIER COMMA size = pureExpression RB
        { $res = new AllocationExpression($locationName.text, $size.res); } # allocation
    | call = functionCall
        { $res = $call.res; } # functionCallExpression
    | body = pureExpression
        { $res = $body.res; } # pureExpAsExp
    ;

singleExpression returns [Expression res]
    : entrypoint = expression EOF
        { $res = $entrypoint.res; }
    ;

// Blocks

blockBinding returns [BlockType res]
    : offset = index COLON type = refinedType
        { $res = new BlockType($offset.res, $type.res); }
    ; // indexed block type binding

block returns [BlockType[] res]
    : (bindings += blockBinding (COMMA bindings += blockBinding)*)?
        { $res = $bindings.Select(i => i.res).ToArray(); }
    ; // block

singleBlock returns [BlockType[] res]
    : entrypoint = block EOF
        { $res = $entrypoint.res; }
    ;

// Heaps

locationBinding returns [LocationBinding res]
    : atLocation = location TARROW bound = block
        { $res = new LocationBinding($atLocation.res, $bound.res); }
    ;

heapElement returns [IHeapElement res]
    : binding = locationBinding
        { $res = $binding.res; }
    | structheap = functionCall
        { $res = $structheap.res; }
    ;

heap returns [IHeapElement[] res]
    : EMP
        { $res = new IHeapElement[] { }; }
    | elems += heapElement (DEREF elems += heapElement)*
        { $res = $elems.Select(i => i.res).ToArray(); }
    ;

singleHeap returns [IHeapElement[] res]
    : entrypoint = heap EOF
        { $res = $entrypoint.res; }
    ;

// Function Schemas

typeArg returns [TypeParameter res]
    : name = IDENTIFIER COLON type = refinedType
        { $res = new TypeParameter($name.text, $type.res); }
    ; // x : T

typeArgList returns [TypeParameter[] res]
    : (typeArgs += typeArg (COMMA typeArgs += typeArg)*)?
        { $res = $typeArgs.Select(i => i.res).ToArray(); }
    ;

world returns [RawWorld res]
    : ARROW returnType = refinedType SLASH outHeap = heap
        { $res = new RawWorld($returnType.res, $outHeap.res); }
    |
        { $res = null; }
    ; // T / h

identifierList returns [string[] res]
    : (idents += IDENTIFIER (COMMA idents += IDENTIFIER)*)?
        { $res = $idents.Select(i => i.Text).ToArray(); }
    ;

functionSchema returns [RawFunctionSchema res]
    : LBA locations = identifierList RBA LB typeArgs = typeArgList RB SLASH inHeap = heap type = world
        { $res = new RawFunctionSchema($locations.res, $typeArgs.res, $inHeap.res, $type.res); }
    ;

singleFunctionSchema returns [RawFunctionSchema res]
    : entrypoint = functionSchema EOF
        { $res = $entrypoint.res; }
    ;

// Functions

functionDefinition returns [FunctionDefinition res]
    : FUN LB(x += IDENTIFIER(COMMA x += IDENTIFIER) *)? RB LBC body = expression RBC COLON schema = functionSchema
        { $res = new FunctionDefinition($body.res, $schema.res); }
    ;

singleFunctionDefinition returns [FunctionDefinition res]
    : entrypoint = functionDefinition EOF
        { $res = $entrypoint.res; }
    ;

// Structs

structField returns [StructField res]
    : name = IDENTIFIER ASSIGN fieldtype = blockBinding
        { $res = new StructField($name.text, $fieldtype.res); }
    ;

structDefinition returns [StructDefinition res]
    : STRUCT LB typeArgs = typeArgList RB (COLON depends += functionCall (COMMA depends += functionCall)*)? LBC (fields += structField)+ RBC COLON LBA locations = identifierList RBA
        { $res = new StructDefinition($locations.res, $typeArgs.res, $depends.Select(i => i.res).ToArray(), $fields.Select(i => i.res).ToArray()); }
    ;

// Programs

program returns [Program res]
    : LETF name = IDENTIFIER ASSIGN definition = functionDefinition IN body = program
        { $res = new FunctionBinding($name.text, $definition.res, $body.res); } # functionBinding
    | LETS name = IDENTIFIER ASSIGN definition = structDefinition IN body = program
        { $res = new StructBinding($name.text, $definition.res, $body.res); } # structBinding
    | name = IDENTIFIER LB RB
        { $res = new MainCall($name.text); } # mainCall
    ;

// Unit

singleProgram returns [Program res]
    : entrypoint = program EOF
        { $res = $entrypoint.res; }
    ; // compilation unit
