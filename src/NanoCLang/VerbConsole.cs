﻿using System;

namespace NanoCLang {
    /// <summary>
    /// Provides the class to toggle the verbosity of the program output during well-formedness checking.
    /// </summary>
    public static class VerbConsole {
        /// <summary>
        /// Gets or sets the current verbosity level. All information that has less verbosity is omitted.
        /// </summary>
        public static VerbosityLevel Verbosity { get; set; } = VerbosityLevel.High;
        /// <summary>
        /// Writes a formatted string to the console depending on the current verbosity level.
        /// </summary>
        /// <param name="level">Level of verbosity to print with. The higher the level of a write, the lower its importance.</param>
        /// <param name="format">A composite format string.</param>
        /// <param name="arg">An array of objects to write using <paramref name="format"/>.</param>
        public static void WriteLine(VerbosityLevel level, string format, params object[] arg) {
            if ((int)Verbosity <= (int)level)
                Console.Error.WriteLine(format, arg);
        }
        /// <summary>
        /// Writes a string to the console depending on the current verbosity level.
        /// </summary>
        /// <param name="level">Level of verbosity to print with. The higher the level of a write, the lower its importance.</param>
        /// <param name="value">The value to write.</param>
        public static void WriteLine(VerbosityLevel level, string value) {
            if ((int)Verbosity <= (int)level)
                Console.Error.WriteLine(value);
        }
    }
    /// <summary>
    /// A verbosity level enumeration that lists possible verbosities.
    /// </summary>
    public enum VerbosityLevel : int {
        /// <summary>
        /// Low verbosity, only outputs on failure.
        /// </summary>
        Quiet = -5,
        /// <summary>
        /// Default verbosity, outputs important information.
        /// </summary>
        Default = 0,
        /// <summary>
        /// High verbosity, outputs additional information.
        /// </summary>
        High = 5
    }
}
