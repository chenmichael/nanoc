﻿using NanoCLang.Entities;
using NanoCLang.Environemnts;
using System;

namespace NanoCLang {
    /// <summary>
    /// Provides an extension class for the NanoC parser that extends it by shortcuts for <see cref="Base"/> entity parsing.
    /// </summary>
    public static class NanoCParserExtensions {
        internal static Exception ParseException => new FormatException("No entity was parsed!");
        /// <summary>
        /// Use the parser to parse an entity and check if the entity was parsed successfully without syntax errors.
        /// </summary>
        /// <typeparam name="T">Type of entity to parse.</typeparam>
        /// <param name="parser">Parser that executes the parsing.</param>
        /// <param name="parse">Function that invokes the parser and returns the entity.</param>
        /// <returns>Parsed entity.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static T ParseEntity<T>(this NanoCParser parser, Func<NanoCParser, T?> parse) where T : class {
            var entity = parse(parser);
            return entity is null || parser.NumberOfSyntaxErrors > 0 ? throw ParseException : entity;
        }
        /// <summary>
        /// Parses a compilation unit including a single program before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed compilation unit</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static Program ParseProgram(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleProgram().res);
        /// <summary>
        /// Parses a single pure expression before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed pure expression.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static PureExpression ParsePureExpression(this NanoCParser parser) => parser.ParseEntity(parser => parser.singlePureExpression().res);
        /// <summary>
        /// Parses a single expression before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed expression.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static Expression ParseExpression(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleExpression().res);
        /// <summary>
        /// Parses a single location before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed location.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static Location ParseLocation(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleLocation().res);
        /// <summary>
        /// Parses a single index before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed index.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static Entities.Index ParseIndex(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleIndex().res);
        /// <summary>
        /// Parses a single basic type before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed basic type.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static BasicType ParseBasicType(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleBasicType().res);
        /// <summary>
        /// Parses a single refined type before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed refined type.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static RefinedType ParseRefinedType(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleRefinedType().res);
        /// <summary>
        /// Parses a single heap before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed heap.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static Heap ParseHeap(this NanoCParser parser) => parser.ParseEntity(parser => parser.ParseHeapElements().GenerateHeap(new GlobalEnvironment()));
        /// <summary>
        /// Parses a single list of heap elements before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed heap elements.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static IHeapElement[] ParseHeapElements(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleHeap().res);
        /// <summary>
        /// Parses a single block before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed block.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static BlockType[] ParseBlock(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleBlock().res);
        /// <summary>
        /// Parses a single function definition before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed function definition.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static FunctionDefinition ParseFunctionDefinition(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleFunctionDefinition().res);
        /// <summary>
        /// Parses a single function schema before the end of input.
        /// </summary>
        /// <param name="parser">Parser that is used to parse the input.</param>
        /// <returns>Parsed function schema.</returns>
        /// <exception cref="FormatException">No entity was parsed successfully.</exception>
        public static RawFunctionSchema ParseFunctionSchema(this NanoCParser parser) => parser.ParseEntity(parser => parser.singleFunctionSchema().res);
    }
}