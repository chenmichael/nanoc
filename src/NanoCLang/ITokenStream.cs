﻿using System.Collections.Generic;

namespace NanoCLang {
    /// <summary>
    /// Interfaces with a formatter that handles a token stream with tokens of type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Type of tokens used.</typeparam>
    /// <typeparam name="A">Type of arguments to the token stream enumerator.</typeparam>
    public interface ITokenStream<T, A> {
        /// <summary>
        /// Gets an enumerator over all tokens in the entity.
        /// </summary>
        /// <returns>Enumerator over the tokens.</returns>
        IEnumerable<T> Tokens(A args);
    }
}