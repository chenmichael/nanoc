﻿using NanoCLang.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NanoCLang.Environemnts {
    /// <summary>
    /// Provides a class for global environemnts mapping function names to their schema.
    /// </summary>
    public class GlobalEnvironment : Dictionary<string, RawFunctionSchema> {
        private readonly Dictionary<string, StructDefinition> structs;
        /// <summary>
        /// Creates a new instance of an empty global environment.
        /// </summary>
        public GlobalEnvironment() : base() {
            structs = new Dictionary<string, StructDefinition>();
        }
        /// <summary>
        /// Evaluates a function with an additional schema binding on the environment.
        /// This function allows for evaluating judgements with additional bindings on an environment without the need to copy the entire environment.
        /// </summary>
        /// <typeparam name="T">Function output to evaluate.</typeparam>
        /// <param name="name">Name of the function binding.</param>
        /// <param name="schema">Schema that the name is bound to.</param>
        /// <param name="p">Function that evaluates the body.</param>
        /// <returns>Evaluated function result.</returns>
        public T With<T>(string name, RawFunctionSchema schema, Func<GlobalEnvironment, T> p) {
            if (!TryAdd(name, schema))
                throw new IllFormedException(schema, $"Cannot bind function to {name} because it was already defined!");
            try {
                return p(this);
            } finally {
                Remove(name);
            }
        }
        /// <summary>
        /// Bind the structure definition to the name in the global environment.
        /// </summary>
        /// <param name="name">Name of the defined structure.</param>
        /// <param name="def">Definition of the structure.</param>
        public void AddStruct(string name, StructDefinition def) {
            structs.Add(name, def);
        }
        /// <inheritdoc cref="With{T}(string, RawFunctionSchema, Func{GlobalEnvironment, T})"/>
        public T With<T>(FunctionBinding def, Func<GlobalEnvironment, T> p) => With(def.Name, def.Definition.Schema, p);
        /// <summary>
        /// Evaluates a function with an additional structure binding on the environment.
        /// This function allows for evaluating judgements with additional bindings on an environment without the need to copy the entire environment.
        /// </summary>
        /// <typeparam name="T">Function output to evaluate.</typeparam>
        /// <param name="name">Name of the function binding.</param>
        /// <param name="structure">Structure that the name is bound to.</param>
        /// <param name="p">Function that evaluates the body.</param>
        /// <returns>Evaluated function result.</returns>
        public T With<T>(string name, StructDefinition structure, Func<GlobalEnvironment, T> p) {
            if (!structs.TryAdd(name, structure))
                throw new IllFormedException(structure, $"Cannot bind structure to {name} because it was already defined!");
            try {
                return p(this);
            } finally {
                structs.Remove(name);
            }
        }
        /// <summary>
        /// Gets the structure definition of the structure that was bound to the <paramref name="name"/> and outputs it.
        /// </summary>
        /// <param name="name">Name of the structure to look for.</param>
        /// <param name="def">Definition of the structure.</param>
        /// <returns><see langword="true"/> iff the name was defined.</returns>
        public bool TryGetStruct(string name, [NotNullWhen(true)] out StructDefinition def) => structs.TryGetValue(name, out def);
        /// <inheritdoc cref="With{T}(string, StructDefinition, Func{GlobalEnvironment, T})"/>
        public T With<T>(StructBinding def, Func<GlobalEnvironment, T> p) => With(def.Name, def.Definition, p);
        /// <inheritdoc/>
        public override string ToString() => string.Join(",", Keys);
    }
}
