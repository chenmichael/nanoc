using Microsoft.Z3;
using NanoCLang.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Type = NanoCLang.Entities.Type;

namespace NanoCLang.Environemnts {
    /// <summary>
    /// Provides a class for local environments.
    /// A local environment maps names in a specific scope to their respective types.
    /// Additionally local environments can contain guard expressions for code flow analysis.
    /// </summary>
    [DebuggerDisplay("{" + nameof(ToString) + "(),nq}")]
    public class LocalEnvironment : Dictionary<string, Type> {
        /// <summary>
        /// Creates a new instance of an empty local environment.
        /// </summary>
        public LocalEnvironment() : base() {
            guards = new Stack<PureExpression>();
            structureLocations = new Dictionary<string, string>();
        }
        /// <summary>
        /// Creates a new instance of a local environment that contains all elements copied from the input environment <paramref name="gamma"/>.
        /// </summary>
        /// <param name="gamma">Environment to copy from.</param>
        public LocalEnvironment(LocalEnvironment gamma) : base(gamma) {
            guards = new Stack<PureExpression>(gamma.guards.Reverse());
            structureLocations = new Dictionary<string, string>(gamma.structureLocations);
        }
        /// <summary>
        /// Creates a new instance of a local environment from a list of type assignments.
        /// </summary>
        /// <param name="types">Type assignments to copy from.</param>
        public LocalEnvironment(IEnumerable<KeyValuePair<string, Type>> types) : base(types) {
            guards = new Stack<PureExpression>();
            structureLocations = new Dictionary<string, string>();
        }
        /// <summary>
        /// Adds links to the local environment.
        /// </summary>
        /// <param name="links">Structure links to add.</param>
        public void AddStructLocs(IEnumerable<KeyValuePair<string, string>> links) {
            foreach (var (key, val) in links)
                structureLocations.Add(key, val);
        }

        /// <summary>
        /// Guard expressions in the current environment.
        /// </summary>
        private readonly Stack<PureExpression> guards;
        /// <summary>
        /// Attempts to get a struct location binding from the environment.
        /// </summary>
        /// <param name="loc">Location that is linked to a struct.</param>
        /// <param name="structName">Name of the struct the <paramref name="loc"/> is linked to.</param>
        /// <returns><see langword="true"/> iff the location link was found.</returns>
        public bool TryGetStructLoc(string loc, [NotNullWhen(true)] out string structName) => structureLocations.TryGetValue(loc, out structName);
        private readonly Dictionary<string, string> structureLocations;
        /// <summary>
        /// Add a guard expression to the environment.
        /// </summary>
        /// <param name="guard">Guard expression that restricts the environment.</param>
        public void PushGuard(PureExpression guard) => guards.Push(guard);
        /// <summary>
        /// Removes and returns the topmost guard expression of the environment.
        /// </summary>
        /// <returns>The guard expression that was removed from the top of the environment.</returns>
        /// <exception cref="InvalidOperationException">No guard expression is on the environment.</exception>
        public PureExpression PopGuard() => guards.Pop();
        /// <inheritdoc/>
        public override string ToString() {
            var sb = new StringBuilder(string.Join(',', this.Select(i => $"{i.Key}:{i.Value}")));
            if (guards.Count > 0) {
                foreach (var guard in guards) sb.Append('|').Append(guard.ToString());
            }
            return sb.ToString();
        }
        /// <summary>
        /// Evaluates a function with a guarded localenvironment.
        /// </summary>
        /// <typeparam name="T">Function output to evaluate</typeparam>
        /// <param name="guard">Guard expression.</param>
        /// <param name="p">Function that evaluates the body.</param>
        /// <returns>Evaluated function result.</returns>
        public T Guarded<T>(PureExpression guard, Func<LocalEnvironment, T> p) {
            PushGuard(guard);
            try {
                return p(this);
            } finally {
                if (guard != PopGuard()) throw new InvalidProgramException("Guard stack was changed out of scope!");
            }
        }
        /// <summary>
        /// Evaluates a function with additional bindings on the environment.
        /// This function allows for evaluating judgements with additional bindings on an environment without the need to copy the entire environment.
        /// </summary>
        /// <typeparam name="T">Function output to evaluate.</typeparam>
        /// <param name="name">Name of the binding.</param>
        /// <param name="type">Type that the name is bound to.</param>
        /// <param name="p">Function that evaluates the body.</param>
        /// <returns>Evaluated function result.</returns>
        public T With<T>(string name, Type type, Func<LocalEnvironment, T> p) {
            // Get the value that was overridden by the name (can be null)
            var overridden = TryGetValue(name, out var t) ? t : null;
            this[name] = type;
            try {
                return p(this);
            } finally {
                if (overridden is Type) this[name] = overridden;
                else Remove(name);
            }
        }
        /// <summary>
        /// Evaluates a function with additional bindings on the environment.
        /// </summary>
        /// <typeparam name="T">Function output to evaluate.</typeparam>
        /// <param name="bindings">Additional bindings on the envionment.</param>
        /// <param name="p">Function that evaluates the body.</param>
        /// <returns>Evaluated function result.</returns>
        public T With<T>(IEnumerable<KeyValuePair<string, Type>> bindings, Func<LocalEnvironment, T> p) {
            var below = new Dictionary<string, Type?>(bindings.Select(i => i.Key).Select(i => new KeyValuePair<string, Type?>(i, TryGetValue(i, out var t) ? t : null)));
            try {
                foreach (var (name, type) in bindings)
                    this[name] = type;
                return p(this);
            } finally {
                foreach (var (name, type) in below)
                    if (type is Type) this[name] = type;
                    else Remove(name);
            }
        }
        /// <summary>
        /// Evaluates a function with additional bindings on the environment.
        /// </summary>
        /// <typeparam name="T">Function output to evaluate.</typeparam>
        /// <param name="bindings">Additional bindings on the envionment.</param>
        /// <param name="p">Function that evaluates the body.</param>
        /// <returns>Evaluated function result.</returns>
        public T WithStructLoc<T>(IEnumerable<KeyValuePair<string, string>> bindings, Func<LocalEnvironment, T> p) {
            var below = new Dictionary<string, string?>(bindings.Select(i => i.Key).Select(i => new KeyValuePair<string, string?>(i, structureLocations.TryGetValue(i, out var t) ? t : null)));
            try {
                foreach (var (location, @struct) in bindings)
                    structureLocations[location] = @struct;
                return p(this);
            } finally {
                foreach (var (location, @struct) in below)
                    if (@struct is string) structureLocations[location] = @struct;
                    else structureLocations.Remove(location);
            }
        }
        /// <summary>
        /// Executes an action with additional bindings on the environment.
        /// </summary>
        /// <param name="name">Name of the binding.</param>
        /// <param name="type">Type that the name is bound to.</param>
        /// <param name="p">Action to execute with the additional binding.</param>
        public void With(string name, Type type, Action<LocalEnvironment> p) =>
            _ = With(name, type, gamma => { p(this); return true; });
        /// <summary>
        /// Converts the expression to an boolean expression that the SMT solver can handle.
        /// </summary>
        /// <param name="smt">SMT solver context.</param>
        /// <returns>Boolean expression that makes up the environment.</returns>
        public BoolExpr ToBoolean(NanoCSMT smt)
        => smt.Context.MkAnd(guards.Select(i => i.ToBoolean(this, smt))
            .Concat(EmbedEUFA(smt).ToList()));
        private IEnumerable<BoolExpr> EmbedEUFA(NanoCSMT smt) {
            foreach (var entry in this) {
                if (!(entry.Value is RefinedType r)) continue;
                var refinement = r.Refinement.Replace(new Substitutor() { Variables = new Dictionary<string, PureExpression>() { { r.RefinementParameter, new VariableExpression(entry.Key) } } });
                yield return refinement.ToBoolean(this, smt);
            }
        }
    }
}
