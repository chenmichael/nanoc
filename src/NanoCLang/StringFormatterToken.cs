﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang {
    /// <summary>
    /// Provides a superclass for string formatter tokens.
    /// </summary>
    public abstract class StringFormatterToken {
        /// <summary>
        /// Implicitly converts a string value to a token with the same <paramref name="value"/>.
        /// </summary>
        /// <param name="value">Value of the token.</param>
        public static implicit operator StringFormatterToken(string value) => new StringToken(value);
        /// <summary>
        /// Create a new token list that concatenates the list of tokens with the separator inbetween every two tokens.
        /// </summary>
        /// <param name="tokens">Enumeration of tokens.</param>
        /// <param name="separator">Separator between the output tokens.</param>
        /// <returns>Enumeration of tokens with <paramref name="separator"/> inbetween the <paramref name="tokens"/>.</returns>
        public static IEnumerable<StringFormatterToken> Separated(IEnumerable<StringFormatterToken> tokens, StringFormatterToken separator) {
            bool first = true;
            foreach (var token in tokens) {
                if (first) first = false;
                else yield return separator;
                yield return token;
            }
        }
        /// <inheritdoc cref="Separated(IEnumerable{StringFormatterToken}, StringFormatterToken)"/>
        public static IEnumerable<StringFormatterToken> Separated(IEnumerable<StringFormatterToken> tokens, string separator)
            => Separated(tokens, (StringFormatterToken)separator);
        /// <inheritdoc cref="Separated(IEnumerable{StringFormatterToken}, StringFormatterToken)"/>
        public static IEnumerable<StringFormatterToken> Separated<T, A>(IEnumerable<T> objs, A args, StringFormatterToken separator) where T : ITokenStream<StringFormatterToken, A> {
            bool first = true;
            foreach (var obj in objs) {
                if (first) first = false;
                else if (separator is StringToken s) foreach (var tk in s.ReplaceNewLines()) yield return tk;
                else yield return separator;
                foreach (var token in obj.Tokens(args)) yield return token;
            }
        }
        /// <summary>
        /// Prints the <paramref name="inner"/> tokens indented by the <paramref name="indent"/> amount.
        /// </summary>
        /// <param name="indent">Amount of indentation.</param>
        /// <param name="inner">Indented block</param>
        /// <returns>Token enumeration that represents the indented block.</returns>
        public static IEnumerable<StringFormatterToken> Indented(int indent, Func<IEnumerable<StringFormatterToken>> inner) {
            yield return new IndentChangeToken(indent);
            foreach (var tk in inner()) yield return tk;
            yield return new IndentChangeToken(-indent);
        }
        /// <inheritdoc cref="Separated(IEnumerable{StringFormatterToken}, StringFormatterToken)"/>
        public static IEnumerable<StringFormatterToken> Separated<T, A>(IEnumerable<T> blocks, A args, string separator) where T : ITokenStream<StringFormatterToken, A>
            => Separated(blocks, args, (StringFormatterToken)separator);
        /// <inheritdoc cref="Separated(IEnumerable{StringFormatterToken}, StringFormatterToken)"/>
        public static IEnumerable<StringFormatterToken> Separated(IEnumerable<string> strings, StringFormatterToken separator) => Separated(strings.Select(i => (StringFormatterToken)i), separator);
        /// <inheritdoc cref="Separated(IEnumerable{StringFormatterToken}, StringFormatterToken)"/>
        public static IEnumerable<StringFormatterToken> Separated(IEnumerable<string> strings, string separator) => Separated(strings, (StringFormatterToken)separator);
    }
    /// <summary>
    /// Provides a class for token enumerations for the string formatter <see cref="StringFormatter"/>.
    /// </summary>
    public class StringToken : StringFormatterToken {
        /// <summary>
        /// Constructs a new string token with a given <paramref name="value"/>.
        /// </summary>
        /// <param name="value">Value of the token.</param>
        public StringToken(string value) {
            Value = value;
        }
        /// <summary>
        /// Gets the value of the token.
        /// </summary>
        public string Value { get; }
        /// <summary>
        /// Replace newlines in the value with newline tokens.
        /// </summary>
        /// <returns>Enumeration of tokens with newlines as tokens.</returns>
        internal IEnumerable<StringFormatterToken> ReplaceNewLines()
            => Separated(Value.Split('\n', StringSplitOptions.None), new NewLineToken());
    }
    /// <summary>
    /// Tokens used to push the current indentation of the formatter.
    /// </summary>
    public class IndentChangeToken : StringFormatterToken {
        /// <summary>
        /// Constructs a new indent change token that increases/decreases the current indent by <paramref name="amount"/>.
        /// </summary>
        /// <param name="amount">Indent change amount.</param>
        public IndentChangeToken(int amount) {
            Amount = amount;
        }
        /// <summary>
        /// The amount of indent to insert/remove.
        /// </summary>
        public int Amount { get; }
    }
    /// <summary>
    /// Token used to print a new line in the formatter (recognizes formatters indent)
    /// </summary>
    public class NewLineToken : StringFormatterToken { }
}