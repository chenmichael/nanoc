﻿using NanoCLang.Entities;
using System;

namespace NanoCLang {
    /// <summary>
    /// Represents errors that occur during well-formedness checking.
    /// </summary>
    public class IllFormedException : Exception {
        /// <summary>
        /// Gets or sets the entity that is ill-formed.
        /// </summary>
        public Base Entity { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="IllFormedException"/> class with a specified error <paramref name="message"/> and an erroneous <paramref name="entity"/>.
        /// </summary>
        /// <param name="entity">Entity that raised the error.</param>
        /// <param name="message">The message that describes the error.</param>
        public IllFormedException(Base entity, string message) : base(message) {
            Entity = entity;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="IllFormedException"/> class with a specified error <paramref name="message"/>, a reference to an <paramref name="innerException"/> that caused the error and an erroneous <paramref name="entity"/>.
        /// </summary>
        /// <param name="entity">Entity that raised the error.</param>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference</param>
        public IllFormedException(Base entity, string message, Exception innerException) : base(message, innerException) {
            Entity = entity;
        }
    }
}