﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class for comparing two heap objects, or rather collections of location bindings.
    /// </summary>
    public class HeapComparer : IEqualityComparer<IDictionary<Location, BlockType[]>> {
        /// <summary>
        /// Provides the default instance of a heap comparer.
        /// </summary>
        public static HeapComparer Default { get; } = new HeapComparer();
        /// <inheritdoc cref="IEqualityComparer{T}.Equals(T, T)"/>
        public bool Equals(IDictionary<Location, BlockType[]> x, IDictionary<Location, BlockType[]> y) => x.Count == y.Count && x.Keys.All(key => y.ContainsKey(key) && x[key].SequenceEqual(y[key]));
        /// <inheritdoc cref="IEqualityComparer{T}.GetHashCode(T)"/>
        public int GetHashCode(IDictionary<Location, BlockType[]> obj) {
            var hash = new HashCode();
            foreach (var item in obj) {
                hash.Add(item.Key);
                foreach (var block in item.Value) hash.Add(block);
            }
            return hash.ToHashCode();
        }
    }
}