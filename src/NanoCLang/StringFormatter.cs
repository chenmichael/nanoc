﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NanoCLang {
    /// <summary>
    /// Provides a class for formatting a NanoC entity to a string representation.
    /// </summary>
    public class StringFormatter {
        private const int DEFAULT_INDENT = 4;
        private const char INDENT_CHAR = ' ';
        private readonly StringBuilder sb = new StringBuilder();
        private int indent = 0;

        /// <summary>
        /// Creates a string representation of the <paramref name="tokens"/> stream.
        /// </summary>
        /// <param name="tokens">Token stream that is converted to a string representation.</param>
        /// <returns>String representation of the <paramref name="tokens"/>.</returns>
        public string ToString(IEnumerable<StringFormatterToken> tokens) {
            foreach (var token in tokens) {
                AppendToken(token);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Appends the <paramref name="token"/> to the string builder.
        /// </summary>
        /// <param name="token">Token to append</param>
        /// <exception cref="ArgumentOutOfRangeException">Token has unexpected type.</exception>
        internal StringFormatter AppendToken(StringFormatterToken token) {
            switch (token) {
            case StringToken tk: sb.Append(tk.Value); break;
            case NewLineToken _: NewLine(); break;
            case IndentChangeToken tk:
                if (tk.Amount == 0) PrintIndent();
                else indent += tk.Amount;
                break;
            default: throw new ArgumentOutOfRangeException("Unknown token type!");
            }
            return this;
        }

        /// <summary>
        /// Inserts a new line into the formatter while keeping track of the current indentation.
        /// </summary>
        /// <returns>Formatter after the new line and indent was inserted.</returns>
        public StringFormatter NewLine() {
            sb.AppendLine();
            PrintIndent();
            return this;
        }

        private void PrintIndent() {
            for (int i = 0; i < indent; i++) sb.Append(INDENT_CHAR);
        }

        /// <summary>
        /// Prints a multi-line text block containing <see cref="NewLine"/> while indenting the block further than the current indentation.
        /// </summary>
        /// <param name="print">Function of the formatter that prints the contents of the block.</param>
        /// <param name="amount">Amount of indentation.</param>
        /// <returns>Formatter after the new indented block was inserted.</returns>
        public StringFormatter Indented(Func<StringFormatter, StringFormatter> print, int amount = DEFAULT_INDENT) {
            indent += amount;
            try {
                return print(this);
            } finally {
                indent -= amount;
            }
        }
    }
}