﻿namespace NanoCLang {
    /// <summary>
    /// Provides a class for setting formatting options on C code formatting.
    /// </summary>
    public class CSourceFormat : NanoCSourceFormat {
        /// <summary>
        /// Gets or sets a flag that specifies whether a pure expression is used as an expression.
        /// </summary>
        public bool PureRequiresReturn { get; set; } = false;
        /// <summary>
        /// Gets or sets a flag that specifies whether the program header was already output.
        /// </summary>
        public bool HeaderPrinted { get; set; } = false;
    }
}