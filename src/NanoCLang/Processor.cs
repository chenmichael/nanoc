﻿using Antlr4.Runtime;
using NanoCLang.Entities;
using System;
using System.IO;

namespace NanoCLang {
    /// <summary>
    /// Provides the interface to other assemblies that compile source files.
    /// </summary>
    public static class Processor {
        /// <summary>
        /// Compile the source file and returns the program entity.
        /// </summary>
        /// <param name="sourcefile">File to be compiled.</param>
        /// <returns>Program entity described by the NanoC source file.</returns>
        public static Program? Compile(string sourcefile) {
            var stream = new AntlrFileStream(sourcefile);
            return Compile(stream);
        }

        /// <summary>
        /// Compile the source stream and returns the program entity.
        /// </summary>
        /// <param name="stream">File stream to be compiled.</param>
        /// <returns>Program entity described by the NanoC char stream.</returns>
        private static Program? Compile(ICharStream stream) {
            var lexer = new NanoCLexer(stream, Console.Out, Console.Error);
            var tokens = new CommonTokenStream(lexer, Lexer.DefaultTokenChannel);
            return Compile(tokens);
        }

        /// <inheritdoc cref="GetParser(string, TextWriter, TextWriter)"/>
        public static NanoCParser GetParser(string input) => GetParser(input, Console.Out, Console.Error);
        /// <summary>
        /// Gets a NanoC string parser for the given <paramref name="input"/> string.
        /// </summary>
        /// <param name="input">Input string to the parser.</param>
        /// <param name="output">Output stream to write to.</param>
        /// <param name="errorOutput">Error stream to write to.</param>
        /// <returns>Parser for the input string.</returns>
        public static NanoCParser GetParser(string input, TextWriter output, TextWriter errorOutput) {
            var stream = new AntlrInputStream(input);
            var lexer = new NanoCLexer(stream, output, errorOutput);
            var tokens = new CommonTokenStream(lexer, Lexer.DefaultTokenChannel);
            return new NanoCParser(tokens, output, errorOutput) {
                BuildParseTree = true
            };
        }
        /// <inheritdoc cref="GetParser(Stream, TextWriter, TextWriter)"/>
        public static NanoCParser GetParser(Stream input) => GetParser(input, Console.Out, Console.Error);
        /// <summary>
        /// Gets a NanoC stream parser for the given <paramref name="input"/> string.
        /// </summary>
        /// <param name="input">Input stream to the parser.</param>
        /// <param name="output">Output stream to write to.</param>
        /// <param name="errorOutput">Error stream to write to.</param>
        /// <returns>Parser for the input stream.</returns>
        public static NanoCParser GetParser(Stream input, TextWriter output, TextWriter errorOutput) {
            var stream = new AntlrInputStream(input);
            var lexer = new NanoCLexer(stream, output, errorOutput);
            var tokens = new CommonTokenStream(lexer, Lexer.DefaultTokenChannel);
            return new NanoCParser(tokens, output, errorOutput) {
                BuildParseTree = true
            };
        }
        /// <summary>
        /// Parse a program from a <see cref="TextReader"/> <paramref name="input"/>.
        /// </summary>
        /// <param name="input">Input stream to the parser.</param>
        /// <param name="output">Output stream to write to.</param>
        /// <param name="errorOutput">Error stream to write to.</param>
        /// <returns>Parsed program.</returns>
        /// <exception cref="FormatException">Program was not parsed successfully.</exception>
        public static Program ParseProgram(TextReader input, TextWriter output, TextWriter errorOutput) {
            var stream = new AntlrInputStream(input);
            var lexer = new NanoCLexer(stream, output, errorOutput);
            var tokens = new CommonTokenStream(lexer, Lexer.DefaultTokenChannel);
            var parser = new NanoCParser(tokens, output, errorOutput) {
                BuildParseTree = true
            };
            return parser.ParseProgram();
        }
        /// <summary>
        /// Compile the token stream and returns the program entity.
        /// </summary>
        /// <param name="tokens">NanoC token stream to be compiled.</param>
        /// <returns>Program entity described by the lexed NanoC token stream.</returns>
        private static Program? Compile(ITokenStream tokens) {
            var parser = new NanoCParser(tokens) {
                BuildParseTree = true
            };
            parser.AddErrorListener(new DiagnosticErrorListener(false));
            var program = parser.ParseProgram();
            if (parser.NumberOfSyntaxErrors > 0)
                return null;
            return program;
        }
    }
}
