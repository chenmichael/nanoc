﻿using System.Collections.Generic;
using System.Linq;

namespace NanoCLang.Entities {
    /// <summary>
    /// Provides a class that compares two arrays of heap elements by building a heap from them.
    /// </summary>
    public class HeapElementComparer : IEqualityComparer<IHeapElement[]> {
        /// <summary>
        /// Returns a default equality comparer for heap elements.
        /// </summary>
        public static HeapElementComparer Default { get; } = new HeapElementComparer();
        private static IEnumerable<T> CastWhere<T>(IEnumerable<IHeapElement> items) where T : class => items.Where(i => i is T).Select(i => (i as T)!);
        private static Heap GetDirectBindings(IHeapElement[] x) => new Heap(CastWhere<LocationBinding>(x).Concat(CastWhere<Heap>(x).SelectMany(i => i.BoundLocations)));
        private static IEnumerable<IHeapElement> GetOthers(IHeapElement[] x) => CastWhere<FunctionCallExpression>(x).OrderBy(i => i.GetHashCode());
        /// <summary>
        /// Compares the heap element lists by creating a heap from the locations and comparing that, after that comparing the rest of the heap elements.
        /// </summary>
        /// <param name="x">Left operand</param>
        /// <param name="y">Right operand</param>
        /// <returns>True if the heap elements refer to the same heap after construction with the global environment.</returns>
        public bool Equals(IHeapElement[] x, IHeapElement[] y) {
            var l = GetDirectBindings(x);
            var r = GetDirectBindings(y);
            if (!EqualityComparer<Heap>.Default.Equals(l, r)) return false;
            return GetOthers(x).SequenceEqual(GetOthers(y));
        }
        /// <inheritdoc cref="IEqualityComparer{T}.GetHashCode(T)"/>
        public int GetHashCode(IHeapElement[] obj) => throw new System.NotImplementedException();
    }
}