﻿using Microsoft.Z3;
using NanoCLang.Entities;
using NanoCLang.Environemnts;
using System;
using System.Linq;

namespace NanoCLang {
    /// <summary>
    /// Provides a class for validating subtyping refinements using an SMT solver.
    /// </summary>
    public class NanoCSMT : IDisposable {
        /// <summary>
        /// Gets the default instance of the NanoC SMT solver.
        /// </summary>
        public static NanoCSMT Default { get; } = new NanoCSMT();
        /// <summary>
        /// Gets the currently used instance of the SMT context.
        /// </summary>
        public Context Context { get; }
        /// <summary>
        /// Gets the number of SMT solver invocations of this solver instance.
        /// </summary>
        public int InvocationCount { get; private set; } = 0;
        private readonly Solver solver;
        private readonly FuncDecl _bs;
        private readonly FuncDecl _be;
        /// <summary>
        /// Creates a new instance of the NanoC SMT solver.
        /// </summary>
        private NanoCSMT() {
            Context = new Context();
            solver = Context.MkSolver("QF_LIA");
            _bs = Context.MkFuncDecl(Context.MkSymbol("BS"), Context.IntSort, Context.IntSort);
            _be = Context.MkFuncDecl(Context.MkSymbol("BE"), Context.IntSort, Context.IntSort);
        }
        /// <inheritdoc cref="Entities.Index.CollidesWith(int, Entities.Index, int)"/>
        public bool IndicesCollide(Entities.Index index1, int size, Entities.Index index2, int otherSize) => Scoped(solver => {
            (var guard1, var value1) = index1.SMTPossibleValues(this, size, "l_");
            (var guard2, var value2) = index2.SMTPossibleValues(this, otherSize, "r_");
            solver.Assert(guard1, guard2, Context.MkEq(value1, value2));
            VerbConsole.WriteLine(VerbosityLevel.Default, "Solving: {0} with type {1} collides {2} with type {3}!", index1, size, index2, otherSize);
            switch (InvokeSolver(solver)) {
            case Status.UNSATISFIABLE: return false;
            case Status.SATISFIABLE:
                VerbConsole.WriteLine(VerbosityLevel.Default, "Bad: Indices collide at offset {0}!", solver.Model.Eval(value1));
                return true;
            default: throw new IllFormedException(index1, $"Cannot determine index collision with fallback SMT solver!");
            }
        });
        /// <summary>
        /// Invokes the SMT solver to solve if the expression is always truthy.
        /// </summary>
        /// <param name="gamma">Current environment.</param>
        /// <param name="exp">Antecedent of the implication.</param>
        /// <returns>Boolean value that represents the value of the expression or <see langword="null"/> if the expression is constant.</returns>
        public bool? TryEvalConstExpression(LocalEnvironment gamma, PureExpression exp) => Scoped(solver => {
            if (gamma.Count > 0)
                solver.Assert(gamma.ToBoolean(this));
            return Scoped(solver => {
                solver.Assert(!exp.ToBoolean(gamma, this));
                VerbConsole.WriteLine(VerbosityLevel.Default, "Solving: {0} always true\n{1}", exp, solver);
                switch (InvokeSolver(solver)) {
                case Status.UNSATISFIABLE: return true;
                case Status.UNKNOWN: return false;
                case Status.SATISFIABLE: return false;
                default: throw new IllFormedException(exp, $"Unexpected status in SMT solver!");
                }
            })
                ? true
                : Scoped(solver => {
                    solver.Assert(exp.ToBoolean(gamma, this));
                    VerbConsole.WriteLine(VerbosityLevel.Default, "Solving: {0} always false\n{1}", exp, solver);
                    switch (InvokeSolver(solver)) {
                    case Status.UNSATISFIABLE: return true;
                    case Status.UNKNOWN: return false;
                    case Status.SATISFIABLE: return false;
                    default: throw new IllFormedException(exp, $"Unexpected status in SMT solver!");
                    }
                }) ? false
                   : (bool?)null;
        });
        /// <summary>
        /// Safely pushes a solver scope and evaluates the function with the scoped solver.
        /// </summary>
        /// <typeparam name="T">Type of the return value to evaluate.</typeparam>
        /// <param name="p">Function to evaluate the return value with the solver.</param>
        /// <returns>Evaluated result inside the scope.</returns>
        private T Scoped<T>(Func<Solver, T> p) {
            solver.Push();
            try {
                return p(solver);
            } finally {
                solver.Pop();
            }
        }
        /// <summary>
        /// Invokes the SMT solver to solve Valid([gamma] and [a1] implies [a2]).
        /// This checks if <paramref name="a1"/> implies that <paramref name="a2"/> holds given the environment <paramref name="gamma"/>.
        /// </summary>
        /// <param name="gamma">Current environment.</param>
        /// <param name="a1">Antecedent of the implication.</param>
        /// <param name="a2">Consequent of the implication.</param>
        /// <returns><see langword="true"/> iff the implication holds, otherwise <see langword="false"/>.</returns>
        /// <exception cref="IllFormedException">Solver is unable to check if the implication holds.</exception>
        public bool ValidImplication(LocalEnvironment gamma, PureExpression a1, PureExpression a2) => Scoped(solver => {
            VerbConsole.WriteLine(VerbosityLevel.Default, $"Checking implication {a1} => {a2}...");
            if (gamma.Count > 0)
                solver.Assert(gamma.ToBoolean(this));
            solver.Assert(!Context.MkImplies(a1.ToBoolean(gamma, this), a2.ToBoolean(gamma, this)));
            VerbConsole.WriteLine(VerbosityLevel.Default, "Solving: {0}", solver);
            switch (InvokeSolver(solver)) {
            case Status.UNSATISFIABLE: return true;
            case Status.UNKNOWN: throw new IllFormedException(a2, $"Refinement cannot be verified!");
            case Status.SATISFIABLE:
                VerbConsole.WriteLine(VerbosityLevel.Default, $"Subtyping broken for values:\n\t{string.Join("\n\t", solver.Model.Consts.Select<System.Collections.Generic.KeyValuePair<FuncDecl, Expr>, string>(i => $"{i.Key.Name} = {i.Value}"))}");
                return false;
            default: throw new IllFormedException(a2, $"Unexpected status in SMT solver!");
            }
        });

        private Status InvokeSolver(Solver solver) {
            InvocationCount++;
            return solver.Check();
        }

        /// <inheritdoc/>
        public void Dispose() {
            ((IDisposable)Context).Dispose();
            ((IDisposable)solver).Dispose();
        }
        #region Support functions
        /// <summary>
        /// Creates a new boolean expression that ensures the pointer <paramref name="v"/> is is correctly offset by <paramref name="i"/> in the same block as the pointer <paramref name="x"/>.
        /// </summary>
        /// <param name="v">Initial pointer.</param>
        /// <param name="x">Other pointer in the same block.</param>
        /// <param name="i">Offset between the pointers.</param>
        /// <returns><see langword="true"/> expression iff the pointer is offset correctly.</returns>
        public BoolExpr PAdd(IntExpr v, IntExpr x, ArithExpr i) => Context.MkAnd(Context.MkEq(v, x + i), Context.MkEq(BS(v), BS(x)), Context.MkEq(BE(v), BE(x)));
        /// <summary>
        /// Creates a new boolean expression that ensures that the pointer <paramref name="v"/> points to the start of a block of length <paramref name="n"/>.
        /// </summary>
        /// <param name="v">Start of the current block (index 0).</param>
        /// <param name="n">Length of the block in bytes.</param>
        /// <returns><see langword="true"/> expression iff the block has the given length and position.</returns>
        public BoolExpr BLen(IntExpr v, ArithExpr n) => Context.MkAnd(Context.MkEq(BS(v), v), Context.MkEq(BE(v), v + n));
        /// <summary>
        /// Creates a new boolean expression that ensures that a pointer is safely within bounds of the current block.
        /// </summary>
        /// <param name="v">Pointer to check for memory safety.</param>
        /// <returns><see langword="true"/> expression iff the pointer is within safe bounds.</returns>
        public BoolExpr Safe(IntExpr v) => Context.MkAnd(v > 0, BS(v) <= v, v < BE(v));
        /// <summary>
        /// Creates a new boolean expression that ensures that a pointer is safely within bounds of the current block and that the entire region of memory contained in this and the next <paramref name="v"/> bytes is within bounds of the allocation.
        /// This is essentially equal to SafeN(v, n) &lt;=&gt; AND_{0 &lt;= i &lt;= n} Safe(v + i)
        /// </summary>
        /// <param name="v">Pointer to check for memory safety.</param>
        /// <param name="n">Length of the memory block in bytes.</param>
        /// <returns><see langword="true"/> expression iff the pointer and its block is within safe bounds.</returns>
        internal BoolExpr SafeN(IntExpr v, IntExpr n) => Context.MkAnd(v > 0, BS(v) <= v, (v + n) <= BE(v));
        /// <summary>
        /// Creates a new integer expression that maps a pointer to the start of the block that it points to.
        /// </summary>
        /// <param name="v">Pointer in the same block.</param>
        /// <returns>Integer expression to the start of the block.</returns>
        public IntExpr BS(IntExpr v) => (IntExpr)_bs[v];
        /// <summary>
        /// Creates a new integer expression that maps a pointer to the end (exclusive) of the block that it points to.
        /// </summary>
        /// <param name="v">Pointer in the same block.</param>
        /// <returns>Integer expression to the end (exclusive) of the block.</returns>
        public IntExpr BE(IntExpr v) => (IntExpr)_be[v];
        #endregion
    }
}