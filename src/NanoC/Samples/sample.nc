letf write = fun() {
    *pt = val
} : [l]
(pt : {v : ref(l, 0) | Safe(v)}, val : {v : 4[any] | 1_1})
/ ~l ~> 0 : {v : 4[any] | 1_1}
* l ~> 0 : {v : 4[any] | 1_1}
-> {v : 0[0] | 1_1}
/ ~l ~> 0 : {v : 4[any] | 1_1}
* l ~> 0 : {v : 4[any] | v + val} in

letf noop = fun() {
    0_0
} : []
()
/ emp
-> {v : 0[0] | 1_1}
/ emp in

noop()