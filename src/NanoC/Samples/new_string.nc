lets string_data = struct() {
    chars = 0+1 : char
} : [data] in

lets string = struct(n : {v : int | (v > 0_4)}) : [data]string_data() {
    length = 0 : {v : int | (v == n)}
    raw = 4 : {v : ref(~data, 0+1) | (Safe(v) && BLen(v, length))}
} : [string, data] in

letf init_alloc = fun(n, res, i, val) {
    let res[i] = val in
    let next = (i + 1_4) in
    if (next < n)
    then [l]init_alloc(n, res, next, val)
    else return
} : [l]
(n : {v : int | (v > 0_4)}, res : {v : ref(l, 0+1) | (Safe(v) && BLen(v, n))}, i : {v : 4[0+1] | ((v >= 0_4) && (v < n))}, val : char)
/ [l]string_data()
* l ~> 0+1 : char
-> 0[0]
/ [l]string_data()
* l ~> 0+1 : char in

letf make_string = fun(n) {
    let res = malloc(l, n) in
    let [l]init_alloc(n, res, 0_4, 0_1) in
    let [fold l] in
    res
} : [l]
(n : {v : int | (v > 0_4)})
/ [l]string_data()
-> {v : ref(~l, 0) | (Safe(v) && BLen(v, n))}
/ [l]string_data() in

letf new_string = fun(n) {
    let str = malloc(string, 12_4) in
    let str->length = n in
    let data = [data]make_string(n) in
    let str->raw = data in
    let [fold string] in
    str
} : [string, data]
(n : {v : int | (v > 0_4)})
/ [string, data]string(n)
-> {v : ref(~string, 0) | (Safe(v) && BLen(v, 12_4))}
/ [string, data]string(n) in

letf call = fun() {
    let ptr = [string, data]new_string(10_4) in
    letu str = [unfold string]ptr in
    let data = str->raw in
    letu dataPtr = [unfold data]data in
    let dataPtr[9_4] = 24_1 in
    let lenArr = str[0_4] in
    let lenStr = str->length in
    let assert(lenArr == lenStr && lenStr == 10_4) in
    let [fold data] in
    [fold string]
} : [string, data]
()
/ [string, data]string(10_4) in

call()