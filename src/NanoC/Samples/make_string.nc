letf init_alloc = fun(n, res, i, val) {
    let *(res + i) = val in
    let next = (i + 1_4) in
    if (next < n)
    then [l]init_alloc(n, res, next, val)
    else 0_0
} : [l]
(n : {v : 4[any] | (v > 0_4)}, res : {v : ref(l, 0+1) | (Safe(v) && BLen(v, n))}, i : {v : 4[0+1] | ((v >= 0_4) && (v < n))}, val : 1[any])
/ ~l ~> 0+1 : 1[any]
* l ~> 0+1 : 1[any]
-> {v : 0[0] | 1_1}
/ ~l ~> 0+1 : 1[any]
* l ~> 0+1 : 1[any] in

letf make_string = fun(n) {
    let res = malloc(l, n) in
    let [l]init_alloc(n, res, 0_4, 0_1) in
    let [fold l] in
    res
} : [l]
(n : {v : 4[any] | (v > 0_4)})
/ ~l ~> 0+1 : 1[any]
-> {v : ref(~l, 0) | (Safe(v) && BLen(v, n))}
/ ~l ~> 0+1 : 1[any] in

letf test = fun() {
    let [l]make_string(12_4) in
    0_0
} : []
()
/ ~l ~> 0+1 : 1[any]
-> {v : 0[0] | 1_1}
/ ~l ~> 0+1 : 1[any] in

test()