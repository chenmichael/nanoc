﻿using CommandLine;
using NanoCLang;
using NanoCLang.Entities;
using System;
using System.IO;
using NanoCProgram = NanoCLang.Entities.Program;

namespace NanoCC {
    /// <summary>
    /// Main entrypoint class of the compiler frontend.
    /// </summary>
    public static class Program {
        /// <summary>
        /// Main entrypoint used to parse command line arguments and direct verbs.
        /// </summary>
        /// <param name="args">Arguments to parse.</param>
        /// <returns>Program exit code.</returns>
        public static int Main(string[] args) => Parser.Default.ParseArguments<CheckOptions, TranslationOptions, FormattingOptions>(args).MapResult(
            (CheckOptions opts) => CheckCode(opts),
            (TranslationOptions opts) => TranslateCode(opts),
            (FormattingOptions opts) => FormatCode(opts),
            errors => 1
        );
        /// <summary>
        /// Parse and typecheck a program from the input options specified file.
        /// </summary>
        /// <param name="opts">Options that specify the input source.</param>
        /// <returns>Well-formed NanoC program or <see langword="null"/> if parsing or well-formedness failed.</returns>
        public static NanoCProgram? GetProgram(InputOption opts) {
            NanoCProgram program;
            using var reader = GetSafeInputReader(opts);
            if (reader is null) return null;

            VerbConsole.Verbosity = opts.GetVerbosity();
            try {
                program = Processor.ParseProgram(reader, TextWriter.Null, Console.Error);
            } catch (FormatException e) {
                Console.Error.WriteLine("Program was not parsed successfully:\n{0}", e.Message);
                return null;
            }
            try {
                ReferenceType.PointerSize = opts.PointerSize;
                program.WellFormed();
            } catch (IllFormedException e) {
                Console.Error.WriteLine("Program is not well formed at: {0}\n{1}", e.Entity, e.Message);
                return null;
            }
            VerbConsole.WriteLine(VerbosityLevel.Default, $"Default SMT solver was invoked a total of {NanoCSMT.Default.InvocationCount} times to verify your input!");
            return program;
        }

        private static TextReader? GetSafeInputReader(InputOption opts) {
            try {
                return opts.GetInputReader();
            } catch (IOException e) {
                Console.Error.WriteLine("Specified input cannot be read: {0}", e.Message);
                return null;
            }
        }
        /// <summary>
        /// Typecheck the program specified in the options.
        /// </summary>
        /// <param name="opts">Options that specify the input source and additional check options.</param>
        /// <returns>Program exit code.</returns>
        public static int CheckCode(CheckOptions opts) {
            var program = GetProgram(opts);
            if (program is null) return 1;

            Console.Error.WriteLine("Program is well formed!");
            return 0;
        }
        /// <summary>
        /// Typecheck the program specified in the options and format it. The formatted program is output according to the output specified in the options.
        /// </summary>
        /// <param name="opts">Options that specify the input source, output stream and additional check options.</param>
        /// <returns>Program exit code.</returns>
        public static int FormatCode(FormattingOptions opts) {
            var program = GetProgram(opts);
            if (program is null) return 1;

            using var output = opts.GetOutputWriter();
            output.Write(program.ToString());
            return 0;
        }
        /// <summary>
        /// Typecheck the program specified in the options and translate it to C code. The formatted program is output according to the output specified in the options.
        /// </summary>
        /// <param name="opts">Options that specify the input source, output stream and additional check options.</param>
        /// <returns>Program exit code.</returns>
        public static int TranslateCode(TranslationOptions opts) {
            var program = GetProgram(opts);
            if (program is null) return 1;

            using var output = opts.GetOutputWriter();
            output.Write(program.ToCString());
            return 0;
        }
    }
}
