﻿using CommandLine;
using NanoCLang;
using System;
using System.IO;
using System.Text;

namespace NanoCC {
    /// <summary>
    /// Provides the class for parsing command line options for the compiler frontend.
    /// </summary>
    public class IOOptions : InputOption, IOutputOption {
        [Option('o', "output", Required = true,
            HelpText = "Output file.\nUse \"" + STD_KEY + "\" to output to the standard output instead of a file.")]
        public string OutputFile { get; set; } = string.Empty;
        /// <summary>
        /// Read the input file. Use this to automatically handle std reading.
        /// </summary>
        /// <returns>Contents of the specified input.</returns>
        public TextWriter GetOutputWriter() => OutputFile == STD_KEY
            ? Console.Out
            : new StreamWriter(OutputFile, false, Encoding.UTF8);
    }
    /// <summary>
    /// Provides the interface for file input options.
    /// </summary>
    public interface IOutputOption {
        /// <summary>
        /// Output file.
        /// Use a single dash to output to the standard output instead of a file.
        /// </summary>
        string OutputFile { get; set; }
        /// <summary>
        /// Read the input file. Use this to automatically handle std reading.
        /// </summary>
        /// <returns>Contents of the specified input.</returns>
        TextWriter GetOutputWriter();
    }
    /// <summary>
    /// Provides the option for file input.
    /// </summary>
    public class InputOption : IInputOption {
        /// <summary>
        /// Gets or sets a flag that increases verbosity.
        /// </summary>
        [Option('v', "verbose",
            HelpText = "Flag to increase verbosity and debug output.")]
        public bool Verbose { get; set; }
        internal VerbosityLevel GetVerbosity() => Verbose ? VerbosityLevel.Default : VerbosityLevel.High;
        /// <summary>
        /// The value used to refer to the standard streams in file descriptors.
        /// </summary>
        protected const string STD_KEY = "+";
        [Option('i', "input", Required = true,
            HelpText = "Input source file to process.\nUse \"" + STD_KEY + "\" to read input from the standard input instead of a file.")]
        /// <inheritdoc/>
        public string InputFile { get; set; } = string.Empty;
        /// <summary>
        /// Gets or sets the size of pointer types in bytes.
        /// </summary>
        [Option('s', "size", Default = 8,
            HelpText = "Size of pointer types in bytes.")]
        public int PointerSize { get; set; }
        /// <inheritdoc/>
        public TextReader GetInputReader() => InputFile == STD_KEY
            ? Console.In
            : new StreamReader(InputFile, Encoding.UTF8);
    }
    /// <summary>
    /// Provides the interface for file input options.
    /// </summary>
    public interface IInputOption {
        /// <summary>
        /// Input source file to process.
        /// Use a single dash to use input from the standard input instead of a file.
        /// </summary>
        string InputFile { get; set; }
        /// <summary>
        /// Read the input file. Use this to automatically handle std reading.
        /// </summary>
        /// <returns>Contents of the specified input.</returns>
        TextReader GetInputReader();
    }
}
