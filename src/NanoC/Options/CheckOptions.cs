﻿using CommandLine;
using CommandLine.Text;
using System.Collections.Generic;

namespace NanoCC {
    /// <summary>
    /// Options for type checking NanoC code.
    /// </summary>
    [Verb("check", isDefault: true,
        HelpText = "Typecheck the NanoC source file.")]
    public class CheckOptions : InputOption {
        /// <summary>
        /// Provide a list of code check option examples.
        /// </summary>
        [Usage(ApplicationAlias = "nanocc")]
        public static IEnumerable<Example> Examples {
            get {
                yield return new Example("Typecheck the source file source.nc", new CheckOptions { InputFile = "source.nc" });
                yield return new Example("Typecheck the source file from the standard input stream", new CheckOptions { InputFile = STD_KEY });
                yield return new Example("Set pointer size to 32 bit (4 bytes) during typechecking", new CheckOptions { InputFile = "source.nc", PointerSize = 4 });
            }
        }
    }
}
