﻿using CommandLine;
using CommandLine.Text;
using System.Collections.Generic;

namespace NanoCC {
    /// <summary>
    /// Options for translating NanoC code to C code.
    /// </summary>
    [Verb("translate",
        HelpText = "Translate the NanoC source file to C code and write it to the output.")]
    public class TranslationOptions : IOOptions {
        /// <summary>
        /// Provide a list of translation option examples.
        /// </summary>
        [Usage(ApplicationAlias = "nanocc")]
        public static IEnumerable<Example> Examples {
            get {
                yield return new Example("Translate input file source.nc to C code and write it to standard output", new TranslationOptions { InputFile = "source.nc", OutputFile = STD_KEY });
                yield return new Example("Translate input from standard input and write it to a file", new TranslationOptions { InputFile = STD_KEY, OutputFile = "source.c" });
            }
        }
    }
}
