﻿using CommandLine;
using CommandLine.Text;
using System.Collections.Generic;
using System.IO;

namespace NanoCC {
    /// <summary>
    /// Options for formatting NanoC code.
    /// </summary>
    [Verb("format",
        HelpText = "Format the NanoC source file and write it to the output.")]
    public class FormattingOptions : IOOptions {
        /// <summary>
        /// Provide a list of formatting option examples.
        /// </summary>
        [Usage(ApplicationAlias = "nanocc")]
        public static IEnumerable<Example> FormatExamples {
            get {
                yield return new Example("Format input file source.nc and write it to standard output", new FormattingOptions { InputFile = "source.nc", OutputFile = STD_KEY });
                yield return new Example("Format input from standard input and write it to a file", new FormattingOptions { InputFile = STD_KEY, OutputFile = "formatted.nc" });
            }
        }
    }
}
