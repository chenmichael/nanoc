using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using NanoCLang.Environemnts;

namespace NanoCLang.Tests {
    [TestClass]
    public class ImpureInference {
        [TestMethod]
        public void PureHeap() {
            var expr = Processor.GetParser(@"
let assert(1_4) in
1_4 + 5_4 - 3_4 < 24_4
").ParseExpression();
            var world = expr.InferWorld(new GlobalEnvironment(), new LocalEnvironment(), new Heap());
            Assert.IsTrue(world.Heap.Empty);
        }
        [TestMethod]
        public void AllocationInference() {
            var heap = Processor.GetParser(@"~data ~> 0+1 : 1[any]").ParseHeap();
            var expr = Processor.GetParser(@"malloc(data, 20_4)").ParseExpression();
            // check correct inference of allocation
            var world = expr.InferWorld(new GlobalEnvironment(), new LocalEnvironment(), heap);
            // check return value
            Assert.IsTrue(world.Type.BaseType is ReferenceType ptr && ptr.Concrete && ptr.Location.Name == "data");
            // check safety
            Assert.IsTrue(world.Type.SubType(new LocalEnvironment(), new RefinedType(
                world.Type.BaseType,
                v => new UninterpretedApplicationExpression("Safe", v)
                & new UninterpretedApplicationExpression("BLen", v, new IntegerConstant(20, 4))
                )));

            // fail if ~data is not bound
            Assert.ThrowsException<IllFormedException>(() => expr.InferWorld(new GlobalEnvironment(), new LocalEnvironment(), new Heap()));
            // fail if data is already bound
            Assert.ThrowsException<IllFormedException>(() => expr.InferWorld(new GlobalEnvironment(), new LocalEnvironment(), world.Heap));
            // fail if allocation size is non-integral
            Assert.ThrowsException<IllFormedException>(() => Processor.GetParser(@"malloc(data, ptr)").ParseExpression()
                .InferWorld(new GlobalEnvironment(), new LocalEnvironment() { ["ptr"] = new ReferenceType(new Location("l", false), new SingletonIndex(0)) }, heap));
            // fail if allocation size is negative
            Assert.ThrowsException<IllFormedException>(() => Processor.GetParser(@"malloc(data, (20_4 - 24_4))").ParseExpression()
                .InferWorld(new GlobalEnvironment(), new LocalEnvironment(), heap));
            // fail if allocation size is zero
            Assert.ThrowsException<IllFormedException>(() => Processor.GetParser(@"malloc(data, (20_4 - 20_4))").ParseExpression()
                .InferWorld(new GlobalEnvironment(), new LocalEnvironment(), heap));
        }
        [TestMethod]
        public void PointerReadInference() {
            var heap = Processor.GetParser(@"~data ~> 0+1 : 1[any] * data ~> 0+1 : 1[any]").ParseHeap();
            var gamma = new LocalEnvironment() {
                ["ptr"] = new RefinedType(new ReferenceType(new Location("data", false), Index.Zero),
                v => new UninterpretedApplicationExpression("Safe", v)
                & new UninterpretedApplicationExpression("BLen", v, new IntegerConstant(20, 4))
                )
            };
            // check correct inference of allocation
            var world = Processor.GetParser("*ptr").ParseExpression().InferWorld(new GlobalEnvironment(), gamma, heap);
            // check return value
            Assert.AreEqual(world.Type.BaseType, new IntegerType(1));
            // check no heap modification
            Assert.AreEqual(world.Heap, heap);

            // cannot read from integer type
            Assert.ThrowsException<IllFormedException>(() => Processor.GetParser("*1_4").ParseExpression()
                .InferWorld(new GlobalEnvironment(), gamma, heap));
            // can read from safely offset pointer
            Assert.AreEqual(world, Processor.GetParser(@"*(ptr + 4_4)").ParseExpression()
                .InferWorld(new GlobalEnvironment(), gamma, heap));
            // can read from safely offset pointer
            Assert.AreEqual(world, Processor.GetParser(@"*(ptr + 19_4)").ParseExpression()
                .InferWorld(new GlobalEnvironment(), gamma, heap));
            // can no read from unsafely offset pointers
            Assert.ThrowsException<IllFormedException>(() => Processor.GetParser(@"*(ptr + 20_4)").ParseExpression()
                .InferWorld(new GlobalEnvironment(), gamma, heap));
            // can no read from unsafely offset pointers
            Assert.ThrowsException<IllFormedException>(() => Processor.GetParser(@"*(ptr - 1_4)").ParseExpression()
                .InferWorld(new GlobalEnvironment(), gamma, heap));
        }
        /// <summary>
        /// This test makes sure that pointers to multiple bytes on the heap are safely read.
        /// This is where the paper probably had a mistake in safety checking.
        /// </summary>
        [TestMethod]
        public void ReadLongTypeFromUnsafe() {
            // Bind a 4 byte integer on the data heap at 0
            var heap = Processor.GetParser(@"~data ~> 0 : 4[any] * data ~> 0 : {v : 4[any] | v == 25_4}").ParseHeap();
            // Create a pointer to this binding that only allocates first two bytes of this location

            // with original rules reading this integer should succeed because the pointer is safe
            // But actually the pointer must also be safe with regards to a type width for correcly reading
            Assert.ThrowsException<IllFormedException>(() => Processor.GetParser("*ptr").ParseExpression().InferWorld(new GlobalEnvironment(), new LocalEnvironment() {
                ["ptr"] = new RefinedType(new ReferenceType(new Location("data", false), Index.Zero),
                v => new UninterpretedApplicationExpression("Safe", v)
                & new UninterpretedApplicationExpression("BLen", v, new IntegerConstant(2, 4))
                )
            }, heap));

            // now 4 bytes are correcly allocated and the 4 byte integer can be successfully read
            var world = Processor.GetParser("*ptr").ParseExpression().InferWorld(new GlobalEnvironment(), new LocalEnvironment() {
                ["ptr"] = new RefinedType(new ReferenceType(new Location("data", false), Index.Zero),
                v => new UninterpretedApplicationExpression("Safe", v)
                & new UninterpretedApplicationExpression("BLen", v, new IntegerConstant(4, 4))
                )
            }, heap);

            // refined type is correctly read
            Assert.IsTrue(world.Type.SubType(new LocalEnvironment(), new RefinedType(new IntegerType(4),
                v => PureExpression.EqualExpression(v, new IntegerConstant(25, 4))
                )));

            // Heap is unchanged
            Assert.AreEqual(heap, world.Heap);
        }
    }
}
