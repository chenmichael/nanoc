using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using NanoCLang.Environemnts;
using System.Collections.Generic;

namespace NanoCLang.Tests {
    [TestClass]
    public class IllFormedTests {
        public IllFormedTests() {
            System.Console.WriteLine("Init class");
            ArrayGenv.AddStruct(INTFIELDSTRUCT_NAME,
                new StructDefinition(new string[] { "arr" }, new TypeParameter[] { }, new FunctionCallExpression[] { },
                new StructField[] { new StructField(STRUCTFIELD_NAME, new BlockType(new SingletonIndex(0), new IntegerType(4))) }));
            ArrayEnv.AddStructLocs(new Dictionary<string, string> {
                { "loc", INTFIELDSTRUCT_NAME }
            });
        }
        private const string ARRAY_LOCATION = "loc";
        private const string ARRAY_NAME = "arr";
        private const string INTEGER_NAME = "val";
        private const string INTFIELDSTRUCT_NAME = "intarray";
        private const string STRUCTFIELD_NAME = "data";
        private const string CONCRETEFUNCTION_NAME = "concretefunction";
        private readonly GlobalEnvironment ArrayGenv = new GlobalEnvironment() {
            {CONCRETEFUNCTION_NAME, new RawFunctionSchema(new string[]{ "loc"}, new TypeParameter[]{ },new IHeapElement[]{
                new FunctionCallExpression(INTFIELDSTRUCT_NAME, new string[]{ "loc"}), new LocationBinding(new Location("loc", false),
                    new BlockType[]{ new BlockType(new SingletonIndex(0), new IntegerType(4)) })},
                new RawWorld(IntegerType.Void, new IHeapElement[]{new FunctionCallExpression(INTFIELDSTRUCT_NAME, new string[]{ "loc"})})) }
        };
        private readonly LocalEnvironment ArrayEnv = new LocalEnvironment(new KeyValuePair<string, Type>[] {
            new KeyValuePair<string, Type>(ARRAY_NAME, new RefinedType(new ReferenceType(new Location(ARRAY_LOCATION, false), Index.Zero),
                v => new UninterpretedApplicationExpression("Safe", v)
                & new UninterpretedApplicationExpression("BLen", v, new IntegerConstant(24, 4)))),
            new KeyValuePair<string, Type>(INTEGER_NAME, new IntegerType(4))
        });
        private readonly Heap ArrayHeap = new Heap(new LocationBinding[] {
            new LocationBinding(new Location(ARRAY_LOCATION, true), new BlockType[]{new BlockType(new SequenceIndex(0, 4), new IntegerType(4))}),
            new LocationBinding(new Location(ARRAY_LOCATION, false), new BlockType[]{new BlockType(new SequenceIndex(0, 4), new IntegerType(4))})
        });
        [TestMethod]
        public void Assertion() {
            Processor.GetParser(@$"assert({INTEGER_NAME} == 1_4 || {INTEGER_NAME} != 1_4)").ParsePureExpression().InferType(ArrayEnv);

            // Cannot assert reference
            Assert.ThrowsException<IllFormedException>(() =>
                Processor.GetParser(@$"assert({ARRAY_NAME})").ParsePureExpression().InferType(ArrayEnv));
        }
        [TestMethod]
        public void MainCall() {
            Assert.ThrowsException<IllFormedException>(() =>
                Processor.GetParser(@$"{CONCRETEFUNCTION_NAME}()").ParseProgram().WellFormed(ArrayGenv));
        }
        [TestMethod]
        public void Folding() {
            Processor.GetParser(@$"[fold {ARRAY_LOCATION}]").ParseExpression().InferWorld(new GlobalEnvironment(), new LocalEnvironment(), ArrayHeap);

            // Fold without concrete binding
            Assert.ThrowsException<IllFormedException>(()
                => Processor.GetParser(@$"[fold {ARRAY_LOCATION}]").ParseExpression().InferWorld(new GlobalEnvironment(), new LocalEnvironment(), new Heap(new LocationBinding[] {
                    new LocationBinding(new Location(ARRAY_LOCATION, true), new BlockType[]{new BlockType(new SequenceIndex(0, 4), new IntegerType(4))}),
                })));

            // Fold concrete without abstract binding
            Assert.ThrowsException<IllFormedException>(()
                => Processor.GetParser(@$"[fold {ARRAY_LOCATION}]").ParseExpression().InferWorld(new GlobalEnvironment(), new LocalEnvironment(), new Heap(new LocationBinding[] {
                    new LocationBinding(new Location(ARRAY_LOCATION, false), new BlockType[]{new BlockType(new SequenceIndex(0, 4), new IntegerType(4))})
                })));

            // Fold unestablished invariant
            Assert.ThrowsException<IllFormedException>(()
                => Processor.GetParser(@$"[fold {ARRAY_LOCATION}]").ParseExpression().InferWorld(new GlobalEnvironment(), new LocalEnvironment(), new Heap(new LocationBinding[] {
                    new LocationBinding(new Location(ARRAY_LOCATION, true), new BlockType[]{new BlockType(new SequenceIndex(0, 4), new IntegerType(4, new SingletonIndex(16)))}),
                    new LocationBinding(new Location(ARRAY_LOCATION, false), new BlockType[]{new BlockType(new SequenceIndex(0, 4), new IntegerType(4))})
                })));
        }
        [TestMethod]
        public void ArrayRead() {
            Processor.GetParser(@$"{ARRAY_NAME}[4_4]").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            Assert.ThrowsException<IllFormedException>(() => {
                Processor.GetParser(@$"{INTEGER_NAME}[4_4]").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            });
        }
        [TestMethod]
        public void ArrayWrite() {
            Processor.GetParser(@$"{ARRAY_NAME}[4_4] = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            Assert.ThrowsException<IllFormedException>(() => {
                Processor.GetParser(@$"{INTEGER_NAME}[4_4] = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            });
        }
        [TestMethod]
        public void PointerRead() {
            Processor.GetParser(@$"*{ARRAY_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            Assert.ThrowsException<IllFormedException>(() => {
                // Only ref deref
                Processor.GetParser(@$"*{INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
                // Bad offset, not a subindex of the block index
                Processor.GetParser(@$"*({ARRAY_NAME} + 1_4)").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            });
        }
        [TestMethod]
        public void PointerWrite() {
            Processor.GetParser(@$"*{ARRAY_NAME} = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            Assert.ThrowsException<IllFormedException>(() => {
                // Only ref deref
                Processor.GetParser(@$"*{INTEGER_NAME} = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
                // Bad offset, not a subindex of the block index
                Processor.GetParser(@$"*({ARRAY_NAME} + 1_4) = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            });
        }
        [TestMethod]
        public void StructRead() {
            Processor.GetParser(@$"{ARRAY_NAME}->{STRUCTFIELD_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            Assert.ThrowsException<IllFormedException>(() => {
                // Only ref deref
                Processor.GetParser(@$"{INTEGER_NAME}->{STRUCTFIELD_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            });
        }
        [TestMethod]
        public void StructWrite() {
            Processor.GetParser(@$"{ARRAY_NAME}->{STRUCTFIELD_NAME} = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            Assert.ThrowsException<IllFormedException>(() => {
                // Only ref deref
                Processor.GetParser(@$"{INTEGER_NAME}->{STRUCTFIELD_NAME} = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap);
            });

            // Must point to beginning of block
            Assert.ThrowsException<IllFormedException>(()
                => Processor.GetParser(@$"let xsd = {ARRAY_NAME} + 4_4 in xsd->{STRUCTFIELD_NAME} = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap));

            // Must point to beginning of block
            Assert.ThrowsException<IllFormedException>(()
                => Processor.GetParser(@$"let xsd = {ARRAY_NAME} + 4_4 in xsd->{STRUCTFIELD_NAME} = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap));

            // Unknown member
            Assert.ThrowsException<IllFormedException>(()
                => Processor.GetParser(@$"{ARRAY_NAME}->unknown = {INTEGER_NAME}").ParseExpression().InferWorld(ArrayGenv, ArrayEnv, ArrayHeap));
        }
        [TestMethod]
        public void TernaryExpression() {
            var ternaryExpression = Processor.GetParser(@$"if {INTEGER_NAME} >= 0_4 then {INTEGER_NAME} else 0_4 - {INTEGER_NAME}").ParseExpression();
            var natzero = new RefinedType(new IntegerType(4),
                v => v >= new IntegerConstant(0, 4));
            ternaryExpression.CheckType(new GlobalEnvironment(), ArrayEnv, new Heap(), new World(natzero, new Heap()));
        }
        [TestMethod]
        public void StructDefinition() {
            // Parameter name collision
            Assert.ThrowsException<IllFormedException>(() =>
                new StructDefinition(new string[] { "loc" }, new TypeParameter[] { new TypeParameter("collide", IntegerType.Boolean), new TypeParameter("collide", IntegerType.Boolean) }, new FunctionCallExpression[] { }, new StructField[] {
                    new StructField("val", new BlockType(Index.Zero, IntegerType.Boolean))
                })
                    .WellFormed(new GlobalEnvironment()));

            // Field <> Parameter name collision
            Assert.ThrowsException<IllFormedException>(() =>
                new StructDefinition(new string[] { "loc" }, new TypeParameter[] { new TypeParameter("collide", IntegerType.Boolean) }, new FunctionCallExpression[] { }, new StructField[] {
                    new StructField("collide", new BlockType(Index.Zero, IntegerType.Boolean))
                })
                    .WellFormed(new GlobalEnvironment()));

            // Field name collision
            Assert.ThrowsException<IllFormedException>(() =>
                new StructDefinition(new string[] { "loc" }, new TypeParameter[] { }, new FunctionCallExpression[] { }, new StructField[] {
                    new StructField("collide", new BlockType(Index.Zero, IntegerType.Boolean)),
                    new StructField("collide", new BlockType(Index.Zero, IntegerType.Boolean))
                })
                    .WellFormed(new GlobalEnvironment()));

            // Unbound dependency call
            Assert.ThrowsException<IllFormedException>(() =>
                new StructDefinition(new string[] { "loc" }, new TypeParameter[] { }, new FunctionCallExpression[] { new FunctionCallExpression("unbound", new string[] { "loc" }) }, new StructField[] {
                    new StructField("val", new BlockType(Index.Zero, IntegerType.Boolean))
                })
                    .WellFormed(new GlobalEnvironment()));

            // Struct Okay
            new StructDefinition(new string[] { "loc", "loc2" }, new TypeParameter[] { }, new FunctionCallExpression[] { new FunctionCallExpression(INTFIELDSTRUCT_NAME, new string[] { "loc2" }) }, new StructField[] {
                    new StructField("val", new BlockType(Index.Zero, IntegerType.Boolean))
                })
                .WellFormed(ArrayGenv);
        }
    }
}
