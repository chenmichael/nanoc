using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using NanoCLang.Environemnts;

namespace NanoCLang.Tests {
    [TestClass]
    public class SMTTests {
        [TestMethod]
        public void IntegerTruthiness() {
            var gamma = new LocalEnvironment();
            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new IntegerConstant(1, 1)));
            Assert.AreEqual(false, NanoCSMT.Default.TryEvalConstExpression(gamma, new IntegerConstant(0, 1)));
        }
        [TestMethod]
        public void BooleanTruthiness() {
            var gamma = new LocalEnvironment();
            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new BooleanConstant(true)));
            Assert.AreEqual(false, NanoCSMT.Default.TryEvalConstExpression(gamma, new BooleanConstant(false)));
        }
        [TestMethod]
        public void RelationalTruthiness() {
            // n >= 2 && n < 8
            var gamma = new LocalEnvironment() {
            { "n", new RefinedType(new IntegerType(4),
                               v => (v >= new IntegerConstant(2, 4))
                               & (v < new IntegerConstant(8, 4))) }
            };
            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") >= new IntegerConstant(2, 4))); // trivial
            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") >= new IntegerConstant(1, 4))); // transitive
            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") > new IntegerConstant(1, 4))); // transitive
            Assert.AreEqual(null, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") >= new IntegerConstant(3, 4))); // incomplete
            Assert.AreEqual(false, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") >= new IntegerConstant(8, 4))); // never true

            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") < new IntegerConstant(8, 4))); // trivial
            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") < new IntegerConstant(9, 4))); // transitive
            Assert.AreEqual(true, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") <= new IntegerConstant(8, 4))); // transitive
            Assert.AreEqual(null, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") < new IntegerConstant(7, 4))); // incomplete
            Assert.AreEqual(false, NanoCSMT.Default.TryEvalConstExpression(gamma, new VariableExpression("n") < new IntegerConstant(2, 4))); // never true
        }
        [TestMethod]
        public void SafeNPointer() {
            var n = new IntegerConstant(20, 4);
            var safenptr = new RefinedType(new ReferenceType(new Location("data", false), new SingletonIndex(0)),
                v => new UninterpretedApplicationExpression("SafeN", v, n));
            var unsafesafenptr = new RefinedType(new ReferenceType(new Location("data", false), new SingletonIndex(0)),
                v => new UninterpretedApplicationExpression("SafeN", v, new IntegerConstant(21, 4)));
            var blenptr = new RefinedType(new ReferenceType(new Location("data", false), new SingletonIndex(0)),
                v => new UninterpretedApplicationExpression("Safe", v)
                & new UninterpretedApplicationExpression("BLen", v, n));
            var gamma = new LocalEnvironment() { ["v"] = safenptr.BaseType };
            Assert.IsTrue(NanoCSMT.Default.ValidImplication(gamma, blenptr.Refinement, safenptr.Refinement));
            Assert.IsFalse(NanoCSMT.Default.ValidImplication(gamma, blenptr.Refinement, unsafesafenptr.Refinement));
            Assert.IsFalse(NanoCSMT.Default.ValidImplication(gamma,
                new UninterpretedApplicationExpression("BLen", new VariableExpression("v"), n),
                new UninterpretedApplicationExpression("Safe", new VariableExpression("v"))));
            Assert.IsFalse(NanoCSMT.Default.ValidImplication(gamma,
                new UninterpretedApplicationExpression("BLen", new VariableExpression("v"), n),
                new UninterpretedApplicationExpression("SafeN", new VariableExpression("v"), n)));
        }
    }
}