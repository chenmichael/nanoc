using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using NanoCLang.Environemnts;
using System;

namespace NanoCLang.Tests {
    [TestClass]
    public class EnvironmentTest {
        [TestMethod]
        public void LocalEnvironment() {
            var env = new LocalEnvironment();
            Assert.IsTrue(env.Count == 0);
            var guard1 = new VariableExpression("n") > new IntegerConstant(20, 4);
            var guard2 = new VariableExpression("x") < new IntegerConstant(40, 4);
            Assert.AreEqual(env.ToString(), string.Empty);
            env.PushGuard(guard1);
            Assert.AreEqual(env.ToString(), "|(n > 20_4)");
            env.PushGuard(guard2);
            Assert.AreEqual(env.ToString(), "|(x < 40_4)|(n > 20_4)");
            env["ptr"] = new ReferenceType(new Location("loc", false), new ArbitraryIndex());
            Assert.AreEqual(env.ToString(), "ptr:ref(loc, any)|(x < 40_4)|(n > 20_4)");
            var copiedEnv = new LocalEnvironment(env);
            // check stack order
            Assert.AreEqual(guard2, env.PopGuard());
            Assert.AreEqual(guard1, env.PopGuard());
            Assert.AreEqual(guard2, copiedEnv.PopGuard());
            Assert.AreEqual(guard1, copiedEnv.PopGuard());
            Assert.ThrowsException<InvalidOperationException>(() => env.PopGuard());
            Assert.ThrowsException<InvalidOperationException>(() => copiedEnv.PopGuard());
        }
        [TestMethod]
        public void GlobalEnvironment() {
            var doublefunbind = Processor.GetParser(@"
letf name = fun() { 0_0 } : []()/emp->0[0]/emp in
letf name = fun() { 0_0 } : []()/emp->0[0]/emp in name()
").ParseProgram();
            Assert.ThrowsException<IllFormedException>(() => doublefunbind.WellFormed());
            var doublestrbind = Processor.GetParser(@"
lets name = struct() { field = 0 : 0[0] } : [] in
lets name = struct() { field = 0 : 0[0] } : [] in
letf noop = fun() { 0_0 } : []()/emp->0[0]/emp in noop()
").ParseProgram();
            Assert.ThrowsException<IllFormedException>(() => doublestrbind.WellFormed());
        }
    }
}
