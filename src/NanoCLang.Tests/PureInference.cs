using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using NanoCLang.Environemnts;

namespace NanoCLang.Tests {
    [TestClass]
    public class PureInference {
        [TestMethod]
        public void IntegerInference() {
            var type = Processor.GetParser("1_4").ParsePureExpression().InferType(new LocalEnvironment());
            Assert.IsTrue(type.BaseType is IntegerType);
            Assert.AreEqual(type.Size, 4);
        }
        [TestMethod]
        public void IllFormedInteger() {
            new IntegerConstant(0, 0).WellFormed(new LocalEnvironment());
            // Bad integer literal
            Assert.ThrowsException<IllFormedException>(() => new IntegerConstant(0, -1).WellFormed(new LocalEnvironment()));

            new IntegerType(0, Index.Zero).WellFormed(new LocalEnvironment(), new Heap());
            // Bad integer type
            Assert.ThrowsException<IllFormedException>(() => new IntegerType(-1, Index.Zero).WellFormed(new LocalEnvironment(), new Heap()));
        }
        [TestMethod]
        public void VoidInference() {
            var type = Processor.GetParser("0_0").ParsePureExpression().InferType(new LocalEnvironment());
            Assert.IsTrue(type.BaseType is IntegerType);
            var i = type.BaseType as IntegerType;
            Assert.IsNotNull(i);
            Assert.IsTrue(i.IsVoid);
        }
        [TestMethod]
        public void ReferenceInference() {
            var type = Processor.GetParser("6_4 + 5_4").ParsePureExpression().InferType(new LocalEnvironment());
            Assert.IsTrue(type.BaseType is IntegerType);
            Assert.AreEqual(type.Size, 4);
        }
        [TestMethod]
        public void ComparisonInference() {
            var type = Processor.GetParser("(6_4 + 5_4) - 1_4 == 7_4 - 2_4").ParsePureExpression().InferType(new LocalEnvironment());
            Assert.IsTrue(type.BaseType is IntegerType);
            Assert.IsTrue(type.BaseType == IntegerType.Boolean);
        }
        [TestMethod]
        public void BadAssertion() {
            var badAssert = Processor.GetParser("assert((6_4 + 5_4) - 1_4 == 13_4 - 2_4)").ParsePureExpression();
            Assert.ThrowsException<IllFormedException>(() => badAssert.InferType(new LocalEnvironment()));
        }
        [TestMethod]
        public void GoodAssertion() {
            var goodAssert = Processor.GetParser("assert((6_4 + 5_4) - 1_4 == 13_4 - 3_4)").ParsePureExpression();
            var inferred = goodAssert.InferType(new LocalEnvironment());
            Assert.IsTrue(inferred.BaseType is IntegerType i && i.IsVoid);
        }
    }
}
