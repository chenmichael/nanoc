using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using NanoCLang.Environemnts;
using System;
using Index = NanoCLang.Entities.Index;

namespace NanoCLang.Tests {
    [TestClass]
    public class ParserTesting {
        public static LocalEnvironment EmptyEnv => new LocalEnvironment();
        public static void TestToStringIdempotent<T>(T program, Func<NanoCParser, T> Parse) where T : Base {
            var parser = Processor.GetParser(program.ToString() ?? throw new InvalidProgramException("No string representation!"));
            Assert.AreEqual(program, Parse(parser));
            Assert.IsTrue(parser.NumberOfSyntaxErrors == 0, $"There were {parser.NumberOfSyntaxErrors} syntax errors while parsing the input!");
        }
        #region Programs
        [TestMethod]
        public void ParseMainCall() {
            var parser = Processor.GetParser("main()");
            var program = parser.ParseProgram();
            Assert.AreEqual(program, new MainCall("main"));
            // program is not well-formed because main is not defined
            Assert.ThrowsException<IllFormedException>(() => program.WellFormed());
            TestToStringIdempotent(program, i => i.ParseProgram());
        }
        [TestMethod]
        public void ParseFunctionDefinition() {
            const string input = "letf test=fun(){0_0}:[]()/emp->{v:0[0]|1_1}/emp in test()";
            var parser = Processor.GetParser(input);
            var program = parser.ParseProgram();
            Assert.AreEqual(program, new FunctionBinding("test", new FunctionDefinition(new IntegerConstant(0, 0), new RawFunctionSchema(new string[] { }, new TypeParameter[] { }, new LocationBinding[] { }, new RawWorld(new RefinedType(IntegerType.Void, _ => new IntegerConstant(1, 1)), new LocationBinding[] { }))), new MainCall("test")));
            program.WellFormed();
            TestToStringIdempotent(program, i => i.ParseProgram());
        }
        #endregion
        #region Impure Expressions
        [TestMethod]
        public void ParseAllocation() {
            const string input = "malloc(loc, 12_4)";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new AllocationExpression("loc", (PureExpression)12));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        [TestMethod]
        public void ParseBinding() {
            const string input = "let x = 10_4 in 0_0";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new BindingExpression("x", (PureExpression)10, new IntegerConstant(0, 0)));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        [TestMethod]
        public void ParseFold() {
            const string input = "[fold loc]";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new FoldExpression("loc"));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        [TestMethod]
        public void ParseFunctionCall() {
            const string input = "[l1, l2]init_alloc(10_4)";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new FunctionCallExpression("init_alloc", new string[] { "l1", "l2" }, new PureExpression[] { new IntegerConstant(10, 4) }));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        [TestMethod]
        public void ParsePointerWrite() {
            const string input = "*ptr = 10_4";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new PointerWriteExpression(new VariableExpression("ptr"), new IntegerConstant(10, 4)));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        [TestMethod]
        public void ParsePointerRead() {
            const string input = "*ptr2";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new PointerReadExpression(new VariableExpression("ptr2")));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        [TestMethod]
        public void ParseTeranry() {
            const string input = "if 10_4 > x then 0_2 else 3_2";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new TernaryExpression(new IntegerConstant(10, 4) > new VariableExpression("x"), new IntegerConstant(0, 2), new IntegerConstant(3, 2)));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        [TestMethod]
        public void ParseUnfoldBinding() {
            const string input = "letu pointer=[unfold ptrloc] ptr in 0_0";
            var expression = Processor.GetParser(input).ParseExpression();
            Assert.AreEqual(expression, new UnfoldExpression("pointer", "ptrloc", new VariableExpression("ptr"), new IntegerConstant(0, 0)));
            TestToStringIdempotent(expression, i => i.ParseExpression());
        }
        #endregion
        #region Pure Expression Tests
        [TestMethod]
        public void ParseBadAssertion() {
            const string input = "assert(10_4 > 14_4)";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, new AssertionExpression(new IntegerConstant(10, 4) > new IntegerConstant(14, 4)));
            Assert.ThrowsException<IllFormedException>(() => expression.CheckType(EmptyEnv, IntegerType.Void), "Assertion should fail due to expression being falsy!");
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        [TestMethod]
        public void ParseGoodAssertion() {
            const string input = "assert(10_4 < 14_4)";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, new AssertionExpression(new IntegerConstant(10, 4) < new IntegerConstant(14, 4)));
            expression.CheckType(EmptyEnv, IntegerType.Void);
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        [TestMethod]
        public void ParseArithPlusExpression() {
            const string input = "10_4 + 14_4";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, new IntegerConstant(10, 4) + new IntegerConstant(14, 4));
            expression.CheckType(EmptyEnv, new RefinedType(new IntegerType(4), v => PureExpression.EqualExpression(v, new IntegerConstant(24, 4))));
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        [TestMethod]
        public void ParseArithMinusExpression() {
            const string input = "10_4 - 14_4";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, new IntegerConstant(10, 4) - new IntegerConstant(14, 4));
            expression.CheckType(EmptyEnv, new RefinedType(new IntegerType(4), v => PureExpression.EqualExpression(v, new IntegerConstant(-4, 4))));
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        [TestMethod]
        public void ParsePtrPlusExpression() {
            var gamma = new LocalEnvironment() {
                ["ptr"] = new ReferenceType(new Location("loc", false), Index.Zero)
            };
            const string input = "ptr + 14_4";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, new VariableExpression("ptr") + new IntegerConstant(14, 4));
            expression.CheckType(gamma, new RefinedType(new ReferenceType(new Location("loc", false), new ArbitraryIndex()), v => new UninterpretedApplicationExpression("PAdd", v, new VariableExpression("ptr"), new IntegerConstant(14, 4))));
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        [TestMethod]
        public void ParsePtrMinusExpression() {
            var gamma = new LocalEnvironment() {
                ["ptr"] = new ReferenceType(new Location("loc", false), Index.Zero)
            };
            const string input = "ptr - 14_4";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, new VariableExpression("ptr") - new IntegerConstant(14, 4));
            expression.CheckType(gamma, new RefinedType(new ReferenceType(new Location("loc", false), new ArbitraryIndex()), v => new UninterpretedApplicationExpression("PAdd", v, new VariableExpression("ptr"), new IntegerConstant(-14, 4))));
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        [TestMethod]
        public void ParsePtrPlusCommutativeExpression() {
            var gamma = new LocalEnvironment() {
                ["ptr"] = new ReferenceType(new Location("loc", false), Index.Zero)
            };
            const string input = "14_4 + ptr";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, new IntegerConstant(14, 4) + new VariableExpression("ptr"));
            var expected = new RefinedType(new ReferenceType(new Location("loc", false), new ArbitraryIndex()), v => new UninterpretedApplicationExpression("PAdd", v, new VariableExpression("ptr"), new IntegerConstant(14, 4)));
            expression.CheckType(gamma, expected);
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        [TestMethod]
        public void ParsePtrMinusCommutativeExpression() {
            var gamma = new LocalEnvironment() {
                ["ptr"] = new ReferenceType(new Location("loc", false), Index.Zero)
            };
            const string input = "(0_4 - 14_4) + ptr";
            var expression = Processor.GetParser(input).ParsePureExpression();
            Assert.AreEqual(expression, (new IntegerConstant(0, 4) - new IntegerConstant(14, 4)) + new VariableExpression("ptr"));
            expression.CheckType(gamma, new RefinedType(new ReferenceType(new Location("loc", false), new ArbitraryIndex()), v => new UninterpretedApplicationExpression("PAdd", v, new VariableExpression("ptr"), new IntegerConstant(-14, 4))));
            TestToStringIdempotent(expression, i => i.ParsePureExpression());
        }
        #endregion
    }
}
