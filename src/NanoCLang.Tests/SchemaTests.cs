using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Environemnts;

namespace NanoCLang.Tests {
    [TestClass]
    public class SchemaTests {
        [TestMethod]
        public void ParseSchema() {
            var schema = Processor.GetParser(@"[l]
(n : {v : 4[any] | (v > 0_4)}, res : {v : ref(l, 0+1) | (Safe(v) && BLen(v, n))}, i : {v : 4[0+1] | ((v >= 0_4) && (v < n))}, val : 1[any])
/ ~l ~> 0+1 : 1[any] * l ~> 0+1 : 1[any]
-> {v : 0[0] | 1_1}
/ ~l ~> 0+1 : 1[any] * l ~> 0+1 : 1[any]").ParseFunctionSchema();
            Assert.AreEqual(schema.Arity, 4);
            Assert.IsTrue(schema.HasWorld);
            var emptyenv = new GlobalEnvironment();
            ParserTesting.TestToStringIdempotent(schema, i => i.ParseFunctionSchema());
            var builtschema = schema.Build(emptyenv);
            ParserTesting.TestToStringIdempotent(builtschema, i => i.ParseFunctionSchema().Build(emptyenv));
        }
    }
}
