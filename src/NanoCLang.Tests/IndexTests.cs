using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using Index = NanoCLang.Entities.Index;

namespace NanoCLang.Tests {
    [TestClass]
    public class IndexTests {
        [TestMethod]
        public void CollisionTests() {
            TestIndexCollision(true, new SingletonIndex(0), 4, new SingletonIndex(3), 4);
            TestIndexCollision(true, new SingletonIndex(3), 4, new SingletonIndex(0), 4);
            TestIndexCollision(true, new SingletonIndex(0), 8, new SingletonIndex(4), 4);
            TestIndexCollision(false, new SingletonIndex(0), 4, new SingletonIndex(4), 4);
            TestIndexCollision(false, new SingletonIndex(4), 4, new SingletonIndex(0), 4);
            TestIndexCollision(false, new SingletonIndex(0), 4, new SingletonIndex(4), 8);

            TestIndexCollision(true, new SequenceIndex(0, 8), 4, new SingletonIndex(5), 4);
            TestIndexCollision(true, new SequenceIndex(3, 8), 4, new SingletonIndex(0), 4);
            TestIndexCollision(true, new SequenceIndex(0, 8), 8, new SingletonIndex(4), 4);
            TestIndexCollision(false, new SequenceIndex(0, 8), 4, new SingletonIndex(4), 4);
            TestIndexCollision(false, new SequenceIndex(4, 4), 4, new SingletonIndex(0), 4);
            TestIndexCollision(false, new SequenceIndex(0, 12), 4, new SingletonIndex(4), 8);

            TestIndexCollision(true, new SequenceIndex(0, 8), 4, new SequenceIndex(5, 8), 4);
            TestIndexCollision(true, new SequenceIndex(3, 8), 4, new SequenceIndex(0, 8), 4);
            TestIndexCollision(true, new SequenceIndex(0, 8), 8, new SequenceIndex(4, 8), 4);
            TestIndexCollision(false, new SequenceIndex(0, 8), 4, new SequenceIndex(4, 8), 4);
            TestIndexCollision(false, new SequenceIndex(4, 8), 4, new SequenceIndex(0, 8), 4);
            TestIndexCollision(false, new SequenceIndex(0, 12), 4, new SequenceIndex(4, 12), 8);
        }

        private void TestIndexCollision(bool res, Index i1, int s1, Index i2, int s2) {
            Assert.AreEqual(res, i1.CollidesWith(s1, i2, s2));
            Assert.AreEqual(res, i2.CollidesWith(s2, i1, s1));
        }
        [TestMethod]
        public void SubIndexTest() {
            TestIndexSubtyping(true, new SingletonIndex(0), new SingletonIndex(0));
            TestIndexSubtyping(false, new SingletonIndex(0), new SingletonIndex(1));

            TestIndexSubtyping(true, new SingletonIndex(0), new SequenceIndex(-1, 1));
            TestIndexSubtyping(false, new SingletonIndex(0), new SequenceIndex(1, 1));

            TestIndexSubtyping(true, new SingletonIndex(5), new SequenceIndex(-1, 3));
            TestIndexSubtyping(false, new SingletonIndex(5), new SequenceIndex(-1, 4));

            TestIndexSubtyping(true, new SingletonIndex(0), new ArbitraryIndex());
            TestIndexSubtyping(false, new ArbitraryIndex(), new SingletonIndex(0));

            TestIndexSubtyping(true, new SequenceIndex(-1, 3), new ArbitraryIndex());
            TestIndexSubtyping(false, new ArbitraryIndex(), new SequenceIndex(-1, 3));

            TestIndexSubtyping(true, new SequenceIndex(-1, 3), new SequenceIndex(-1, 3));
            TestIndexSubtyping(true, new SequenceIndex(2, 3), new SequenceIndex(-1, 3));
            TestIndexSubtyping(false, new SequenceIndex(-1, 3), new SequenceIndex(2, 3));

            TestIndexSubtyping(true, new ArbitraryIndex(), new ArbitraryIndex());
        }

        private void TestIndexSubtyping(bool res, Index i1, Index i2) {
            Assert.AreEqual(res, i1 <= i2);
            Assert.AreEqual(res, i2 >= i1);
        }
    }
}