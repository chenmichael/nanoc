using Microsoft.VisualStudio.TestTools.UnitTesting;
using NanoCLang.Entities;
using NanoCLang.Environemnts;

namespace NanoCLang.Tests {
    [TestClass]
    public class SubtypingTesting {
        private static Type PositiveInteger { get; } = Processor.GetParser("{v : 4[any] | (v > 0_4)}").ParseRefinedType();
        private static Type TrivialRefinedInteger { get; } = Processor.GetParser("{v : 4[any] | (v > 0_4) || (v <= 0_4)}").ParseRefinedType();
        private static Type SafeBlock(int length, string location = "l") => Processor.GetParser($"{{v : ref({location}, 0) | (Safe(v) && BLen(v, {length}_4))}}").ParseRefinedType();
        private static Type Integer { get; } = Processor.GetParser("4[any]").ParseBasicType();
        private static Type NullPtr { get; } = Processor.GetParser("4[0]").ParseBasicType();
        private static Type Reference(string location = "l") => Processor.GetParser($"ref({location}, 0)").ParseBasicType();
        private static Type SafeReference(string location = "l") => Processor.GetParser($"{{v : ref({location}, 0) | Safe(v)}}").ParseRefinedType();
        [TestMethod]
        public void IntegerSubtyping() {
            Assert.IsTrue(PositiveInteger.SubType(new LocalEnvironment(), PositiveInteger));
            Assert.IsTrue(PositiveInteger.SubType(new LocalEnvironment(), Integer));
            Assert.IsTrue(Integer.SubType(new LocalEnvironment(), TrivialRefinedInteger));
            Assert.IsFalse(Integer.SubType(new LocalEnvironment(), PositiveInteger));
        }
        [TestMethod]
        public void ReferenceSubtyping() {
            var ptr = SafeBlock(8);
            var reftype = Reference();
            var saferef = SafeReference();
            Assert.IsTrue(ptr.SubType(new LocalEnvironment(), ptr));
            Assert.IsTrue(ptr.SubType(new LocalEnvironment(), reftype));
            Assert.IsTrue(ptr.SubType(new LocalEnvironment(), saferef));
        }
        [TestMethod]
        public void NullpointerSubtyping() {
            var absref = Reference("~l");
            var saferef = SafeReference();
            Assert.IsTrue(saferef.SubType(new LocalEnvironment(), absref));
            Assert.IsFalse(absref.SubType(new LocalEnvironment(), saferef));
            Assert.IsTrue(NullPtr.SubType(new LocalEnvironment(), absref));
            Assert.IsFalse(NullPtr.SubType(new LocalEnvironment(), saferef));
        }
        /// <summary>
        /// Checking the recursion boundary check of init_alloc
        /// <code>
        /// letf init_alloc = fun(n, res, i, val) {
        ///     let write = *(res + i) = val in
        ///     let next = i + 1_4 in
        ///     if next &lt; n
        ///     then [l]init_alloc(n, res, next, val)
        ///     else 0_0
        /// }
        /// </code>
        /// </summary>
        [TestMethod]
        public void RecursionBounds() {
            var next = Processor.GetParser("{v : 4[any] | v == i + 1_4}").ParseRefinedType();
            var bound = Processor.GetParser("{v : 4[any] | v >= 0_4 && v < n}").ParseRefinedType();

            var gamma = new LocalEnvironment {
                { "n", Processor.GetParser("{v : 4[any] | v > 0_4}").ParseRefinedType() },
                { "i", Processor.GetParser("{v : 4[any] | v >= 0_4 && v < n}").ParseRefinedType() },
                { "next", next }
            };

            // First fail because boundary guard is missing (edge case)
            Assert.IsFalse(next.SubType(gamma, bound));

            // Add boundary guard from if statement
            gamma.PushGuard(Processor.GetParser("next < n").ParsePureExpression());
            Assert.IsTrue(next.SubType(gamma, bound));

            // Remove guard
            gamma.PopGuard();
            // First fail because boundary guard should be removed now
            Assert.IsFalse(next.SubType(gamma, bound));
        }
        /// <summary>
        /// Checking the safe pointer arithmetic Valid([gamma] &amp; [PAdd(v, res, i)] =&gt; [Safe(v)]) in
        /// <code>
        /// letf init_alloc = fun(n, res, i, val) {
        ///     let write = *(res + i) = val in
        ///     ...
        /// }
        /// </code>
        /// </summary>
        [TestMethod]
        public void SafePointerArithmetic() {
            var offsetPtr = Processor.GetParser("{ v : ref(l, any) | PAdd(v, res, i) }").ParseRefinedType();
            var safePtr = Processor.GetParser("{ v : ref(l, any) | Safe(v) }").ParseRefinedType();

            var gamma = new LocalEnvironment {
                { "res", Processor.GetParser("{ v : ref(l, 0) | Safe(v) && BLen(v, n) }").ParseRefinedType() },
                { "n", Processor.GetParser("{v : 4[any] | v > 0_4}").ParseRefinedType() },
                { "i", Processor.GetParser("{v : 4[any] | v >= 0_4 && v < n}").ParseRefinedType() },
            };

            Assert.IsTrue(offsetPtr.SubType(gamma, safePtr));
        }
        [TestMethod]
        public void BadPointerArithmetic() {
            var offsetPtr = Processor.GetParser("{ v : ref(l, any) | PAdd(v, res, i) }").ParseRefinedType();
            var safePtr = Processor.GetParser("{ v : ref(l, any) | Safe(v) }").ParseRefinedType();
            var anyPointer = Processor.GetParser("ref(l, any)").ParseBasicType();

            var gamma = new LocalEnvironment {
                { "res", Processor.GetParser("{ v : ref(l, 0) | Safe(v) && BLen(v, n) }").ParseRefinedType() },
                { "n", Processor.GetParser("{v : 4[any] | v > 0_4}").ParseRefinedType() },
                // Note that i is now allowed to equal n making resulting in an unsafe pointer
                { "i", Processor.GetParser("{v : 4[any] | v >= 0_4 && v <= n}").ParseRefinedType() },
            };

            Assert.IsFalse(offsetPtr.SubType(gamma, safePtr));
            Assert.IsTrue(offsetPtr.SubType(gamma, anyPointer));
        }
    }
}
