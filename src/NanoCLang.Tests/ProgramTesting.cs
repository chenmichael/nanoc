using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NanoCLang.Tests {
    [TestClass]
    public class ProgramTesting {
        private static void TestProgram(in string input) {
            var parser = Processor.GetParser(input);
            var program = parser.ParseProgram();
            Assert.IsTrue(parser.NumberOfSyntaxErrors == 0);
            program.WellFormed();
            ParserTesting.TestToStringIdempotent(program, i => i.ParseProgram());
            Console.WriteLine(new StringFormatter().ToString(program.Tokens(new CSourceFormat())));
        }
        #region Programs
        [TestMethod]
        public void ParseMakeStringExample() {
            TestProgram(@"
letf init_alloc = fun(n, res, i, val) {
    let *(res + i) = val in
    let next = (i + 1_4) in
    if (next < n)
    then [l]init_alloc(n, res, next, val)
    else 0_0
} : [l]
(n : {v : 4[any] | (v > 0_4)}, res : {v : ref(l, 0+1) | (Safe(v) && BLen(v, n))}, i : {v : 4[0+1] | ((v >= 0_4) && (v < n))}, val : 1[any])
/ ~l ~> 0+1 : 1[any]
* l ~> 0+1 : 1[any]
-> {v : 0[0] | 1_1}
/ ~l ~> 0+1 : 1[any]
* l ~> 0+1 : 1[any] in

letf make_string = fun(n) {
    let res = malloc(l, n) in
    let [l]init_alloc(n, res, 0_4, 0_1) in
    let [fold l] in
    res
} : [l]
(n : {v : 4[any] | (v > 0_4)})
/ ~l ~> 0+1 : 1[any]
-> {v : ref(~l, 0) | (Safe(v) && BLen(v, n))}
/ ~l ~> 0+1 : 1[any] in

letf test = fun() {
    let [l]make_string(12_4) in
    0_0
} : []
()
/ ~l ~> 0+1 : 1[any]
-> {v : 0[0] | 1_1}
/ ~l ~> 0+1 : 1[any] in

test()");
        }
        [TestMethod]
        public void ParseSimpleExample() {
            TestProgram(@"
letf write = fun(pt, val) {
    *pt = val
} : [l]
(pt : {v : ref(l, 0) | SafeN(v, 4_4)}, val : 4[any])
/ ~l ~> 0 : 4[any]
* l ~> 0 : 4[any]
-> {v : 0[0] | 1_1}
/ ~l ~> 0 : 4[any]
* l ~> 0 : {v : 4[any] | (v == val)} in

letf noop = fun() {
    0_0
} : []
()
/ emp
-> {v : 0[0] | 1_1}
/ emp in

noop()");
        }
        [TestMethod]
        public void ParseNewStringExample() {
            TestProgram(@"
letf init_alloc = fun(n, res, i, val) {
    let *(res + i) = val in
    let next = (i + 1_4) in
    if (next < n)
    then [l]init_alloc(n, res, next, val)
    else 0_0
} : [l]
(n : {v : 4[any] | (v > 0_4)}, res : {v : ref(l, 0+1) | (Safe(v) && BLen(v, n))}, i : {v : 4[0+1] | ((v >= 0_4) && (v < n))}, val : 1[any])
/ ~l ~> 0+1 : 1[any]
* l ~> 0+1 : 1[any]
-> 0[0]
/ ~l ~> 0+1 : 1[any]
* l ~> 0+1 : 1[any] in

letf make_string = fun(n) {
    let res = malloc(l, n) in
    let [l]init_alloc(n, res, 0_4, 0_1) in
    let [fold l] in
    res
} : [l]
(n : {v : 4[any] | (v > 0_4)})
/ ~l ~> 0+1 : 1[any]
-> {v : ref(~l, 0) | (Safe(v) && BLen(v, n))}
/ ~l ~> 0+1 : 1[any] in

letf new_string = fun(n) {
    let str = malloc(string, 12_4) in
    let *str = n in
    let data = [data]make_string(n) in
    let *(str + 4_4) = data in
    let [fold string] in
    str
} : [string, data]
(n : {v : 4[any] | (v > 0_4)})
/ ~string ~> 0 : {v : 4[any] | (v == n)}, 4 : {v : ref(~data, 0+1) | (Safe(v) && BLen(v, @0))}
* ~data ~> 0+1 : 1[any]
-> {v : ref(~string, 0) | (Safe(v) && BLen(v, 12_4))}
/ ~string ~> 0 : {v : 4[any] | (v == n)}, 4 : {v : ref(~data, 0+1) | (Safe(v) && BLen(v, @0))}
* ~data ~> 0+1 : 1[any] in

letf call = fun() {
    let ptr = [string, data]new_string(10_4) in
    letu str = [unfold string]ptr in
    let data = *(str + 4_4) in
    letu dataPtr = [unfold data]data in
    let *(dataPtr + 9_4) = 24_1 in
    let [fold data] in
    let [fold string] in
    0_0
} : [string, data]
()
/ ~string ~> 0 : {v : 4[any] | (v == 10_4)}, 4 : {v : ref(~data, 0+1) | (Safe(v) && BLen(v, @0))}
* ~data ~> 0+1 : 1[any] in

call()");
        }
        [TestMethod]
        public void ParseStructNewStringExample() {
            TestProgram(@"
lets string_data = struct() {
    chars = 0+1 : 1[any]
} : [data] in

lets string = struct(n : {v : 4[any] | (v > 0_4)}) : [data]string_data() {
    length = 0 : {v : 4[any] | (v == n)}
    raw = 4 : {v : ref(~data, 0+1) | (Safe(v) && BLen(v, length))}
} : [string, data] in

letf init_alloc = fun(n, res, i, val) {
    let res[i] = val in
    let next = (i + 1_4) in
    if (next < n)
    then [l]init_alloc(n, res, next, val)
    else 0_0
} : [l]
(n : {v : 4[any] | (v > 0_4)}, res : {v : ref(l, 0+1) | (Safe(v) && BLen(v, n))}, i : {v : 4[0+1] | ((v >= 0_4) && (v < n))}, val : 1[any])
/ [l]string_data()
* l ~> 0+1 : 1[any]
-> 0[0]
/ [l]string_data()
* l ~> 0+1 : 1[any] in

letf make_string = fun(n) {
    let res = malloc(l, n) in
    let [l]init_alloc(n, res, 0_4, 0_1) in
    let [fold l] in
    res
} : [l]
(n : {v : 4[any] | (v > 0_4)})
/ [l]string_data()
-> {v : ref(~l, 0) | (Safe(v) && BLen(v, n))}
/ [l]string_data() in

letf new_string = fun(n) {
    let str = malloc(string, 12_4) in
    let str->length = n in
    let data = [data]make_string(n) in
    let str->raw = data in
    let [fold string] in
    str
} : [string, data]
(n : {v : 4[any] | (v > 0_4)})
/ [string, data]string(n)
-> {v : ref(~string, 0) | (Safe(v) && BLen(v, 12_4))}
/ [string, data]string(n) in

letf call = fun() {
    let ptr = [string, data]new_string(10_4) in
    letu str = [unfold string]ptr in
    let data = str->raw in
    letu dataPtr = [unfold data]data in
    let dataPtr[9_4] = 24_1 in
    let lenArr = str[0_4] in
    let lenStr = str->length in
    let assert(lenArr == lenStr && lenStr == 10_4) in
    let [fold data] in
    [fold string]
} : [string, data]
()
/ [string, data]string(10_4) in

call()");
        }
        [TestMethod]
        public void ParseStructExampleWithShorthandTypes() {
            TestProgram(@"
lets string_data = struct() {
    chars = 0+1 : char
} : [data] in

lets string = struct(n : {v : int | (v > 0_4)}) : [raw]string_data() {
    length = 0 : {v : int | (v == n)}
    raw = 4 : {v : ref(~raw, 0+1) | (Safe(v) && BLen(v, length))}
} : [string, raw] in

letf init_alloc = fun(n, res, i, val) {
    let res[i] = val in
    let next = (i + 1_4) in
    if (next < n)
    then [l]init_alloc(n, res, next, val)
    else return
} : [l]
(n : {v : int | (v > 0_4)}, res : {v : ref(l, 0+1) | (Safe(v) && BLen(v, n))}, i : {v : 4[0+1] | ((v >= 0_4) && (v < n))}, val : char)
/ [l]string_data()
* l ~> 0+1 : char
-> 0[0]
/ [l]string_data()
* l ~> 0+1 : char in

letf make_string = fun(n) {
    let res = malloc(l, n) in
    let [l]init_alloc(n, res, 0_4, 0_1) in
    let [fold l] in
    res
} : [l]
(n : {v : int | (v > 0_4)})
/ [l]string_data()
-> {v : ref(~l, 0) | (Safe(v) && BLen(v, n))}
/ [l]string_data() in

letf new_string = fun(n) {
    let str = malloc(string, 12_4) in
    let str->length = n in
    let data = [data]make_string(n) in
    let str->raw = data in
    let [fold string] in
    str
} : [string, data]
(n : {v : int | (v > 0_4)})
/ [string, data]string(n)
-> {v : ref(~string, 0) | (Safe(v) && BLen(v, 12_4))}
/ [string, data]string(n) in

letf call = fun() {
    let ptr = [string, data]new_string(10_4) in
    letu str = [unfold string]ptr in
    let data = str->raw in
    letu dataPtr = [unfold data]data in
    let dataPtr[9_4] = 24_1 in
    let lenArr = str[0_4] in
    let lenStr = str->length in
    let assert(lenArr == lenStr && lenStr == 10_4) in
    let [fold data] in
    [fold string]
} : [string, data]
()
/ [string, data]string(10_4) in

call()");
        }
        [TestMethod]
        public void ParseAbsoluteFunction() {
            TestProgram(@"
letf abs = fun(n) {
    if n < 0_4
    then 0_4 - n
    else n
} : [](n : int) / emp
-> {v : 4[any] | v >= 0_4 && (v == n || v == 0_4 - n)} / emp in

letf noop = fun(){0_0}:[]()/emp->0[0]/emp in noop()");
        }
        #endregion
    }
}