# A Low-Level Liquid Type System for Memory Safety

[[_TOC_]]

Implementing a NanoC type system using liquid types to achieve memory safety in a low-level environment.

The repository includes a CI configuration for continuous building and testing. The current pipeline status can be seen here [![pipeline status](https://gitlab.com/chenmichael/nanoc/badges/main/pipeline.svg)](https://gitlab.com/chenmichael/nanoc/-/commits/main).

The pipeline additionally generates a code coverage report for the current state of the repository (the latest commit on all branches) that is available at the [repository pages](https://nanoc.chenmichael.de).

## Type System

The [`src`](src/) directory contains the source code for the NanoC language and the implementation of the compiler user interface (console application). The compiler can be invoked using the `nanocc` command.

### Using NanoCC

NanoCC is available as a cross-platform .NET Core 3.1 application `NanoCC.dll` that can be run using the `dotnet` application with the corresponding .NET Core [runtime](https://dotnet.microsoft.com/download) installed. When building the NanoCC project using the .NET Core SDK it will output a `NanoCC.exe` or `./NanoCC` application depending on the operating system that can be directly invoked instead of `dotnet NanoCC.dll`.

The command line takes parameters `i` (`input`) and `o` (`output`) to specify the input and output files of the compiler. You can use `+` instead of a filename to input and output to the standard I/O streams. More information and verb specific help and arguments using `NanoCC help` or `NanoCC help <verb>`.

You can typecheck a sourcefile using:

```bash
nanocc -i /code/source.nc
```

Or you can directly pipe NanoC code to the input:

```bash
cat source.nc | nanocc -i +
```

To translate a local source file into C code using docker interactively you could execute:

```bash
# using stdin pipe
cat source.nc | nanocc translate -i + -o + > output.nc
# or using file pipes
nanocc translate -i + -o + < source.nc > output.c
# or just write to the stdout without creating a file
nanocc translate -i + -o + < source.nc
# or format/beautify the source file using (overwriting the original file)
nanocc format -i source.nc -o source.nc
```

### Running using Docker

If you prefer not installing anything on your machine to be able to run NanoCC you can still choose to run NanoCC inside a docker container. You can pull the image straight from the GitLab container registry [here](https://gitlab.com/chenmichael/nanoc/container_registry).

There are several tags available:
- registry.gitlab.com/chenmichael/nanoc:latest (default image when no tag is specified) that is created from the latest commit on the main branch (stable release branch)
- registry.gitlab.com/chenmichael/nanoc:develop which is the image that is created from the newest commits on the secondary release branch
- registry.gitlab.com/chenmichael/nanoc:<name> is created from commits from the corresponding `name` branch

You can compile local files using bind mounts into a container:

```bash
# replace <localdir> with %CD% on Windows or $(pwd) on Linux to mount the local directory into the container
# optionally you can add :ro after the /code mount to specify a readonly mount
docker run --rm -v "<localdir>:/code" registry.gitlab.com/chenmichael/nanoc:latest -i /code/source.nc
```

Or you can directly pipe into the interactive docker container using:

```bash
# activate the -i flag for interactive input in docker
cat source.nc | docker run --rm -i registry.gitlab.com/chenmichael/nanoc:latest -i +
```

To translate a local source file into C code using docker interactively you could execute:

```bash
# using stdin pipe
cat source.nc | docker run --rm -i registry.gitlab.com/chenmichael/nanoc:latest translate -i + -o + > output.nc
# or using file pipes
docker run --rm -i registry.gitlab.com/chenmichael/nanoc:latest translate -i + -o + < source.nc > output.c
# or just write to the stdout without creating a file
docker run --rm -i registry.gitlab.com/chenmichael/nanoc:latest translate -i + -o + < source.nc
```

**Note**: The docker commands have the exact same syntax as the `NanoCC` executable.

## NanoCLang

NanoCLang is compiled into a .NET Standard 2.1 compatible class library that can be compiled to a dynamic link library or packed into a nuget package. If you want to build your own application on top of the NanoC language library you can simply include the package from the GitLab package registry [here](https://gitlab.com/chenmichael/nanoc/-/packages) in your .NET solution.

The NanoCLang project is tested using the MSTest project in the NanoCLang.Tests directory.

## Documentation

The [`doc`](doc/) directory contains the LaTeX files that contain the type system extensions rules.